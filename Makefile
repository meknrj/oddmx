# vim: tabstop=8
# vim: shiftwidth=8
# vim: noexpandtab

.PHONY: help
.DEFAULT_GOAL := help

newsboat_dir = ~/.newsboat
nvim_dir = ~/.config/nvim

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

dots: vim bashrc newsboat ## Install/Update all dot files

vim: nvim

$(nvim_dir): ## Create config folder for neovim
	@echo "Creating $(nvim_dir) folder for neovim config"
	@mkdir -p $@

nvim: | $(nvim_dir) ## Install/Update neovim settings
	@cp conf/init.lua $(nvim_dir)
	@echo "Neovim settings were copied"

alacritty: ## Install/Update alacritty config
	@cp conf/alacritty.toml ${HOME}/.alacritty.toml
	@echo "Alacritty settings were copied"

bashrc: ## Install/Update bashrc
	@cp conf/bashrc ${HOME}/.bashrc
	@echo "Bashrc was copied"

$(newsboat_dir): ## Create config folder for newsboat
	@echo "Creating $(newsboat_dir) folder for newsboat configs"
	@mkdir -p $@

newsboat: | $(newsboat_dir) ## Update urls for rss feeds for newsboat
	@cp conf/newsboat/* $(newsboat_dir)
	@echo "Newsboat settings were copied"

to_install = neovim python-pynvim mc cabextract unarj cdrtools newsboat ncdu \
	xclip ctags jq dosbox libreoffice-still \
	telegram-desktop vlc keepassxc yt-dlp qemu-full atril \
	nomacs ripgrep audacious qt5ct qt6ct podman transmission-gtk \
	shellcheck tree mousepad xfce4-panel-profiles fzf \
	alacritty j4-desktop-dmenu fd python-virtualenvwrapper \
	ttf-liberation-mono-nerd

packages: ## Install packages
	@yay -Syu $(to_install);

todo: ## List of Todos - further projects
	@cat TODO.md

commit_count: ## Show amount of git commits
	@git rev-list --count master
