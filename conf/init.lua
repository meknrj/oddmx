-- tip - to close all windows except current (:only or <c-w>o)
-- vim - add vimdiff keymaps
-- vim - add bookmarks to NERDTREE
-- vim - use fzf to search for files in exact folder (like tannwiki!)
-- vim - move back to vimwiki
-- https://github.com/rafamadriz/friendly-snippets/blob/main/snippets/go.json
-- Use mini.pick to search lsp diagnostics
-- Setup Rg for mini.pick
-- Read more about git integration in mini
-- Review mini.files mapping
-- https://github.com/Myzel394/easytables.nvim


-- Install Mini.Deps manually
local path_package = vim.fn.stdpath("data") .. "/site/"
local mini_path = path_package .. "pack/deps/start/mini.nvim"
if not vim.uv.fs_stat(mini_path) then
    vim.cmd('echo "Installing `mini.nvim`" | redraw')
    local clone_cmd = {
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/echasnovski/mini.nvim",
        mini_path,
    }
    vim.fn.system(clone_cmd)
    vim.cmd("packadd mini.nvim | helptags ALL")
    vim.cmd('echo "Installed `mini.nvim`" | redraw')
end

-- Setup Mini.Deps
require("mini.deps").setup({ path = { package = path_package } })

local add, now, later = MiniDeps.add, MiniDeps.now, MiniDeps.later


-- User settings
now(function()
    vim.opt.autowrite = true                                -- enable autowrite
    vim.opt.clipboard:prepend({ "unnamed", "unnamedplus" }) -- Sync with system clipboard
    vim.opt.cmdheight = 2                                   -- number of screen lines to use for the command-line
    vim.opt.conceallevel = 3                                -- hide * markup for bold and italic
    vim.opt.confirm = true                                  -- auto confirm to save changes before exiting modified buffer
    vim.opt.cursorline = true                               -- enable highlighting of the current line
    vim.opt.completeopt = { "menu", "menuone", "noselect" } -- List of sources for Insert mode completion
    vim.opt.expandtab = true                                -- expand tabs to spaces
    vim.opt.ignorecase = true                               -- ignore normal letters during search
    vim.opt.iminsert = 0
    vim.opt.imsearch = 0
    vim.opt.listchars = { tab = ">~", nbsp = "_", trail = "." } -- what to show in list mode
    vim.opt.list = true                                         -- displays whitespace and tabs
    vim.opt.mouse = "a"                                         -- enable mouse in all modes (can resize splits!)
    vim.opt.number = true                                       -- show line numbers
    vim.opt.path:append({ "**" })                               -- This is a list of directories which will be searched when using the gf
    vim.opt.showmode = false                                    -- do not show mode in statusline (we have statusline for this)
    vim.opt.shiftwidth = 4                                      -- number of spaces to use for each step of (auto)indent.
    vim.opt.softtabstop = 4                                     -- number of spaces that a <Tab> counts for while performing editing operations, like inserting a <Tab> or using <BS>.
    vim.opt.smartindent = true                                  -- do smart autoindenting when starting a new line.
    vim.opt.showmatch = true                                    -- when a bracket is inserted, briefly jump to the matching one.
    vim.opt.signcolumn ="yes"                                   -- always show the signcolumn, otherwise it would shift the text each time
    vim.opt.smartcase = true                                    -- ignore case when search pattern contains lowercase letters only
    vim.opt.tabstop = 4                                         -- number of spaces that a <Tab> in the file counts for.
    vim.opt.textwidth = 120                                     -- maximum width of text that is being inserted.
    vim.opt.wrap = false                                        -- disable line wrap
    vim.opt.updatetime = 100                                    -- if this many milliseconds nothing is typed the swap file will be written to disk
    vim.opt.undolevels = 5000                                   -- maximum number of changes that can be undone.
    vim.opt.undofile = true                                     -- store undo buffer in file
end)

-- User mappings
now(function()
    vim.keymap.set("n", "<C-a>", ":set hlsearch! hlsearch?<cr>", {
        desc = "Toggle highlight search",
        silent = true,
    })

    vim.keymap.set("n", "<leader>cd", ":lcd %:p:h<CR>:pwd<CR>", {
        desc = "Change current working directory for window",
        silent = true,
    })

    vim.keymap.set("n", "<leader>ev", ":tabnew ~/.config/nvim/init.lua<CR>", {
        desc = "Open init file in new tab",
        silent = true,
    })

    vim.keymap.set("n", "<leader>sv", ":source ~/.config/nvim/init.lua<CR>", {
        desc = "Open init file in new tab",
        silent = true,
    })

    vim.keymap.set("n", "<C-u>", ":tabnew<cr>", {
        desc = "Create new tab",
        silent = true,
    })

    vim.keymap.set("n", "<C-l>", ":tabnext<cr>", {
        desc = "Move to next tab",
        silent = true,
    })

    vim.keymap.set("n", "<C-h>", ":tabprevious<cr>", {
        desc = "Move to previous tab",
        silent = true,
    })

    vim.keymap.set("n", "Q", "gq<cr>", {
        desc = "Format a paragraph into lines",
        silent = true,
    })

    -- Readline in Insert mode
    vim.keymap.set('i', '<C-a>', '<C-o>^')
    vim.keymap.set('i', '<C-e>', '<C-o>$')

    -- Readline in Command mode
    vim.keymap.set('c', '<C-a>', '<Home>')
    vim.keymap.set('c', '<C-e>', '<End>')
end)

-- User autocommands
now(function()
    vim.api.nvim_create_autocmd({ "BufWritePre" }, {
        desc = "Delete trailing whitespaces on save",
        pattern = { "*" },
        command = ":%s/\\s\\+$//e",
    })

    vim.api.nvim_create_autocmd({ "FocusGained", "BufEnter" }, {
        desc = "Set to auto read when a file is changed from the outside",
        pattern = { "*" },
        command = "checktime",
    })

    vim.api.nvim_create_autocmd({ "Filetype" }, {
        desc = "Disable auto commenting in a new line",
        pattern = { "*" },
        command = "setlocal formatoptions-=c formatoptions-=r  formatoptions-=o",
    })
end)

-- User commands
now(function()
    vim.api.nvim_create_user_command("W", "w", { bang = true })
    vim.api.nvim_create_user_command("Wq", "wq", { bang = true })
    vim.api.nvim_create_user_command("Wa", "wa", { bang = true })

    vim.api.nvim_create_user_command("Q", "q!", { bang = true })
    vim.api.nvim_create_user_command("QQ", "q!", { bang = true })
end)

-- Eyecandy
now(function()

    require('mini.hues').setup({ background = '#121212', foreground = '#dedede' })

    -- Icons
    require("mini.icons").setup()

    --Statusline
    require("mini.statusline").setup()

    -- Indentation
    require("mini.indentscope").setup({
        mappings = {
            goto_top = "[[",
            goto_bottom = "]]",
        },
    })
end)

-- Editing
now(function()
    require("mini.comment").setup({})
    require("mini.pairs").setup()
end)


-- Search
later(function()
    local win_config = function()
        local height = math.floor(1.1 * vim.o.lines)
        local width = math.floor(1.1 * vim.o.columns)
        return {
            anchor = "NW",
            height = height,
            width = width,
            border = "single",
            row = math.floor(0.5 * (vim.o.lines - height)),
            col = math.floor(0.5 * (vim.o.columns - width)),
        }
    end


    require("mini.pick").setup({
        options = {
            use_cache = true,
        },
        window = {
            config = win_config,
        },
    })
    vim.ui.select = MiniPick.ui_select

    require("mini.extra").setup() -- for extra pickers

    vim.keymap.set("n", "<leader>cc", "<cmd>lua MiniPick.builtin.resume()<cr>", { desc = "Resume last picker" })
    vim.keymap.set("n", "<leader>ff", "<cmd>lua MiniPick.builtin.files()<cr>", { desc = "Search for Files" })
    vim.keymap.set("n", "<leader>gf", "<cmd>lua MiniPick.builtin.files({tool = 'git'})<cr>", { desc = "Search for Files in git repo" })
    vim.keymap.set("n", "<leader>rr", "<cmd>lua MiniPick.builtin.grep_live()<cr>", { desc = "Search in Files" })
    vim.keymap.set("n", "<leader>hh", "<cmd>lua MiniPick.builtin.help()<cr>", { desc = "Search Helptags" })
    vim.keymap.set("n", "<leader>gc", "<cmd>lua MiniExtra.pickers.git_commits()<cr>", { desc = "Search Git Commits" })
    vim.keymap.set("n", "<leader>sh", "<cmd>lua MiniExtra.pickers.history()<cr>", { desc = "Search Command History" })
    vim.keymap.set("n", "<leader>ll", "<cmd>lua MiniExtra.pickers.buf_lines()<cr>", { desc = "Search in Buffers Lines" })
    vim.keymap.set("n", "<leader>oo", "<cmd>lua MiniExtra.pickers.oldfiles()<cr>", { desc = "Search Visited Files" })
end)

-- Git
later(function()
    require("mini.git").setup()

    require("mini.diff").setup()
end)

--Filesystem
later(function()
    require("mini.files").setup({
        mappings = {
            close = '<ESC>',
        },
        windows = {
            preview = true,
            border = "single",
            width_preview = 80,
        }
    })

    vim.keymap.set("n", "<C-\\>", "<cmd>lua MiniFiles.open()<cr>", { desc = "Open File Explorer" })
end)

-- Treesitter
later(function()
    add({
        source = "nvim-treesitter/nvim-treesitter",
        hooks = { post_checkout = function() vim.cmd("TSUpdate") end },
    })

    require("nvim-treesitter.configs").setup({
        auto_install = true,
        indent = { enable = true },
        ensure_installed = { "c", "go", "jq", "lua", "vim", "sql", "cpp", "yaml",
            "bash", "make", "query", "cmake", "gomod", "latex",
            "python", "vimdoc", "markdown", "markdown_inline"
        },
        highlight = {
            enable = true,
            additional_vim_regex_highlighting = false,
        },
    })
end)

-- Completion
later(function()
    require("mini.completion").setup({
        mappings = {
            go_in = "<RET>",
        },
        window = {
            info = { border = "single" },
            signature = { border = "single" },
        },
    })
end)

-- LSP
now(function()
    add({
        source = "neovim/nvim-lspconfig"
    })

    require("lspconfig").gopls.setup({})
    require("lspconfig").pylsp.setup({})
    require("lspconfig").clangd.setup({
        cmd = {
            "clangd",
            "--background-index",
            "--clang-tidy",
            "--header-insertion=never",
            "--completion-style=detailed",
            "--function-arg-placeholders",
            "--fallback-style=llvm",
        },
    })

    vim.api.nvim_create_autocmd("LspAttach", {
        desc = "LSP Actions",
        callback = function(event)
            local opts = { buffer = event.buf }

            vim.keymap.set("n", "gd", "<cmd>lua vim.lsp.buf.definition()<cr>", opts)
            vim.keymap.set("n", "ca", "<cmd>lua vim.lsp.buf.code_action()<cr>", opts)
            vim.keymap.set("n", "rf", "<cmd>lua MiniExtra.pickers.lsp({ scope = 'references'})<cr>", opts)
            vim.keymap.set("n", "ds", "<cmd>lua MiniExtra.pickers.lsp({ scope = 'document_symbol'})<cr>", opts)
            vim.keymap.set("n", "ws", "<cmd>lua MiniExtra.pickers.lsp({ scope = 'workspace_symbol'})<cr>", opts)
            vim.keymap.set("n", "lo", "<cmd>lua MiniExtra.pickers.diagnostic({ scope = 'current'})<cr>", opts)
            vim.keymap.set("n", "rn", vim.lsp.buf.rename, opts)
            vim.keymap.set("n", "bf", vim.lsp.buf.format, opts)
            vim.keymap.set("n", "K", vim.lsp.buf.hover, opts)
        end,
    })
end)

-- Databases
now(function()
    add({
        source = "kristijanhusak/vim-dadbod-ui",
        depends = { "tpope/vim-dadbod"},
    })

    vim.g.db_ui_use_nerd_fonts = 1
end)
