<!-- Generel todos: -->
<!-- 1. Add Battle stanzas for Fighter -->
<!-- 2. To reach 11 and 12 level Magic-User need to got through serious quest -->
<!-- 3. Switch to silver standard -->
<!-- --- -->
<!-- title: White Box: Fantastic Midieval Adventure Game -->
<!-- author: Charlie Mason -->
<!-- date: February 12, 2024 -->
<!-- geometry: "left=2cm, right=2cm, top=3cm, bottom=3cm" -->
<!-- --- -->

<!-- Title page -->
<!-- Editing and Some WordsLayout: -->
<!-- Charlie MasonMichael Clarke -->
<!-- Cover ArtForums: -->
<!-- Eric Lofgrensnw.smolderingwizard.com -->
<!-- Interior ArtSeattle Hill Games -->
<!-- William McAusland -->
<!-- James V. West (character sheet)seattle-hill-games.blogspot.com -->
<!-- There is no board. I don’t get it! -->
<!-- Thanks To -->
<!-- Matt Finch, Marv Breig, John M. Stater, Simon J. Bull, James M. Spahn, -->
<!-- Doug ‘Merias’ Maxwell, Travis Casey, James V. West and Arthur Braune -->
<!-- Compatible with Swords & Wizardry: WhiteBox by Matt Finch and Marv Breig -->
<!-- Copyright © 2017 Seattle Hill Games. Seattle Hill Games is a trademark of Charlie Mason -->
<!-- Version 2.1: April 2020 -->
<!-- The OSR logo is by Stuart Robertson and used under the Creative Commons license linked below: -->
<!-- https://creativecommons.org/licenses/by/3.0/ -->
<!-- “Swords & Wizardry, S&W, WhiteBox, and Mythmere Games are the trademarks of Matthew J. Finch,” Seattle Hill -->
<!-- Games is not affiliated with Matthew J. Finch or Mythmere GamesTM. -->

# Introduction

When I discovered the OSR, I enjoyed every game I read, but one stood out from the rest as the most versatile of games:
Swords & Wizardry WhiteBox.  Its conciseness was a breath of fresh air after the volumnous and verbose trend in modern
fantasy role-playing games. But it had one problem, its presentation left a lot to be desired. I waited for years to see
an update, but it never hap- pened. Finally, one day, I had waited long enough. If no one else was going to update it,
then I would do it myself. I had no experience in publishing and had a decent understanding of MS Word. White Box:
Fantastic Medieval Adven- ture Game was born.

I have since gained some experience points and raised to level 2. I have also formed some relationships with industry
professionals. So when the time came that I had to add some things to the game, I took advantage of my experience and
spent some gold on a hireling to enhance the game’s presentation.

Your eyes now behold the result of that labor.

It is my sincere hope that you will find this version of the game easy to use and simple to learn and that it will serve
as the foundation for many great adven- tures in the world of your imagination.

Good journey!

-Charlie

MYTHIC MAGIC ITEM
The White Box

<!-- TODO: Use this table to create a places for modules. Like each place is an actual place which will be used in module -->

| Roll 1d6 | Destination                                                 |
| :------: | :---------:                                                 |
| 1        | The land of Blackmarsh                                      |
| 2        | The dungeons beneath Castle Grey                            |
| 3        | A magical house on a lake in Jeneve                         |
| 4        | The Green Tower on Chief’s Hill                             |
| 5        | A land of faeries and talking animals                       |
| 6        | Anywhere you want. You are only limited by your imagination |

The White Box was created by two men, who were masters of eldritch sorcery.  They brought their powers together, to
create a magical artifact, that would transport the users to other worlds of magic and wonder. It is said; no one knows
what it is, until they hold it in their hands and discover the magic for themselves. For some the box may appear to be
made of wood and for others it may even appear as a book.

# Chapter 1: Getting Started

White Box requires two kinds of participants: (1) The Referee and (2) the Player. For Players, the first step in playing
the game is to create a Player Char- acter (PC). This is a simple matter of rolling some dice to determine your
character’s attributes, picking a character class, race (if non-human) and buy- ing in-game equipment. If you’re the
Referee, however, you’ve got a bit more preparation to do—that’s covered in Chapter 7.

## Rule Number One

The most important rule in White Box is that the Referee always has the right to modify the rules. In fact, it’s
encouraged! This is your game, after all. There are gaps in the rules—holes left open on purpose—because much of the fun
of role-playing games is being able to make up rules as needed.

## Dice

White Box uses several different types of dice, and they are abbreviated herein according to the number of sides they
have. For example, a four-sided die is noted as “d4.” If this text requires a player to roll “3d4” that means to roll
three (3) four-sided dice and add the results together. Other die types (six-sided, eight-sided, ten-sided,
twelve-sided, and twenty-sided) follow in the same manner.

There is no die with 100 sides. When asked to roll d100, roll two (2) ten-sided dice, treating the first roll as the
“tens” and the second as the “ones.” So, if one were to roll a “7” and then a “3”, the result would be “73.”

The roll of two zeroes is treated as a result of “100,” so you cannot roll a “0” on percentile dice. The lowest result
possible is a “1.”

## About the Optional Rules

White Box is very easy to learn and play.The rules are simple and clear. As you create your character and read through
the rules, you will see that there are several “Optional Rules” described. These are for customizing the game to fit the
way you want to play, but if you are the Referee and this is your first time running a game like this, stick with the
basic rules and customize later. The optional rules, plus any new rules that you and your group invent, are often called
“House Rules,” because each gaming group will use a different mix of alternate and invented rules. This is a good thing,
eventually you’re supposed to customize your game to fit what you want, not to follow the “official” rules forever and
ever.

## Character Sheets

A character sheet is a piece of paper designed to organize and contain any and all necessary PC information, for the
benefit of both the Referee and Player.  For White Box, the character sheet could be something as simple as a 3x5” index
card—with equipment and spells written on the back. Or you can down- load the sheet below at
http://whiteboxgame.blogspot.com/

## Attributes

The basic attributes are numbers which represent the Strength, Dexterity, Constitution, Intelligence, Wisdom and
Charisma of a character.The standard way to create attribute scores is to roll 3d6 for each attribute in the previously
mentioned order. Once those rolls have been made, it is often obvious which type of character best fits the
abilities—though, the player always has the op- tion to play any class desired. Once the player decides on a class,
points from other attributes may be shifted around (on a 2 for 1 basis), but only into the “Prime Attribute”of that
character class. Shifting points around may not lower another ability score below 9, however.

Some Referees allow more flexibility (optionally). One example is to let the players roll 3d6 a total of six times and
arrange the rolls however they want.  This allows the player to put the best results into abilities that best suit the
type of character the player wants to be.

### Table 1: Attribute Bonus

| Attribute Roll | Description | Modifier |
| :------------: | :---------: | :------: |
| 3-6            | Low         | -1       |
| 7-14           | Average     | 0        |
| 15-18          | High        | +1       |

### Attribute Modifier

White Box doesn’t give many modifiers for the range of attribute scores. You can modify the game to change the effect
that these scores have. Table 1 gives the standard range and effects for attributes scores. Feel free to change the
values. For example, if you prefer the “average” range to be 9–12, adjust the table.

### Experience Bonus

Each character may get a bonus percentage to their “experience points” (XP) that will increase the amount of experience
points gained during an adventure.  All characters add together the following: 5% if the character’s Prime Attribute is
15+, 5% if Wisdom is 15+, and 5% if Charisma is 15+. A Cleric with Wisdom of 15+ gets the 5% bonus twice: once because
it is the Prime Attribute for a Cleric, and again because it is applied to all characters. The maximum attainable bonus
is 15%. All awards of XP to the character during play will be in- creased by the amount of the bonus (by 0%, 5%, 10%, or
15%, whichever it is).

An optional method for the experience point bonus is simply to give the character a bonus of 10% if the Prime Attribute
is 15+, 5% if the Prime Attribute is 13-14, and –10% if the Prime Attribute is lower than 9.  There is no bonus for
scores between 9 and 12. If you use this method, do not also give the +5% bonuses described in the basic rules that
follow: if you do it this way, the maximum XP bonus is 10%.

### Strength

Strength represents the character’s physical power. It is the Prime Attribute for Fighters, so when a Fighter character
has Strength of 15+, t he character gets a 5% bonus to XP. Fighters can use their Strength Bonus to modify their
“to-hit” and damage results when using melee weapons.

### Intelligence

Intelligence represents knowledge, reasoning, and the ability to solve puzzles or understand difficult concepts. A high
Intelligence score gives a character an additional language for every point above 10. Intelligence is the Prime
Attribute for Magic-users, who will get a 5% bonus to XP if the character’s Intelligence is 15+. Magic-users can use
their Intelligence Bonus to improve “spell effectiveness” (i.e. target suffers a loss on his saving throw).

### Wisdom

Wisdom determines a character’s insight, perception, and good judgment.  Wisdom is the Prime Attribute for Clerics. Any
character with a Wisdom score of 15+ gets a 5% bonus to XP, and a Cleric character with Wisdom 15+ gets another 5%
(total of 10%) because it is the Prime Attribute for the Cleric class. Clerics can use their Wisdom Bonus to improve
“spell effectiveness” (i.e.  target suffers a loss on his saving throw).

### Constitution

Constitution refers to the health and endurance of a character. A high Constitution score (15+) gives the character a +1
to each hit die, and a low score (6 or lower) gives a –1 penalty to each hit die.

### Dexterity

Dexterity is a combination of coordination and quickness. It is the Prime Attribute for Thieves (if they are used). A
high Dexterity score (15+) gives a character a bonus on “to-hit” rolls when attacking with a bow or other ranged weapon,
and a low score (6 or less) gives a penalty of -1 to such rolls. Dexterity can be used to modify a character’s Armor
Class.

### Charisma

Charisma refers to a character’s personal magnetism and leadership ability. Any character with a Charisma of 15+
receives a 5% bonus to XP. Charisma also determines the number of Non-Player Character (NPC) hirelings a character can
acquire. These hirelings include specialists (ship captains, assassins,etc.) and non-human creatures, but do not usually
include basic soldiers. Cha- risma modifies the loyalty of NPCs (See “Loyalty”).

### Table 2: Charisma Modifier

| Charisma | Hirelings (Max \#) | Loyalty |
| :------: | :----------------: | :-----: |
| 3–4      | 1                  | -2      |
| 5–6      | 2                  | -2      |
| 7–8      | 3                  | -1      |
| 9–12     | 4                  | 0       |
| 13–15    | 5                  | +1      |
| 16–17    | 6                  | +2      |
| 18       | 7                  | +2      |

### Hit Points

Hit points (HP) represent the amount of “damage” a character can take before dying: they are determined by the number of
hit dice (HD) the character has at each level, which is found in the description of each character class later on.  If a
player begins the game with a 1st level Fighter (1+1 HD) the player would therefore roll 1 HD (1d6) and add “+1” to the
end result to determine his PC’s starting hit points. When the character accumulates enough XP to gain a level, usually
a new hit die is rolled and added to the total (sometimes a new level only gives an additional hit point, though, not a
whole new hit die).

### Alignment

Alignments in the game represent cosmic forces and are described as Law, Chaos, and Neutrality. In general, Law also
corresponds to being “good,”Chaos corresponds to being “evil,” and Neutrality simply means that the character is
indifferent between the two moral polarities. It is quite possible for the Referee to make the alignment system more
complex: perhaps Lawful only means that you are in favor of centralized hierarchies in society, in which case you could
actually be Lawful Evil as well as Lawful Good. It is a matter of preference if you want to make the alignment system
more complex.

### Character Retirement

If a character reaches the highest level listed on the table for that character class, what happens? Does advancement
stop? Does the character have to retire? Each group has its own play style and a preference for a given range of
character levels for their games. If the Referee wants to extend the tables to allow higher-level characters, then go
for it!

# Chapter 2: Character Classes

There are four possible character classes in the game: the Cleric, the Fighter, the Magic-user, and the Thief
(optional). The Referee is, of course, free to invent or allow other character classes in the game.

## The Cleric

Clerics are armored priests who serve a particular alignment, religion, or patron deity. Players may make up the details
if the Referee doesn’t use a particular mythology for the campaign. Mythologies and other details of a campaign world
often come later if the Referee is just starting.

The Cleric is a champion of his faith and moral alignment. The character might be a sinister witch-hunter, an exorcist
of demons, a shining knight of the faith, or a secret agent of some temple hierarchy. Since many of the Cleric’s
abilities are oriented toward healing and protecting, they tend to play a support role during combat.

However, they are able to stand shoulder-to-shoulder with the party’s Fighters if need be—at least for a while. Clerics
must be either Lawful (good) or Chaotic (evil). There are no Neutral Clerics unless the Referee decides otherwise.

### Cleric Abilities

#### WEAPON AND ARMOR RESTRICTIONS

Because of secret religious practices, Clerics may only use blunt weapons (club, flail, mace, etc.) and the only missile
weapon they are allowed is oil. Clerics have no armor restrictions.

#### SPELL CASTING

Clerics cast divine spells from a specific spell list, as per the Cleric Advance- ment table. Each day, the Cleric prays
for a certain set of spells, choosing any spells from the standard list. Clerics of specific gods might have entirely
different sets of spells as designed by the Referee, but the standard Cleric uses the standard spell list.

#### SAVING THROW

Clerics receive a +2 bonus on saving throws vs. poison and paralysis (unless the alternative “Saving Throw Matrix” is
used).

### Table 3: Cleric Advancement

| Level | Exp. Points  | Hit Dice  | To Hit  | ST  | S1  | S2  | S3  | S4  | S5  |
| :---: | :----------: | :-------: | :-----: | :-: | :-: | :-: | :-: | :-: | :-: |
| 1     | 0            | 1         | 0       | 15  | -   | -   | -   | -   | -   |
| 2     | 1500         | 2         | 0       | 14  | 1   | -   | -   | -   | -   |
| 3     | 3000         | 3         | 0       | 13  | 2   | -   | -   | -   | -   |
| 4     | 6000         | 3+1       | +1      | 12  | 2   | 1   | -   | -   | -   |
| 5     | 12000        | 4         | +1      | 11  | 2   | 2   | 1   | -   | -   |
| 6     | 24000        | 5         | +2      | 10  | 2   | 2   | 1   | 1   | -   |
| 7     | 48000        | 6         | +2      | 9   | 2   | 2   | 2   | 1   | 1   |
| 8     | 96000        | 6+1       | +3      | 8   | 2   | 2   | 2   | 2   | 2   |
| 9     | 192000       | 7         | +4      | 7   | 3   | 3   | 3   | 2   | 2   |
| 10    | 384000       | 8         | +5      | 6   | 3   | 3   | 3   | 3   | 3   |

#### TURN THE UNDEAD

Clerics can use their divine power to banish the undead, causing them to flee.

#### ESTABLISH TEMPLE

At tenth level, the rank of “Patriarch” is awarded. A Cleric, who chooses to build and dedicate a temple to a deity, may
attract a body of loyal followers who swear fealty to the character. If the Cleric changes alignment after establishing
a Temple, the character will lose any followers (and probably face a revolt).

#### EXPERIENCE BONUS FOR WISDOM

Wisdom is the Prime Attribute for Clerics. Clerics with Wisdom of 15 or higher receive a 10% to experience, 5% as
normal, and 5% because it is the Prime Attribute for the class.

#### TURN THE UNDEAD

Lawful clerics (only) have the ability to turn the undead, causing them to flee or destroying them outright. When a
turning attempt is made, roll 3d6 and consult the Turn Undead table for the result. One turn attempt may be made per
encounter.

If the result on the dice is equal to or greater than the number shown on the table, all undead creatures of the
targeted type are turned and will flee for 3d6 rounds (or cower helplessly if they are unable to flee). Optionally, the
Referee may rule that only 2d6 HD are turned, starting with the lowest to highest.

### Table 4: Turn Undead

| HD   | Examples | Lvl 1 | Lvl 2 | Lvl 3 | Lvl 4 | Lvl 5 | Lvl 6 | Lvl 7 | Lvl 8 | Lvl 9 | Lvl 10 |
| :--: | :------: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :----: |
| <1   | Skeleton | 10    | 7     | 4     | D     | D     | D     | D     | D     | D     | D      |
| 1    | Zombie   | 13    | 10    | 7     | 4     | D     | D     | D     | D     | D     | D      |
| 2    | Ghoul    | 15    | 13    | 10    | 7     | 4     | D     | D     | D     | D     | D      |
| 3    | Wight    | 17    | 15    | 13    | 10    | 7     | 4     | D     | D     | D     | D      |
| 4    | Wraith   | -     | 17    | 15    | 13    | 10    | 7     | 4     | D     | D     | D      |
| 5    | Mummy    | -     | -     | 17    | 15    | 13    | 10    | 7     | 4     | D     | D      |
| 6    | Spectre  | -     | -     | -     | 17    | 15    | 13    | 10    | 7     | 4     | D      |
| 7    | Vampire  | -     | -     | -     | -     | 17    | 15    | 13    | 10    | 7     | 4      |
| 8    | Vampire  | -     | -     | -     | -     | -     | 17    | 15    | 13    | 10    | 7      |
| 9    | Vampire  | -     | -     | -     | -     | -     | -     | 17    | 15    | 13    | 10     |
| 10   | Lich     | -     | -     | -     | -     | -     | -     | -     | 17    | 15    | 13     |

#### TURN UNDEAD (VARIANT)

Referees who wish to limit the use of the Turn Undead ability might try to count this as merely a 1st level Cleric
spell.

#### CHAOTIC CLERICS AND THE UNDEAD (OPTIONAL)

Many Referees allow Chaotic clerics as well as Lawful clerics to turn the Undead. If Chaotic clerics are allowed to turn
the undead, treat a result of “D” on the table a bit differently; instead of destroying the undead creature, the Chaotic
cleric has forced it into servitude. It will follow the character’s commands, although the Referee will decide the
limitations on this power. Perhaps the cleric can only keep a certain number of undead under control at one time (or a
certain number of hit dice) – the cleric’s level is a good number to use for this limit, whether it stands for a maximum
number of Undead creatures or for a maximum number of total hit dice.

## The Fighter

The Fighter is a warrior, trained for battle and in the use of armor and weapons. Your character might be a ferocious
Viking raider, a roaming samurai, or a medieval knight. Because they are the best equipped of all the character classes
to deal out and absorb damage, Fighters often end up on the front lines, going toe-to-toe with dragons, goblins, and
evil cultists. If you are the party’s Fighter, the down-and-dirty work is up to you.

### Fighter Abilities

#### WEAPON AND ARMOR RESTRICTIONS

Fighters are trained in warfare and, as such, have no restrictions on the kind of weapons or armor they can use.

#### COMBAT FURY

Against foes of one hit dice (HD) or fewer, Fighters get one attack per level each combat round.

#### SAVING THROW

Fighters receive a +2 bonus on saving throws vs. death and poison (unless the alternative “Saving Throw Matrix” is
used).


### Table 5: Fighter Advancement

| Level | Exp. Points  | Hit Dice  | To Hit  | ST  |
| :---: | :----------: | :-------: | :-----: | :-: |
| 1     | 0            | 1+1       | 0       | 14  |
| 2     | 2000         | 2         | +1      | 13  |
| 3     | 4000         | 3         | +2      | 12  |
| 4     | 8000         | 4         | +2      | 11  |
| 5     | 16000        | 5         | +3      | 10  |
| 6     | 32000        | 6         | +4      | 9   |
| 7     | 64000        | 7         | +4      | 8   |
| 8     | 128000       | 8         | +5      | 7   |
| 9     | 256000       | 9         | +6      | 6   |
| 10    | 512000       | 10        | +6      | 5   |

#### ESTABLISH STRONGHOLD

At ninth level, a Fighter who chooses to build a castle is considered to have reached the rank of “Baron” or “Baroness,”
bestowed by the local ruler or monarch. The character may choose to attract a body of soldiers, who will swear their
fealty as loyal followers.

#### EXPERIENCE BONUS FOR STRENGTH

Strength is the Prime Attribute for Fighters, which means that a Strength score of 15+ grants an additional 5%
experience.

## The Magic-User

The Magic-user is a mysterious figure, a student of arcane powers and dark magic. They can be devastating opponents.
However, at lower levels, they are quite vulnerable and must be protected by the other party members. As Magic-users
progress, they generally become the most powerful of the character classes—holding sway over the political destinies of
great kingdoms and able to create wondrous magical artifacts.

### Magic-user Abilities

#### WEAPON AND ARMOR RESTRICTIONS

Magic-users tend to spend their waking hours in study of arcane tomes and scrolls. As such, they have little time to
train with weapons or learn how to properly engage in physical combat. Magic-users may only wield daggers or staves, and
are not allowed the use of armor.

#### SPELL CASTING

Unlike the Cleric, the Magic-user owns a book of spells—which does not necessarily include all of the spells on the
standard lists. Reading from this book, the Magic-user presses a select spell formula into her mind, effectively
“preparing” it to be cast. Once a prepared spell is cast, the spell formulae disappears from the Magic-user’s mind, and
must be prepared again before another attempt can be made to cast it. However, it is possible to prepare a spell
multiple times using the available “slots” in the Magic-user’s memory. If the Magic-user finds spell scrolls during an
adventure, she can copy them into her spell book.

#### SAVING THROW

Magic-users receive a +2 bonus on saving throws vs. spells—including those cast from wands and staves (unless the
Alternate “Saving Throw Matrix” is used).

#### EXPERIENCE BONUS FOR INTELLIGENCE

Intelligence is the Prime Attribute for Magic-users, which means that an Intelligence score of 15+ grants them an
additional 5% to all experience points awarded.

#### ESTABLISH TOWER

At tenth level, a Magic-user gains the title of “witch”or “wizard”and can build a stronghold to house her library and
laboratory. She will attract a mix of mercenaries, strange servants and even a few monsters (like flying monkeys).  This
motley crew will swear fealty to her and serve her with whatever loyalty she can inspire.

### Table 6: Magic-User Advancement

| Level | Exp. Points  | Hit Dice  | To Hit  | ST  | S1  | S2  | S3  | S4  | S5  | S6  |
| :---: | :----------: | :-------: | :-----: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |
| 1     | 0            | 1         | 0       | 15  | 1   | -   | -   | -   | -   | -   |
| 2     | 2500         | 1+1       | 0       | 14  | 2   | -   | -   | -   | -   | -   |
| 3     | 5000         | 2         | 0       | 13  | 3   | 1   | -   | -   | -   | -   |
| 4     | 10000        | 2+1       | 0       | 12  | 4   | 2   | -   | -   | -   | -   |
| 5     | 20000        | 3         | +1      | 11  | 4   | 2   | 1   | -   | -   | -   |
| 6     | 40000        | 3+1       | +1      | 10  | 4   | 2   | 2   | -   | -   | -   |
| 7     | 80000        | 4         | +2      | 9   | 4   | 3   | 2   | 1   | -   | -   |
| 8     | 160000       | 4+1       | +2      | 8   | 4   | 3   | 3   | 2   | -   | -   |
| 9     | 320000       | 5         | +3      | 7   | 4   | 3   | 3   | 2   | 1   | -   |
| 10    | 640000       | 5+1       | +3      | 6   | 4   | 4   | 3   | 2   | 2   | -   |
| 11    | -            | 6         | +4      | 5   | 4   | 4   | 4   | 3   | 3   | -   |
| 12    | -            | 6+1       | +4      | 4   | 4   | 4   | 4   | 4   | 4   | 1   |

## The Thief (optional)

While there are many who wield sword and spell while exploring tombs and dungeons, the thief hopes to recover lost
riches through the use of guile and stealth. Though trained in arms, he is not a combatant by trade.Instead he strikes
from the shadows and his keen eyes see dangers that his companions do not. Thieves are rarely noble, but are more often
pragmatic professionals looking for opportunities to rapidly amass great wealth.  Still, their unique skill set makes
them very useful to dungeoneers and explorers and there are some among their ilk who hold to a code of “honor among
thieves.”

Thieves often go equipped with but a few weapons and light armor, and rely on picks and tools for survival. When combat
does arise, they fade into the darkness, ready to plunge a dagger into the backs of unsuspecting foes. The rare thief
who manages to amass fortune and glory is likely to retire and start a guild of others with similar skills.

### Thief Abilities

#### WEAPONS AND ARMOR RESTRICTIONS

Thieves are agile and skilled in combat. They may wield any weapon. However, they prefer to rely on their reflexes and
only wear leather armor.

#### BACK STAB

Any time a thief attacks an opponent who is unaware of their presence, the thief receives a +2 to their Base Hit Bonus.
If the attack is successful, the thief may roll his weapon damage twice.

#### SAVING THROW

Thieves receive a +2 bonus to any saving throw made to reduce or avoid the effects of any trap, magical or mundane.

#### THIEVERY

Thieves are stealthy and light-footed. The thievery ability may be used for any number of clandestine or stealth-based
actions. From picking pockets or locks, discovering and disarming traps, remaining silent and hidden while moving or
even climbing walls free-handed.The Referee has the final say as to whether an action falls under the purview of
thievery or not. When the thief attempts to use this ability the Referee rolls 1d6. If the result of the roll is equal
to or lower than their rating in the thievery ability, the thief is successful in their attempt. Thieves are not often
aware of when their attempts to remain stealthy fail, and the thief always believes he is successful.

#### ESTABLISH GUILD

A thief of ninth or higher level may choose to establish a secret hideout, often in an urban location. He will attract
the attention and service of other, lower-leveled, thieves who hope to learn from a master. He is known as a Guild
Master, and those who arrive are members of his Thieves Guild. In return for a cut of their earnings, the thief will
provide these apprentices with protection from local law enforcement as much as he is able.

#### EXPERIENCE BONUS FOR DEXTERITY

Dexterity is the Prime Attribute for Thieves, which means that a Dexterity score of 15+ grants an additional 5%
experience.

### Table 7: Thief Advancement

| Level | Exp. Points  | Hit Dice  | To Hit  | ST  | Thievery |
| :---: | :----------: | :-------: | :-----: | :-: | :------: |
| 1     | 0            | 1         | 0       | 14  | 2        |
| 2     | 1250         | 2         | 0       | 13  | 2        |
| 3     | 2500         | 3         | 0       | 12  | 2        |
| 4     | 5000         | 3+1       | +1      | 11  | 3        |
| 5     | 10000        | 4         | +1      | 10  | 3        |
| 6     | 20000        | 5         | +2      | 9   | 3        |
| 7     | 40000        | 6         | +2      | 8   | 4        |
| 8     | 80000        | 6+1       | +3      | 7   | 4        |
| 9     | 160000       | 7         | +4      | 6   | 4        |
| 10    | 320000       | 8         | +4      | 5   | 5        |

# Chapter 3: Character Races

In a fantasy world, humans often aren’t alone. Elves may populate the great forests, Dwarves may carve their halls and
mines into the heart of the earth, and Halflings may reside in the comfortable hill-houses of their bucolic shires.  By
contrast, some fantasy worlds depict an isolated human race pitted against ancient pre-human evils and the grim, savage
wilderness of worlds at the dawn (or dusk) of human civilization itself. Some fantasy worlds, as a third example,
accentuate the bizarre, with a wide variety of fantasy races available to the players—such worlds are filled with
conflict and contradictions, always with a new wonder to be found beyond the next corner.The Referee determines what
non-human races, if any, you can choose for your character.

## The Dwarf

Dwarves tend to live in underground cities. As such, Dwarves easily take note of certain features of stonework: sloping
corridors, moving walls, and traps made of stone (in particular: falling blocks, rigged ceilings, and tiny slits
designed to release arrows, darts, or poison gas). What the Dwarf does or does not perceive is for the Referee to decide
but for an optional die roll to use, see the Keen Detection racial ability.

### Dwarven Abilities

#### CHARACTER ADVANCEMENT

Dwarfs can choose between two classes, the Fighter or Thief (if used), and they are typically allowed to advance as high
as 6th level.

#### WEAPON AND ARMOR RESTRICTIONS

Like human Fighters, Dwarves have been trained in warfare and have no restrictions on the weapons or armor they may
use.

#### FIGHTING GIANTS

Giants, ogres, and similar giant-type creatures such as trolls are not good at fighting dwarves, and only inflict half
the normal damage against them.

#### KEEN DETECTION

Dwarves are good at spotting traps, slanting passages, and construction while underground (1-4 on a d6 when searching,
1-2 on a d6 if just passing by).

#### SAVING THROW

Dwarves do not use magic and are somewhat immune to its effects; they receive a +4 bonus on saving throws vs. magic
(whether or not the alternate “Saving Throw Matrix” is used).

#### LANGUAGES

For campaigns that give each race its own dialect, Dwarves should be able to speak with gnomes, goblins, orcs, and
kobolds.

## The Elf

The Referee can interpret Elves in many different ways. Are they faerie-folk of Irish legend, the Vanir of Norse
mythology, or perhaps something more akin to the Elves of Tolkien’s imagination?

As a baseline, most Elves are associated with magic as well as being skilled with the sword and bow. The Elven
adventurer may choose, on any given day—perhaps when the moon rises—whether to use the capabilities of the Fighter or
Magic-user. As a result, the Elf has two alternate advancement paths (experience points, hit dice, saving throws,
“to-hit” bonuses, etc.) depending upon whether he donned steel that day or summoned up the power to cast spells.

Elves must use a spell book to prepare spells, just as a Magic-user. Spells disap- pear from his casting capability once
they are cast, until prepared again.

### Elven Abilities

#### CHARACTER ADVANCEMENT

Elves may use either the Fighter or Magic-user class advancement charts, to be
announced at the start of an adventure. They are typically allowed to progress
to 4th level as Fighters and 8th as Magic-users.

#### WEAPON AND ARMOR RESTRICTIONS

When an Elf adventures as a Magic-user, the character has the same weapon
and armor limitations as a Magic-user. The exception to this would be magic
armor, which may still be worn even when the Elf is acting as a Magic-user.

#### HEREDITARY FOES

Elves gain an extra +1 (“to-hit” or to damage) when fighting goblins, orcs, in-
telligent undead, and lycanthropes. Elves are also immune to paralysis caused
by undead such as ghouls.

#### KEEN DETECTION

Elves are good at spotting hidden and concealed doors (1-4 on a d6 when searching, 1-2 on a d6 if just passing by).

#### LANGUAGES

For campaigns that give each race its own dialect, Elves should be able to speak with gnolls, goblins, orcs, and
hobgoblins.

## The Elf (Variant)

Some Referees may want to allow the Elf to advance as a blend of Fighter and Magic-user instead of switching back and
forth. In that model, the following advancement table might be used instead. Aside from Character Advancement and Weapon
and Armor Restrictions, Elven Race Abilities remain the same.

### Table 8: Elf Advancement

| Level | Exp. Points  | Hit Dice  | To Hit  | ST  | S1  | S2  | S3  |
| :---: | :----------: | :-------: | :-----: | :-: | :-: | :-: | :-: |
| 1     | 0            | 1+1       | 0       | 14  | -   | -   | -   |
| 2     | 5000         | 2         | +1      | 13  | 1   | -   | -   |
| 3     | 10000        | 2+1       | +1      | 12  | 2   | -   | -   |
| 4     | 20000        | 3         | +1      | 11  | 2   | 1   | -   |
| 5     | 40000        | 3+1       | +2      | 10  | 3   | 2   | -   |
| 6     | 80000        | 4         | +2      | 9   | 4   | 2   | -   |
| 7     | 160000       | 4+1       | +2      | 8   | 4   | 2   | 1   |
| 8     | 320000       | 5         | +2      | 7   | 4   | 2   | 2   |

#### WEAPON AND ARMOR RESTRICTIONS

Elves would have the advantage of both magic and armor at the same time, so the Referee may limit the Elf to chain mail.
Elves may not use two-handed weapons (two-handed sword, polearms, etc.) or shields while casting spells.

## The Halfling

Halflings are short, often stout, and live in shires, rustic communities that are usually remote from those of larger
folk. A few of them have a mildly adventurous spirit, enough to venture forth for a while at least, exploring the world
beyond the farms and fields of the local shire.

### Halfling Abilities

#### CHARACTER ADVANCEMENT

Halflings can choose between two classes, the Fighter or Thief (if used) and they are typically allowed to advance as
high as 4th as a fighter and 6th as a thief.

#### WEAPON AND ARMOR RESTRICTIONS

Like human Fighters, the Halfling has no weapon or armor restrictions.

#### FIGHTING GIANTS

Giants, ogres, and similar giant-type creatures such as trolls are not good at fighting small creatures such as
halflings and dwarves, and only inflict half the normal damage against them.

#### DEADLY ACCURACY WITH MISSILES

Halflings receive a +2 “to-hit” when firing missile weapons in combat.

#### NEAR INVISIBILITY

When not engaged in combat, Halflings can be quite stealthy, making themselves hard to spot and moving in almost total
silence. The success of this ability is determined by the Referee, or a die roll can be used, with a roll of 1-5 on a d6
indicating success.

#### SAVING THROW

Halflings are somewhat immune to magic, and receive +4 on saving throws vs. magic (even if the “Saving Throw Matrix” is
used).

#### LANGUAGES

For campaigns that give each race its own dialect, Halflings should be able to speak with creatures that fit the style
of the Referee’s campaign.

# Chapter 4: Equipment

Each character begins the game with 3d6x10 gold pieces, which is used to buy equipment. One gold piece (gp) is worth 10
silver pieces (sp) or 100 copper pieces (cp). Prices for equipment are listed on the tables below in the amount of gold
pieces. The Referee is encouraged to include additional items and equipment.

## Equipment Weight

A “normal” level of miscellaneous equipment is assumed to weigh 10 pounds.  If treasure is added to this, each coin and
gem is assumed to weigh 0.1 pound.

## Weapon Damage (optional)

In the original game, all weapons did 1d6 damage, regardless of type. White Box provides some slight variation. To play
the original game as it was written, simply ignore all variations in weapon damage – they all inflict 1d6 damage
regardless of whether the weapon is a two-handed sword or a dagger.

## Table 9: Adventuring Gear

| Item                       | Cost (gp) |
| :--:                       | :-------: |
| Backpack (30 lb. capacity) | 5         |
| Bedroll                    | 2         |
| Belladonna, bunch          | 10        |
| Bottle (wine), glass       | 1         |
| Case (map or scroll)       | 3         |
| Crowbar                    | 5         |
| Flint and Steel            | 5         |
| Garlic (1 lb.)             | 10        |
| Grappling Hook             | 5         |
| Hammer                     | 2         |
| Helmet                     | 10        |
| Holy Symbol, wooden        | 2         |
| Holy Symbol, silver        | 25        |
| Holy Water, small vial     | 25        |
| Lantern                    | 10        |
| Mirror (small), steel      | 5         |
| Oil (lamp), 1 pint         | 2         |
| Pole, 10 ft.               | 1         |
| Rations, trail (day)       | 1         |
| Rations, dried (day)       | 3         |
| Rope (50 ft.), hemp        | 1         |
| Rope (50 ft.), silk        | 5         |
| Sack (15 lb. capacity)     | 1         |
| Sack (30 lb. capacity)     | 2         |
| Shovel                     | 5         |
| Spellbook (blank)          | 100       |
| Spikes (12), iron          | 1         |
| Stakes (12), wooden        | 1         |
| Tent                       | 20        |
| Thieves’Tools              | 25        |
| Torches (6)                | 1         |
| Waterskin                  | 1         |
| Wolfsbane, bunch           | 10        |

## Table 10: Transportation

| Type                   | Cost (gp) |
| :--:                   | :-------: |
| Armor, horse (barding) | 320       |
| Bags, saddle           | 10        |
| Boat                   | 100       |
| Cart                   | 80        |
| Galley, large          | 30,000    |
| Galley, small          | 10,000    |
| Horse, draft           | 30        |
| Horse, light riding    | 40        |
| Mule                   | 20        |
| Raft                   | 40        |
| Saddle                 | 25        |
| Ship, sailing (large)  | 20,000    |
| Ship, sailing (small)  | 5,000     |
| Wagon, small           | 160       |
| Warhorse, heavy        | 200       |
| Warhorse, medium       | 100       |
| Warship                | 40,000    |

#### Important

Numbers for the “Ascending” armor class system always appear in brackets [like this].

## Calculating Armor Class

To calculate a character’s Armor Class, you must decide which system to use.  The two systems have the same armor
protection, but in one of them a high armor class is better, and in the other a low armor class is better. Just decide
which you prefer:

#### Descending AC System

In this system, an unarmored human has an AC of 9. The armor you buy lowers your AC and the lower the AC, the harder you
are to hit. To calculate your Armor Class, look at the Armor table above, in the “Effect on...” column. For whatever
type of armor you bought, subtract the number shown from your base AC of 9. That’s your new Armor Class.

#### Ascending AC System

For the ascending system, an unarmored person has an AAC of \[10\]. AAC means “ascending armor class,” and it is used in
brackets as a reminder of which system is in use. Your armor adds to your AAC, so the higher your AAC, the harder it is
for your enemies to hit you. To calculate your Ascending Armor Class, look at the Armor table, in the “Effect on...”
column. For whatever type of armor you bought, add the number shown in brackets to your base AAC of \[10\]. That’s
your new Armor Class.

#### Converting Armor Class Systems

To translate between the Ascending and Descending AC systems, the ascend- ing armor class (AAC) and descending armor
class (AC) values should add up to 19. Thus, AC 7 is the same as AAC \[12\] \(12+7=19\).

## Hiring Assistants

Many characters, particularly when first starting an adventuring career, are in need of hirelings to assist in carrying
loot or fighting monsters. Characters are free to create advertisements and inquire at local taverns to find available
hirelings. Prices are in gold pieces and are for one week of service.
These rates are for humans only. Demi-humans cost more to hire—and it may take more than just the promise of gold coins.

## Table 11: Melee Weapons

| Weapon                              | Damage | Weight | Cost (gp) |
| :----:                              | :----: | :----: | :-------: |
| Axe, battle\*                       | 1d6+1  | 15     | 7         |
| Axe, hand\‡                         | 1d6    | 5      | 3         |
| Club                                | 1d6    | 10     | -         |
| Dagger                              | 1d6-1  | 2      | 3         |
| Flail                               | 1d6    | 15     | 8         |
| Mace                                | 1d6    | 10     | 5         |
| Morning Star                        | 1d6    | 15     | 6         |
| Polearm (bardiche, halberd, etc.)\* | 1d6+1  | 15     | 7         |
| Spear\†\‡                           | 1d6    | 10     | 2         |
| Staff\*                             | 1d6    | 10     | 1         |
| Sword, long                         | 1d6    | 10     | 10        |
| Sword, short                        | 1d6-1  | 5      | 8         |
| Sword, two-handed\*                 | 1d6+1  | 15     | 15        |
| Warhammer                           | 1d6    | 10     | 5         |

\* Two-handed weapon

\† Can be used as either a one-handed or two-handed weapon

\‡ Can be used as either a melee or missile weapon

## Table 12: Hiring Assistants

| Hireling Type                         | Cost (gp) |
| :-----------:                         | :-------: |
| Alchemist                             | 250       |
| Animal Trainer                        | 125       |
| Armorer                               | 25        |
| Assassin                              | 500       |
| Blacksmith                            | 5         |
| Captain, Ship                         | 75        |
| Engineer                              | 200       |
| Horse rider                           | 3         |
| Non-combatant (servant, torch bearer) | 2         |
| Sage                                  | 500       |
| Sailor                                | 3         |
| Soldier                               | 5         |
| Spy                                   | 125       |

## Table 13: Missile Weapons

| Weapon              | Damage | Rate of Fire | Range  | Weight | Cost (gp) |
| :----:              | :----: | :----------: | :---:  | :----: | :-------: |
| Arrows (20)         | –      | –            | –      | 1      | 5         |
| Arrow, silver       | –      | –            | –      | 1      | 5         |
| Axe, hand           | 1d6    | 1            | 10 ft. | 10     | 3         |
| Bolt, crossbow (30) | –      | –            | –      | 5      | 5         |
| Bow, long           | 1d6    | 2            | 70 ft. | 5      | 40        |
| Bow, short          | 1d6-1  | 2            | 50 ft. | 5      | 25        |
| Case (30 bolts)     | –      | –            | –      | 1      | 5         |
| Crossbow, heavy     | 1d6+1  | 1/2          | 80 ft. | 5      | 25        |
| Crossbow, light     | 1d6-1  | 1            | 60 ft. | 5      | 15        |
| Pouch (20 stones)   | –      | –            | –      | 1      | 1         |
| Quiver (20 arrows)  | –      | –            | –      | 1      | 5         |
| Sling               | –      | 1            | 30 ft. | 1      | 2         |
| Spear               | 1d6    | 1            | 20 ft. | 10     | 2         |
| Stones (20)         | 1d6    | –            | –      | 1      | 1         |

\* Rate of Fire is the number of projectiles than can be fired per combat round

\† There is a +2 “to-hit” bonus for missile weapons utilized at short range (x1), a +1 “to-hit” bonus at medium range
(x2), and no bonus or penalty for long range (x3) attacks

## Table 14: Armor

| Armor      | Effect on AC [AAC] | Weight (lb.)\*  | Cost |
| :---:      | :----------------: | :-------------: | :--: |
| Chain mail | -4 [+4]            | 50              | 30   |
| Leather    | -2 [+2]            | 25              | 15   |
| Plate mail | -6 [+6]            | 75              | 50   |
| Shield     | -1 [+1]            | 10              | 10   |

\* At the Referee’s discretion, magical armor weighs either half of its normal weight or nothing at all

# Chapter 5: Playing the Game

Once characters have been created, the Referee will describe where the characters are and what they can see. The game
might start in a rural peasant village, a vast and teeming city spiked with towers and minarets, a castle, a tavern, or
at the gates of an ancient tomb. From that point on, Players describe what their characters do. Going down stairs,
attacking a dragon, talking to other Player Characters (PCs) or Non-Player Characters (NPCs) controlled by the Referee
—all of these kinds of actions are decided by the players. The Referee then describes what happens as a result: the
stairs lead down to a huge tomb, the dragon attacks the characters, etc. The rules below are guidelines handling events
like combat, gaining experience, movement, healing, dying, and other important parts of the game.

Basically, the player and the Referee work together, with the Referee handling the details of a dangerous fantasy world,
and the player handling what their character does in it. The epic story of a character’s rise to greatness (or death in
the effort) is the player’s to create in the Referee’s world.

## Gaining Experience

Characters are awarded Experience Points (XP) for killing monsters and accumulating treasure. When enough XP is earned,
the character raises a level.  Monsters have set Experience Point values in their descriptions, and one gold piece
acquired is equal to one XP. Experience is awarded for accumulating treasure because every gold piece gained by a
character is an index of his player’s skill. Awarding experience only for killing monsters fails to reward a party of
adventurers that successfully lures a dragon away from its hoard so that its treasure can be stolen without a fight— it
fails to reward characters that operate by intelligence, stealth, trickery, and misdirection.

Each character class has a Prime Attribute listed in its description, and the character creation process details how to
determine the character’s total XP bonus. There are two different ways of doing it, but each one results in a percent
bonus. If a character’s bonus is 10%, for example, and the Referee awards 1000xp to each character, that particular
character would get 1100xp (10% of 1000 is 100, so the character gets a bonus of 100xp).

## Time

The Referee will be required to make general rulings on the passage of time during the course of a campaign (e.g. “A few
hours later...”) and should be governed by common sense.There are, however, two important time measurements that merit
brief definitions--the “turn” and “combat round.” A turn (lasting ten minutes) is used to track and measure actions,
movement and resources when the intrepid adventurers are in dangerous places. The shorter combat round (lasting one
minute) represents the back and forth action of battle.

## Movement

Base movement rate for all races is calculated on the table below in tens of
feet per turn, allowing for two moves per turn.

Careful movement is used for exploring, checking for traps and hidden things. Running movement is used when you are
trying to get somewhere fast or get away from something wicked. Normal movement is pretty much everything else.

Combat movement is used when characters are engaged in battle and time has transitioned to Rounds instead of Turns.

## Outdoor Movement

Base movement rate is in miles per day, but can be doubled during a forced march. For outdoor combat round movement
rates, take the base rate, divide by three and multiply by 10 yards. For example a base movement rate of 9 normally
allows 9 miles of travel per day, 18 miles forced, and 30 yards of movement per combat round. The Referee should
decrease the normal rate of movement for travel over difficult terrain, like swamps or mountains.

### Table 15: Movement Rate

| Weight Carried (lb.) | Elf/Human | Dwarf/Halfling |
| :------------------: | :-------: | :------------: |
| 0–75                 | 12        | 9              |
| 76-100               | 9         | 6              |
| 101–150              | 6         | 3              |
| 151–300              | 3         | 3              |

### Table 16: Movement Rate Adjustments

| Movement Type | Adjustment            |
| :-----------: | :--------:            |
| Careful       | Half of Movement Rate |
| Normal        | Movement Rate         |
| Running       | Double Movement Rate  |
| Combat        | 1/3 Movement Rate     |

## Dungeon Doors

Dungeon doors are large, heavy and even unlocked are hard to open. Humans and Elves will open an unlocked door on a roll
of 1-2 in 6, while Dwarves and Halflings have a base 1 in 6 chance. At the Referee’s option, characters will apply any
strength bonus or penalty to this roll. It is assumed that most monsters can easily open doors, and that once opened,
doors will shut on their own.

## Light

Torches and lanterns illuminate a 30-foot radius. Torches burn for one hour (six turns), while lanterns burn one pint of
oil in four hours (24 turns). Players using a light source cannot normally surprise monsters, but they can of course
still be surprised. It is assumed that all monsters see in the dark, unless they are charmed or otherwise in the service
of players.

## Listening at Doors

Humans have a 1 in 6 chance of hearing noise, non-humans hear noise on a roll of 1-2 in 6. Note that success indicates
the player heard something, but they may not know what caused the sound.

## Secret Doors

Secret doors can be detected by any player who is actively searching for one with a roll of 1-2 on a d6. Elves, however,
find secret doors on a roll of 1-4 on a d6 when searching, or may sense with a 1-2 on a d6 chance that something is
amiss by merely passing by a secret door. It takes one turn for each 10’x10’ area searched.

## Traps

Most traps and pits are triggered on a roll of 1-2 on a d6 when any player passes over the triggering mechanism. Note
that Dwarves can detect stonework traps similarly to how Elves detect secret doors - they have a 1-4 on a d6 chance to
detect such traps if actively searching, or 1-2 in 6 if merely passing by. Players falling into a pit trap will take 1d6
damage per 10 feet fallen.

### Note on finding secret doors and traps

Ideally, players will be descriptive enough during a search that they will automatically find a trap or secret door. For
example, if moving a wall sconce opens a secret door, and the player says “I examine the sconces on the north wall for
anything unusual”, a Referee might automatically allow them to figure out how the secret door opens. If, however, they
merely state “I search the north wall for secret doors”, the Referee can require a die roll. Some features might be so
well hidden as to always merit a die roll, or at least a roll with some sort of adjustment.

## Combat

When the party of adventurers comes into contact with enemies, the order of events in the combat round is as follows:

1. The Referee determines if one side or the other is entitled to a free attack
or move as a result of surprise—this is either a judgement or a die roll of some
kind, depending on the circumstances. Normally, anyone is surprised if they
roll a 1 or a 2 on a d6.
2. Determine initiative. One roll is made for each side, not for each individual
in combat.
    - Party with initiative acts first (missile fire, casting spells, movement, melee attacks, etc.) and results take effect.
    - Party that lost initiative acts; results take effect.
    - The round is complete; roll initiative for the next round if the battle has not been resolved.

### Intent (optional)

Some Referees prefer to have all parties make a “statement of intent” before they roll initiative in order to force
players to decide what they’re doing before they know who goes first.

### How Initiative Works

At the beginning of the combat round, each side rolls initiative on a d6—high roll wins. The winning side acts first,
casting spells, moving, and attacking. The other side takes damage and casualties, and then has its turn.

Initiative rolls may result in a tie. When this happens, both sides are considered to be acting simultaneously. The
Referee may handle this situation in any way he chooses—with one caveat.  The damage inflicted by the combatants during
any simultaneous initiative is inflicted even if one of the combatants dies during the round. It is possible for two
combatants to kill each other in this situation. If you want to play things more simply, just re-roll initiative if
there is a tie until one side or the other wins the initiative.

### Alternate Combat Sequence (optional)

The sequence of battle is one of the most commonly house-ruled parts of the
game. Here is one possibility, also based on the Original Game:

1. Roll for surprise. Anyone not surprised gets a free round of combat in the normal order (see below). If neither side,
   or both sides, are surprised, move on to the next step.
2. Roll for initiative (one roll for each whole side), and follow the sequence described:
    - First, any “prepared” spells are cast. It takes a full combat round to prepare a spell, but one can be prepared
      ahead of time. Spellcasters who won the initia- tive cast their spells first. They may then start preparing a
      spell for the next round, if desired.

    - Next, missile weapons are fired (if the bow was already in the character’s hands and ready to go). This also
      happens in order of initiative.  Then melee combat takes place, in order of initiative. Finally, movement takes
      place, in order of initiative.

If it is important to find out which character in the party goes before another one, just compare their Dexterity scores
to see which is higher.

### The Attack Roll

To attack with a weapon, the player rolls a d20 and adds any bonuses to the result. These “to-hit” bonuses may include a
strength bonus (for attacks with hand held weapons), a dexterity bonus (for attacks with missile weapons), and any
bonuses for magic weapons. The player then subtracts any “to-hit” penalties they might have from their roll.

The attack roll is then compared to a table to see if the attack hits. If the attack roll is equal to or higher than the
number on the table, the attack hits. If you are using the Ascending AC system, rather than refer to these tables, there
is a quick method presented below that you can use. The Referee decides which will be used.

If an attack hits, it inflicts damage (as determined by the weapon’s damage die).  The damage is subtracted from the
defender’s hit point total (See “Damage and Death”).

### Ascending AC Combat – Quick Method

If you’re using the Ascending AC system, it may be easier to calculate your “to-hit” rolls according to a simple
formula. The numbers are the same as the Descending AC tables—this is just a different way of calculating the results.
Here’s how it’s done:

To attack with a weapon, the player rolls a d20 and adds any bonuses to the result. These “to-hit” bonuses now include
the character’s Base “to-hit” Bonus, as shown on the character Class table, and may include a strength bonus (for
attacks with handheld weapons), a dexterity bonus (for attacks with missile weapons), and any bonuses for magic weapons.
The player then subtracts any “to-hit” penalties they might have from their roll. If the result is equal to or greater
than the opponent’s Ascending AC, the attack hits.

Note: This quick system only works for the ascending AC system. In order to use this system, you’ll need to write down
your Base “to-hit” Bonus, and adjust it as you gain levels, but after doing that, you won’t have to check the table to
see if you score a hit. All you need to know is whether the total result was equal to or higher than the target’s armor
class.

### Damage and Death

When a character (or creature) is hit, the amount of damage taken is deducted from his hit points.  When hit points
reach 0, the character dies.

### Table 17: Cleric and Thief Attack Rolls (DAC[AAC])

| Cleric (Thief) Lvl | 9[10] | 8[11] | 7[12] | 6[13] | 5[14] | 4[15] | 3[16] | 2[17] | 1[18] | 0[19] |
| :----------------: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| 1–3                | 10    | 11    | 12    | 13    | 14    | 15    | 16    | 17    | 18    | 19    |
| 4–5                | 9     | 10    | 11    | 12    | 13    | 14    | 15    | 16    | 17    | 18    |
| 6–7                | 8     | 9     | 10    | 11    | 12    | 13    | 14    | 15    | 16    | 17    |
| 8                  | 7     | 8     | 9     | 10    | 11    | 12    | 13    | 14    | 15    | 16    |
| 9                  | 6     | 7     | 8     | 9     | 10    | 11    | 12    | 13    | 14    | 15    |
| 10                 | 5     | 6     | 7     | 8     | 9     | 10    | 11    | 12    | 13    | 14    |

### Table 18: Fighter Attack Rolls (DAC[AAC])

| Fighter Lvl | 9[10] | 8[11] | 7[12] | 6[13] | 5[14] | 4[15] | 3[16] | 2[17] | 1[18] | 0[19] |
| :---------: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| 1           | 10    | 11    | 12    | 13    | 14    | 15    | 16    | 17    | 18    | 19    |
| 2           | 9     | 10    | 11    | 12    | 13    | 14    | 15    | 16    | 17    | 18    |
| 3–4         | 8     | 9     | 10    | 11    | 12    | 13    | 14    | 15    | 16    | 17    |
| 5           | 7     | 8     | 9     | 10    | 11    | 12    | 13    | 14    | 15    | 16    |
| 6–7         | 6     | 7     | 8     | 9     | 10    | 11    | 12    | 13    | 14    | 15    |
| 8           | 5     | 6     | 7     | 8     | 9     | 10    | 11    | 12    | 13    | 14    |
| 9–10        | 4     | 5     | 6     | 7     | 8     | 9     | 10    | 11    | 12    | 13    |

### Table 19: Magic-User Attack Rolls (DAC[AAC])

| Magic-User Lvl | 9[10] | 8[11] | 7[12] | 6[13] | 5[14] | 4[15] | 3[16] | 2[17] | 1[18] | 0[19] |
| :------------: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| 1–4            | 10    | 11    | 12    | 13    | 14    | 15    | 16    | 17    | 18    | 19    |
| 5–6            | 9     | 10    | 11    | 12    | 13    | 14    | 15    | 16    | 17    | 18    |
| 7–8            | 8     | 9     | 10    | 11    | 12    | 13    | 14    | 15    | 16    | 17    |
| 9–10           | 7     | 8     | 9     | 10    | 11    | 12    | 13    | 14    | 15    | 16    |

### A Less Harsh Method (optional)

Referees have different ideas concerning how lethal a game should be. For this reason, many allow characters to become
“unconscious” at 0 HP, with death staved off until the character reaches some predetermined negative number.

For example, many Referees employ a house rule which allows a character’s HP to fall below 0 by as many points as their
level before the character dies; a 4th level character might die only if he falls below -4 HP. Generally, though, a
character with fewer than 0 HP is considered to be bleeding to death at 1 HP per round until the wounds are bound up.

### Healing

In addition to the various magical means of restoring HP, a character will recover 1 full Hit point per day of
uninterrupted rest. Four weeks of rest will restore all of a character’s HP—regardless of how many the character lost.

### Binding Wounds (optional)

Referees can allow characters to bind 1d6 HP worth of wounds following a battle. This is of particular use in low-magic
campaigns or in adventures where none of the Players has chosen to run a Cleric.

Note that the character can only recover HP lost during this particular battle.  Recovered HP cannot cause a character
to have more hit points than normal.  Some Referees view hit points as an abstract measurement of a character’s stamina
with the attack that brings a character to zero hit points representing an actual significant blow from a weapon. As
such, they may allow characters to heal more hit points per day of rest.

### Invisible Opponents

An invisible opponent can only be attacked if its general location is known, and the attack suffers a -4 penalty
“to-hit.” Note that more powerful monsters (those with sensitive smell, hearing, or those with more than 6 HD) will
frequently be able to detect invisible opponents; the Referee should determine the chance of this based on the creature
and the situation.

### Oil

Thrown oil that is subsequently set alight by a thrown torch or similar will burn for two rounds, doing 1d6 damage per
round on a successful hit of AC 8 \[11\]. The Referee should adjust the chance to hit based on the size of the creature
being targeted, obviously large creatures such as Ogres and Giants will be easier to hit than, say, giant spiders.

### Melee Attack

A melee attack is an attack with a hand-held weapon such as a sword, spear, or dagger. Two combatants within ten feet of
each other are considered to be “in melee.”

### Missile Attack

Missile attacks are with ranged weapons such as crossbows, slings, or thrown axes. When using missiles to fire into
melee, it is not possible to choose which opponent (or friend!) will receive the brunt of the attack.

### Morale

Certain monsters, such as mindless or undead creatures, are fearless and will always fight to the death. The majority,
however, will not continue to fight a hopeless battle and will seek to retreat, surrender, or flee. The Referee will
decide when monsters abandon battle and retreat, based upon the situation and the monster’s intelligence. Referees
should also use morale to determine the actions and loyalty of hirelings or other companion NPCs.

If there is uncertainty about the morale of NPC’s or monsters, the following table can be used at the Referee’s option.

### Table 20: Morale Check

| Roll      | Morale              |
| :--:      | :----:              |
| 2 or less | Surrender           |
| 3-5       | Flee                |
| 6-8       | Hold                |
| 9-11      | Fight for advantage |
| 12        | Attack!             |

### Negotiation and Diplomacy

Some combats can be averted with a few well-chosen words (even lies). If the party is outmatched, or the monsters don’t
seem likely to be carrying much in the way of loot, the party might elect to brazen their way through in an attempt to
avoid combat or at least delay it until more favorable conditions arise.

### Jousting

A joust is a contest in which two fighters ride at one another while wielding lances. The object is to make contact with
the opponent’s armor or shield that is hard enough to break the lance or to actually unhorse your opponent.

### Table 21: Jousting Points

| Condition                   | Points |
| :-------:                   | :----: |
| Attack Roll of 12 or Higher | 1      |
| Inflict Damage              | 2      |
| Unhorse Opponent            | 3      |

The joust, as a sport, consisted of three tilts. In White Box, this equates to three rounds of combat. Each round
consists of an attack with a lance. If the lances are blunted, they deal 1d6-1 points of damage, but cannot kill the
jouster. At 0 hit points, the jouster is knocked unconscious and falls from his steed (no additional damage). If the
lances are not blunted, they deal normal damage and might kill.

Any jouster that suffers damage must pass a Saving Throw or be knocked from his steed and suffer 1d6 damage from the
fall.

Medium war horses grant a +1 bonus to attack. Heavy war horses grant a +2.

The jouster with the most points after three rounds wins the joust. If one opponent is knocked unconscious or killed,
they automatically lose the joust.  In a duel, the loser must forfeit his mount and his armor, and permit himself to be
ransomed for as much as his companions or family can pay, usually commensurate with the fighter’s social class.

### Spells

Spell casting begins at the start of the combat round. It is possible to prepare a spell while within melee range of an
opponent (10 feet) but if the caster suffers damage while preparing a spell, the spell is lost. Unless stated otherwise,
the spell is cast (takes effect) in the caster’s initiative phase.

Note that in the Alternative Sequence of Combat, spell casting works differently. In that system, it takes a full round
to prepare a spell, the spell is cast at the beginning of the round before anything else happens, and the caster may
spend the rest of the round preparing a new spell (or doing something else such as moving or even fighting).

### Loyalty

The Referee may wish to make “loyalty checks” for NPCs put into dangerous situations or ones offered bribes to change
sides during a conflict. When a loyalty check is made, roll 3d6 and consult the Loyalty table for the result.  Remember
that these checks can be modified by a Player’s Charisma score.

Good treatment, respect, and a fair share of treasure earned should garner bonuses to loyalty checks, while abuse,
physical or otherwise, will bring about penalties, abandonment, or worse.  NPCs should be treated as distinct
individuals, and not mere extensions of the Player Character.

If an NPC is in a situation in which a Morale check may be appropriate, the Loyalty modifier can be applied to the
Morale roll (optional).

### Table 22: Loyalty

| Roll  | Loyalty                  |
| :--:  | :-----:                  |
| 3     | Traitor                  |
| 4–5   | -2 on next loyalty check |
| 6–8   | -1 on next loyalty check |
| 9–12  | Average                  |
| 13–15 | +1 on next loyalty check |
| 16–17 | +2 on next loyalty check |
| 18    | Loyalist                 |

### Saving Throws

From time to time, a spell or some other kind of hazard requires you to make a “saving throw.” A successful saving throw
means that the character avoids a threat or lessens its effect. Each character class has a saving throw target number
which gets lower as the character gains levels. To make a saving throw, roll a d20. If the result is equal to or greater
than the character’s saving throw target number, the saving throw is successful.

### Alternate Rule: The Saving Throw Matrix

The White Box saving throw system is an adaptation of the original, which had several categories of different risks
instead of a single basic saving throw as used here. Numbers inspired by the original system are given below (the Single
Saving Throw number is provided for comparison):

#### Table 23: Cleric Saving Throws

| Level | Death/Poison | Wands/Rays | Paralyze/Stone | Dragon Breath | Spells/Staffs | Single ST |
| :---: | :----------: | :--------: | :------------: | :-----------: | :-----------: | :-------: |
| 1     | 11           | 12         | 14             | 16            | 15            | 15        |
| 2     | 10           | 11         | 13             | 15            | 14            | 14        |
| 3     | 10           | 11         | 13             | 15            | 14            | 13        |
| 4     | 9            | 10         | 12             | 14            | 13            | 12        |
| 5     | 9            | 10         | 12             | 14            | 13            | 11        |
| 6     | 8            | 9          | 11             | 13            | 12            | 10        |
| 7     | 8            | 9          | 11             | 13            | 12            | 9         |
| 8     | 7            | 8          | 10             | 12            | 11            | 8         |
| 9     | 7            | 8          | 10             | 12            | 11            | 7         |
| 10    | 6            | 7          | 9              | 11            | 10            | 6         |

#### Table 24: Fighter Saving Throws

| Level | Death/Poison | Wands/Rays | Paralyze/Stone | Dragon Breath | Spells/Staffs | Single ST |
| :---: | :----------: | :--------: | :------------: | :-----------: | :-----------: | :-------: |
| 1     | 12           | 13         | 14             | 15            | 16            | 14        |
| 2     | 12           | 13         | 14             | 15            | 16            | 13        |
| 3     | 10           | 11         | 12             | 15            | 14            | 12        |
| 4     | 10           | 11         | 12             | 12            | 14            | 11        |
| 5     | 10           | 11         | 12             | 12            | 14            | 10        |
| 6     | 8            | 9          | 10             | 12            | 12            | 9         |
| 7     | 8            | 9          | 10             | 12            | 12            | 8         |
| 8     | 8            | 9          | 10             | 9             | 12            | 7         |
| 9     | 6            | 7          | 8              | 9             | 10            | 6         |
| 10    | 6            | 7          | 8              | 9             | 10            | 5         |

#### Table 25: Magic-User Saving Throws

| Level | Death/Poison | Wands/Rays | Paralyze/Stone | Dragon Breath | Spells/Staffs | Single ST |
| :---: | :----------: | :--------: | :------------: | :-----------: | :-----------: | :-------: |
| 1     | 13           | 14         | 13             | 16            | 15            | 15        |
| 2     | 12           | 13         | 12             | 15            | 15            | 14        |
| 3     | 12           | 13         | 12             | 15            | 15            | 13        |
| 4     | 11           | 12         | 11             | 14            | 12            | 12        |
| 5     | 11           | 12         | 11             | 14            | 12            | 11        |
| 6     | 10           | 11         | 10             | 13            | 12            | 10        |
| 7     | 10           | 11         | 10             | 13            | 12            | 9         |
| 8     | 9            | 10         | 9              | 12            | 9             | 8         |
| 9     | 9            | 10         | 9              | 12            | 9             | 7         |
| 10    | 8            | 9          | 8              | 11            | 9             | 6         |

#### Table 26: Thief Saving Throws

| Level | Death/Poison | Wands/Rays | Paralyze/Stone | Dragon Breath | Spells/Staffs | Single ST |
| :---: | :----------: | :--------: | :------------: | :-----------: | :-----------: | :-------: |
| 1     | 13           | 14         | 13             | 15            | 16            | 14        |
| 2     | 12           | 14         | 12             | 14            | 15            | 13        |
| 3     | 12           | 14         | 12             | 14            | 15            | 12        |
| 4     | 11           | 11         | 11             | 13            | 14            | 11        |
| 5     | 11           | 11         | 11             | 13            | 14            | 10        |
| 6     | 10           | 11         | 10             | 12            | 13            | 9         |
| 7     | 10           | 11         | 10             | 12            | 13            | 8         |
| 8     | 9            | 8          | 9              | 11            | 12            | 7         |
| 9     | 9            | 8          | 9              | 11            | 12            | 6         |
| 10    | 8            | 8          | 8              | 10            | 11            | 5         |

This table does not integrate directly into White Box: if you were to house-rule it in, you would have to eliminate the
White Box class bonuses on saving throws (i.e., the bonuses listed for each class in the class description but not
racial bonuses for non-humans).

#### Remember

White Box is a free-form roleplaying game, meaning that there aren’t very many rules. The Referee is responsible for
handling situations that aren’t covered by the rules, making fair evaluations of what the characters do and deciding
what happens as a result. This is not a game in which the players are “against” the Referee, even though the Referee is
responsible for creating tricky traps, dangerous situations, and running the monsters and other foes the PCs will
encounter during the game. In fact, the players and the Referee cooperate with each other to create a fantasy epic, with
the Referee creating the setting and the players developing the story of the heroes. If they aren’t skillful and smart,
the epic might be very short. But it’s not the Referee’s job to defeat the players—it’s his job to provide interesting
(and dangerous) challenges, and then guide the story fairly.

# Chapter 6: Magig & Spells

Clerics and Magic-Users are the only classes that can cast spells. Spells have three phases. First they are memorized (a
character can only memorize a certain number of spells to bring along on an adventure – see the description of the
character classes to find out how many, and what level). Second, they are prepared, which is the process of saying the
magic words and making the magic gestures. Third, the spell is cast, which means it takes effect and the magic happens.
In the normal sequence of combat, the caster starts preparing at the beginning of the round and casts the spell whenever
his/her side has the initiative. In the alternative combat sequence, it takes a whole round to prepare a spell, but
spells are cast at the beginning of the next round, and the caster can then take other actions during the round,
including preparing a spell for the next round.

## Cleric Spell List

### Table 27: Cleric Spells

| Level 1                         | Level 2            |
| :-----:                         | :-----:            |
| Cure (Cause) Light Wounds       | Bless (Curse)      |
| Detect Chaos (Law)              | Find Traps         |
| Detect Magic                    | Hold Person        |
| Light (Dark)                    | Speak with Animals |
| Protection from Chaos (Law)     | -                  |
| Purify (Putrefy) Food and Drink | -                  |

| Level 3                 | Level 4                                    |
| :-----:                 | :-----:                                    |
| Cure (Cause) Disease    | Cure (Cause) Serious Wounds                |
| Light (Dark), Continual | Neutralize Poison                          |
| Locate Object           | Protection from Chaos (Law), 10 ft. radius |
| Remove Curse            | Speak with Plants                          |
| -                       | Sticks to Snakes                           |

| Level 5               | Level 6            |
| :-----:               | :-----:            |
| CommuneLevel          | 6 Cleric Spells?   |
| Create Food and Drink | Only the Gods Know |
| Dispel Chaos (Law)    | -                  |
| Insect Plague         | -                  |
| Quest                 | -                  |
| Raise Dead            | -                  |

\* The spell name in parenthesis indicates the chaotic incarnation of the spell—the
consequences of Lawful Clerics using chaotic spells is determined by the Referee.

## Magic-User Spell List

### Table 28: Magic-User Spells

| Level 1               | Level 2               |
| :-----:               | :-----:               |
| Charm Person          | Detect Chaos          |
| Detect Magic          | Detect Invisibility   |
| Hold Portal           | Detect Thoughts (ESP) |
| Light                 | Invisibility          |
| Protection from Chaos | Knock                 |
| Read Languages        | Levitate              |
| Read Magic            | Light, Continual      |
| Sleep                 | Locate Object         |
| -                     | Phantasmal Force      |
| -                     | Web                   |
| -                     | Wizard Lock           |

| Level 3                              | Level 4               |
| :-----:                              | :-----:               |
| Alter Time                           | Charm Monster         |
| Crystal Ball                         | Confusion             |
| Darkvision                           | Dimension Portal      |
| Dispel Magic                         | Hallucinatory Terrain |
| Fireball                             | Massmorph             |
| Fly                                  | Plant Growth          |
| Hold Person                          | Polymorph             |
| Invisibility, 10 ft. radius          | Remove Curse          |
| Lightning Bolt                       | Wall of Fire or Ice   |
| Protection from Chaos, 10 ft. radius | Wizard Eye            |
| Protection from Normal Missiles      | -                     |
| Water Breathing                      | -                     |

| Level 5               | Level 6               |
| :-----:               | :-----:               |
| Animal Growth         | Anti-Magic Shell      |
| Animate Dead          | Control Weather       |
| Cloudkill             | Death Spell           |
| Conjure Elemental     | Disintegrate          |
| Contact Other Plane   | Invisible Stalker     |
| Feeblemind            | Move Earth            |
| Hold Monster          | Move Water            |
| Magic Jar             | Project Image         |
| Passwall              | Quest                 |
| Telekinesis           | Reincarnation         |
| Teleport              | Transform Stone-Flesh |
| Transform Rock-Mud    | -                     |
| Wall of Stone or Iron | -                     |

## Spell Descriptions

Contained herein are all of the Cleric and Magic-user spells, in alphabetical order. The chaotic-version spells in
parentheses are available to Clerics only.

### Alter Time

**Level**: M3

**Range**: 240 ft.

**Duration**: 30 min.

The caster must announce which of the two options is being cast. Both options have a 60 foot radius area of effect:

- As a Haste spell, as many as 24 creatures may move and attack at double normal speed.
- As a Slow spell, as many as 24 creatures failing a saving throw may only move and attack at half speed.

### Animal Growth

**Level**: M5

**Range**: 120 ft.

**Duration**: 2 hours

This spell causes 1d6 normal creatures to grow immediately to giant size.The affected creatures can attack as per a
giant version of the normal animal.

### Animate Dead

**Level**: M5

**Range**: Referee’s discretion

**Duration**: Permanent

This spell animates skeletons or zombies from dead bodies. 1d6 undead are animated (per level of the caster above 8th).
The corpses remain animated until destroyed or dispelled.

### Anti-Magic Shell

**Level**: M6

**Range**: Caster

**Duration**: 2 hours

An invisible bubble of force surrounds the caster, impenetrable to magic.  Spells and other magical effects cannot pass
into or out of the shell.

### Bless (Curse)

**Level**: C2

**Range**: Target PC or NPC (out-of-combat)

**Duration**: 1 hour

The caster must announce which of the two options is being cast. The recipient of this spell cannot be in combat when it
is cast.

- As a Bless spell, the recipient is granted a +1 bonus to all attack rolls and, if the target is not a Player
  Character, improves overall morale.
- As a Curse spell, the recipient is cursed with a -1 penalty to all attack rolls and, if the target is not a Player
  Character, suffers a decrease to morale.

### Charm Monster

**Level**: M4

**Range**: 60 ft.

**Duration**: Until dispelled

This spell operates in the same manner as Charm Person, but can affect more powerful monsters. Up to 3d6 monsters of
fewer than 3 HD are affected.

### Charm Person

**Level**: M1

**Range**: 120 ft.

**Duration**: Until dispelled

This spell affects living bipeds of human size or smaller, such as goblins or dryads. If the spell succeeds (saving
throw allowed), the unfortunate creature falls under the caster’s influence.

### Cloudkill

**Level**: M5

**Range**: Close

**Duration**: 1 hour

Foul and poisonous vapors boil from the thin air, forming a cloud 15 feet in radius. The cloud moves away from the
caster at a rate of 6 feet per minute unless its direction or speed is affected by winds. Unusually strong gusts can
dissipate and destroy it. Poison-laden, the horrid mist is heavier than air, and thus sinks down any pits or stairs in
its path. This spell affects only creatures with 5 or fewer HD.

### Commune

**Level**: C5

**Range**: Caster

**Duration**: 3 questions

Higher powers grant answers to three questions the caster poses. Higher powers don’t like being constantly interrogated
by mere mortals, so the spell should be limited to one casting per week. The Referee may rule that a caster may cast a
double strength Commune spell composed of six questions once per year.

### Confusion

**Level**: M4

**Range**: 120 ft.

**Duration**: 2 hours

This spell confuses people and monsters, making them act randomly. The effects of the confusion may shift every 10
minutes, and the dice are rolled again to determine any change. Roll 2d6 to determine the creature’s behavior:

#### Table 29: Confusion Reaction

| Roll | Reaction                           |
| :--: | :------:                           |
| 2–5  | Attack the caster (and his allies) |
| 6–8  | Stand baffled and inactive         |
| 9–12 | Attack each other                  |

The spell affects 2d6 creatures, plus an additional creature for every caster level above 8th. Creatures of 3 HD or
fewer are automatically affected by the spell, and it takes effect immediately. Creatures of 4 HD or more automatically
resist the confusion effect until it reaches its full power, which takes a number of minutes equal to 1d4. These
creatures must make a saving throw when the effect reaches full power and every 10 minutes thereafter or become confused
for the remainder of the spell’s duration.

### Conjure Elemental

**Level**: M5

**Range**: 240 ft.

**Duration**: Until dispelled

The caster summons a 16 HD elemental (any kind) from the elemental planes of existence, and binds it to his commands.
Each type of elemental may only be summoned once per day. The elemental obeys the caster only for as long as the caster
concentrates on it; when the caster ceases to concentrate, even for a moment, the elemental is released and will attack.

### Contact Other Plane

**Level**: M5

**Range**: None

**Duration**: See table

The caster creates a mental contact with the planes, forces, powers, and geometries of the beyond, in order to gain
affirmative or negative answers to the questions he contemplates. The spell’s effect depends on how deeply the caster
wishes to delve into the planes of existence. Roll a d20 on the table below.

#### Table 30: Contact Other Plane

| Plane\* | Insanity\† | Right | Wrong\‡ |
| :-----: | :--------: | :---: | :-----: |
| 1       | 1–2        | 3–11  | 12–20   |
| 2       | 1–4        | 4–13  | 14–20   |
| 3       | 1–6        | 7–16  | 16–20   |
| 4       | 1–8        | 9–17  | 18–20   |
| 5       | 1–10       | 11–18 | 19–20   |
| 6       | 1–12       | 13–19 | 20      |

\* Planes are the “depth” at which the caster chooses to seek the truth; number of Yes/No questions asked

\† Temporary insanity lasts for as many weeks equal to the depth of the plane where the caster’s sanity failed

\‡ This represents the possibility of being mislead or misinterpreting an answer

### Control Weather

**Level**: M6

**Range**: Referee’s discretion

**Duration**: Referee’s discretion

The caster can summon or stop rainfall, tornadoes, create unusually high or low temperatures, clear the sky of clouds
or summon clouds into being.

### Create Food and Drink

**Level**: C5

**Range**: Close

**Duration**: Instantaneous

This spell creates a one-day supply of simple food and drinking water for 24 humans (or horses, which drink the same
amount as a man for game purposes).

### Crystal Ball

**Level**: M3

**Range**: 60 ft.

**Duration**: 2 hours

The caster must announce which of the two options is being cast. A two foot thickness of solid stone or a thin layer of
lead blocks both options.

- As a Clairaudience spell, the caster can hear through solid obstacles.
- As a Clairvoyance spell,the caster can see through solid obstacles.

### Cure (Cause) Disease

**Level**: C3

**Range**: Touch

**Duration**: Instantaneous

As a Cure Disease spell, the recipient is cured of all diseases—including those magically inflicted.

As a Cause Disease spell, the recipient is inflicted with a disease to be determined by the Referee.

### Cure (Cause) Light Wounds

**Level**: C1

**Range**: Touch

**Duration**: Instantaneous

- As a Cure Light Wounds spell, the caster cures 1d6+1 HP.
- As a Cause Light Wounds spell, the caster causes 1d6+1 damage.

### Cure (Cause) Serious Wounds

**Level**: C4

**Range**: Touch

**Duration**: Instantaneous

- As a Cure Serious Wounds spell, the caster cures 3d6+3 HP.
- As a Cause Serious Wounds spell, the caster causes 3d6+3 damage.

## Darkvision

**Level**: M3

**Range**: Touch

**Duration**: 1 day

The recipient of the spell can see in total darkness. The recipient should roll 1d6 on the table to determine the range
of his vision.

#### Table 31: Darkvision Range

| Roll d6 | Range of Vision (ft.) |
| :-----: | :-------------------: |
| 1–2     | 40                    |
| 3–4     | 50                    |
| 5–6     | 60                    |

### Death Spell

**Level**: M6

**Range**: 240 ft.

**Duration**: Permanent

Within a 60 foot radius, up to 2d8 creatures with fewer than 7 HD perish.

Detect Chaos (Law)

Level: C1, M2
Range: 120 ft. (C), 60 ft. (M)
Duration: 1 hour (C), 20 min. (M)

- As a Detect Chaos spell, the caster detects creatures of Chaos, or those with chaotic enchantments, intentions,
  thoughts, or auras within the spell’s range. Poison is not inherently evil or chaotic and cannot be detected by means
  of this spell.
- As a Detect Law spell, the spell works exactly like Detect Chaos except that it detects Law.

### Detect Invisibility

**Level**: M2

**Range**: 10 ft./caster level

**Duration**: 1 hour

Caster can perceive invisible creatures and objects.

### Detect Magic

**Level**: C1, M1

**Range**: 60 ft.

**Duration**: 20 min.

The caster can perceive in places, people, or things the presence of a magical spell or enchantment. Magical items or
secretly placed charms may be discovered with this spell.

### Detect Thoughts (ESP)

**Level**: M2

**Range**: 60 ft.

**Duration**: 2 hours

The caster can detect the thoughts of other beings. The spell cannot penetrate more than two feet of stone and is
blocked by even a thin sheet of lead.

### Dimensional Portal

**Level**: M4

**Range**: 10 ft. casting, 360 ft. teleport

**Duration**: Instantaneous

Dimensional Portal is a weak form of the Teleport spell that can be managed by lesser magicians. The caster can teleport
himself, an object, or another person with perfect accuracy to the stated location, as long as it is within the spell’s
given range.

### Disintegrate

**Level**: M6

**Range**: 60 ft.

**Duration**: Permanent

The caster defines one specific target such as a door, a peasant, or a statue, and it disintegrates into dust. Magical
materials are not disintegrated, and living creatures (such as the aforementioned peasant) are permitted a saving throw.
The Disintegrate spell cannot be dispelled.

### Dispel Chaos (Law)

**Level**: C5

**Range**: 30 ft.

**Duration**: 10 minutes

- As a Dispel Chaos spell, the spell is similar to the arcane spell Dispel Magic, but works against items, spells, or
agents of Chaos. However, unlike Dispel Magic, this spell also functions against chaotic sendings, possibly including
dreams or supernatural hunting-beasts.
- As a Dispel Law spell, the spell works exactly like Dispel Chaos, except that it will dispel Law.

### Dispel Magic

**Level**: M3

**Range**: 120 ft.

**Duration**: 10 min.

Dispel magic can be used to completely dispel most spells and enchantments.

As an example of how one might referee this spell, the chance of dispelling magic could be a percentage based on the
ratio of the level of the caster trying to dispel over the level of the caster (or HD of the monster) who hurled the
original magic.

Thus, a 6th level Magic-user attempting to dispel a charm cast by a 12th level Magic-user has a 50% chance of success (6
/ 12 = 1/2). If the 12th level Magic-user were dispelling the 6th level Magic-user’s charm, the dispelling caster’s
chance of success would be 200% (12 ∕ 6 = 2).

### Feeblemind

**Level**: M5

**Range**: 240 ft.

**Duration**: Permanent until dispelled

Feeblemind is a spell that affects only Magic-users. The saving throw against the spell is made at a -4 penalty, and if
the saving throw fails, the targeted Magic-user becomes feebleminded until the magic is dispelled.

### Find Traps

**Level**: C2

**Range**: 30 ft.

**Duration**: 20 min.

Find Traps can allow the caster to perceive both magical and non-magical traps at a distance of 30 feet.

### Fireball

**Level**: M3

**Range**: 240 ft.

**Duration**: Instantaneous

A missile shoots from the caster’s finger to explode at the targeted location in a furnace-like blast of fire. It has a
burst radius of 20 feet and damage is 1d6 per level of the caster. The blast shapes itself to the available volume.  A
successful saving throw means that the target only takes half damage.

### Fly

**Level**: M3

**Range**: Touch

**Duration**: 1d6 turns + 1 turn/level

This spell grants the power of flight, with a movement rate of 120 feet per round. The Referee rolls for the duration of
the spell in secret and does not disclose this information to the Player.

### Hallucinatory Terrain

**Level**: M4

**Range**: 240 ft.

**Duration**: Until touched by an enemy or dispelled

This spell changes the appearance of the terrain into the semblance of what the caster desires. A hill can be made to
disappear, or could be replaced with an illusory forest, for example.

### Hold Monster

**Level**: M5

**Range**: 120 ft.

**Duration**: 1 hour + 10 minutes per level

The caster can target either 1d4 creatures (saving throw applies) or may instead target a single creature who must make
their save at a -2 penalty.

### Hold Person

**Level**: C2, M3

**Range**: 180 ft. (C), 120 ft. (M)

**Duration**: 90 min. (C), 1 hour + 10 minutes per level (M)

The caster can target either 1d4 persons of human size or smaller (saving throw applies) or may instead target a single
creature who must make their save at a -2 penalty.

### Hold Portal

**Level**: M1

**Range**: Referee’s discretion

**Duration**: 2d6 turns

This spell magically holds a door or gate in position for the spell’s duration (or until dispelled). Creatures with
magic resistance can shatter the spell without much effort.

### Insect Plague

**Level**: C5

**Range**: 480 ft.

**Duration**: 1 day

This spell only works outdoors. A storm of insects gathers, and goes wherever the caster directs. The cloud is
approximately 400 square feet (20 × 20 feet, with roughly corresponding height). Any creature with fewer than 2 HD
exposed to the cloud of insects will flee in terror (no saving throw).

### Invisibility

**Level**: M2

**Range**: 240 ft.

**Duration**: Until dispelled or an attack is made

The object of this spell, whether a person or a thing, becomes invisible to both normal vision and to darkvision. If
the Referee is using the invisibility rules unchanged, the result is that an invisible creature cannot be attacked
unless its approximate lo- cation is known, and all attacks are made at -4 to-hit.

### Invisibility, 10 ft. radius

**Level**: M3

**Range**: 240 ft.

**Duration**: Until dispelled or an attack is made

Identical to the Invisibility spell, which makes the target creature or object invisible to normal vision and to dark
vision, except that it also throws a sphere of invisibility (with a 10 foot radius) around the recipient, which moves
along with the target.

### Invisible Stalker

**Level**: M6

**Range**: Near Caster

**Duration**: Until mission is completed

This spell summons an Invisible Stalker with 8 HD. The stalker will perform one task as commanded by the caster,
regardless of how long the task may take or how far the stalker may have to travel. The stalker cannot be banished by
means of Dispel Magic; it must be killed in order to deter it from its mission.

### Knock

**Level**: M2

**Range**: 60 ft.

**Duration**: Instantaneous

This spell unlocks and unbars all doors, gates, and portals within its range, including those held or locked by normal
magic.

### Levitate

**Level**: M2

**Range**: 20 ft. per level

**Duration**: 10 minutes per level

This spell allows the Magic-user to levitate himself, moving vertically up or down, but the spell itself provides no
assistance with lateral movement.  A wall, cliff side, or ceiling could be used to move along hand-over-hand.

Levitation allows up or downward movement at a rate of up to 6 feet per minute (60 feet per turn), and the caster cannot
levitate more than 20 feet per level from the ground level where the spell was cast (such range being applied both to
movement into the air and to downward movement into a pit or chasm).

### Light (Dark)

**Level**: C1, M1

**Range**: 120 ft.

**Duration**: 2 hours (C), 1 hour + 10 minutes per level (M)

- As a Light spell, the caster targets a person or object which then produces a light about as bright as a torch with a
  radius of 20 feet.
- As a Dark spell, the caster instead causes night-time darkness to fall upon the area with a radius of 20 feet.

### Light (Dark), Continual

**Level**: C3, M2

**Range**: 120 ft.

**Duration**: Permanent until dispelled

- As a Light, Continual spell, the caster targets a person or object which then produces a light about as bright as a
  torch with a radius of 20 feet.
- As a Dark, Continual spell, the caster instead causes night-time darkness to fall upon the area with a radius of 20
  feet.

### Lightning Bolt

**Level**: M3

**Range**: 60 ft.

**Duration**: Instantaneous

A nearly ten foot wide bolt of lightning extends 60 feet from the fingertip of the caster. Anyone in its path suffers
1d6 points of damage per level of the caster, though a successful saving throw halves damage. The bolt always extends 60
feet, even if this means that it ricochets backward from something that blocks its path.

### Locate Object

**Level**: C3, M2

**Range**: 90 ft. (C), 60 ft. + 10 ft. per level (M)

**Duration**: 1 minutes per level

This spell gives the caster the correct direction (as the crow flies) toward an object the caster specifies with a
description. The object cannot be something the caster has never seen, although the spell can detect an object in a
general class of items known to the caster: stairs, gold, etc.

### Magic Jar

**Level**: M5

**Range**: See below

**Duration**: See below

This spell relocates the caster’s life essence, intelligence, and soul into an object (of virtually any kind). The “jar”
must be within 30 feet of the caster’s body for the transition to succeed.

Once within the magic jar, the caster can possess the bodies of other crea- tures and people, provided that they are
within 120 feet of the jar and fail a saving throw. The caster can return his soul to the magic jar at any time, and if
a body he controls is slain, his life essence returns immediately to the jar. If the caster’s body is destroyed while
his soul is in the magic jar, the soul no longer has a home other than within the magic jar (although the disembodied
wizard can still possess other bodies as before). If the jar itself is destroyed while the Magic-user’s soul is within,
the soul is lost. The Magic-user can return from the jar to his own body whenever desired, thus ending the spell.

### Massmorph

**Level**: M4

**Range**: 240 ft.

**Duration**: Until negated or dispelled

One hundred or fewer human-sized creatures are changed to appear like innocent trees. The illusion is so perfect that
creatures moving through the “forest” will not detect the deception.

### Move Earth

**Level**: M6

**Range**: 240 ft.

**Duration**: Permanent

This spell can only be used above ground. It allows the caster to move hills and other raised land or stone at a rate of
6 feet per minute for 1 hour.

### Move Water

**Level**: M6

**Range**: 240 ft.

**Duration**: See below

The caster must announce which of the two options are being cast:
- The spell lowers the depth and water level of lakes, rivers, wells, and other bodies of water to 1/2 their normal
  levels. Used this way, the spell lasts for 10 turns.

- Alternately, this spell creates a gap through water, but only to a depth of 10 feet. Used this way, the spell lasts
  for 6 turns.

### Neutralize Poison

**Level**: C4

**Range**: Referee’s discretion

**Duration**: 10 min.

This spell counteracts poison, but does not bring the dead back to life.

### Passwall

**Level**: M5

**Range**: 30 ft.

**Duration**: 30 min.

This spell creates a hole through solid rock. The hole or tunnel is up to 10 feet deep and is large enough to allow the
passage of an average sized human.

### Phantasmal Force

**Level**: M2

**Range**: 240 ft.

**Duration**: Until negated or dispelled

This spell creates a realistic illusion in the sight of all who view it. The illusion disappears when it is touched, but
if the viewer believes the illusion is real he can take damage from it.

### Plant Growth

**Level**: M4

**Range**: 120 ft.

**Duration**: Permanent until dispelled

Up to 300 square feet of ground can be affected by this spell; undergrowth in the area suddenly grows into an impassable
forest of thorns and vines.  The caster can decide the shape of the area to be enchanted. An alternate version
(Referee’s decision) would allow the spell to affect an area of 300 × 300 feet, for a total of 90,000 square feet.

### Polymorph

**Level**: M4

**Range**: See below

**Duration**: See below

The caster must announce which of the two options are being cast:

- The caster assumes the form of any object or creature, gaining the new form’s attributes (the use of wings, for
  example), but not its hit points or combat abilities. The Referee might allow the benefit of the new form’s armor
  class, if it is due to heavily armored skin. A great deal of the spell’s effect is left to the Referee to decide. This
  form of the spell lasts for roughly one hour plus one additional hour for each level of the caster.

- Alternately, this spell allows the caster to turn another being into a different type of creature (such as a dragon, a
  garden slug, and of course, a frog or newt). The polymorphed creature gains all the abilities of the new form, but
  retains its own mind and hit points. Used in this way, the range on this spell is 60 feet. This form of the spell
  lasts until dis- pelled.

### Project Image

**Level**: M6

**Range**: 240 ft.

**Duration**: 1 hour

The caster projects an image of his person to a maximum range of 240 feet. Not only does the projected image mimic the
caster’s sounds and gestures, but any spells he casts will appear to originate from the image.

### Protection from Chaos (Law)

**Level**: C1, M1

**Range**: Caster only

**Duration**: 2 hours (C), 1 hour (M)

- As a Protection from Chaos spell, the caster creates a magical field of protection around himself to block out all
  chaotic monsters, who suffer a -1 penalty “to-hit” against the caster, and the caster gains +1 on all saving throws
  against such attacks.
- As a Protection from Law spell, it does the same thing except that lawful creatures suffer the -1 penalty.

### Protection from Chaos (Law), 10 ft. radius

**Level**: C4, M3

**Range**: 10 ft. radius around caster

**Duration**: 2 hours

- As a Protection from Chaos, 10 ft. radius spell, it has the same effect as Protection from Chaos—except that its
  effect covers an area rather than an individual.
- As a Protection from Law, 10 ft.  radius spell, it has the same effect as Protection from Law—except that its effect
  covers an area rather than an individual.

### Protection from Normal Missiles

**Level**: M3

**Range**: 30 ft.

**Duration**: 2 hours

The recipient becomes invulnerable to small non-magical missiles. Only those missiles projected by normal humans and/or
weapons are affected.

### Purify (Putrefy) Food and Drink

**Level**: C1

**Range**: Close/Touch

**Duration**: Instantaneous

- As a Purify Food and Drink spell.  The caster causes enough food and water for up to a dozen people to be made pure,
  removing spoilage and poisons.
- As a Putrefy Food and Drink spell. The caster causes enough food and water for up to a dozen people to be made putrid,
  creating spoilage and poisons.

### Quest

**Level**: C5, M6

**Range**: 30 ft.

**Duration**: Until completed

If the victim fails his saving throw, the caster may set a task for him. If a Magic-user casts this spell the victim
will die if he ignores the Quest altogether. If a Cleric casts this spell the victim’s failure to obey will result in a
curse to be determined by the Referee.

### Raise Dead

**Level**: C5

**Range**: Line of sight

**Duration**: See below

Raise Dead allows the Cleric to raise a corpse from the dead, provided it has not been dead too long. The normal time
limit is 4 days, but for every caster level higher than 8th, the time limit extends another 4 days. Characters with low
constitution might not survive the ordeal; and even for those with a strong constitution, a period of two weeks is
required before they can function normally. This spell only functions on races that can be used for Player Characters
(i.e., “human-like”).

### Read Languages

**Level**: M1

**Range**: Reading distance

**Duration**: 1 or 2 readings

This spell allows the caster to read directions, instructions, and similar notations that are written in unfamiliar or
even unknown languages. It is especially useful for treasure maps.

### Read Magic

**Level**: M1

**Range**: Caster only

**Duration**: 2 scrolls or other writings

This spell allows the caster to read magical writings on items and scrolls.  Magical writing cannot be read with- out
the use of this spell.

### Reincarnation

**Level**: M6

**Range**: Touch

**Duration**: Instantaneous

This spell brings a dead character’s soul back from the dead, but the soul reappears in a newly formed body of the same
alignment as the deceased.

### Remove Curse

**Level**: C3, M4

**Range**: Close/Touch

**Duration**: Instantaneous

This spell removes one curse from a person or object.

### Sleep

**Level**: M1

**Range**: 240 ft.

**Duration**: Referee’s discretion

This spell puts creatures into an enchanted slumber (no saving throw is permitted). It can affect a number of creatures
based on their hit dice.

#### Table 32 : Affected by Sleep

| Victim’s HD        | Number Affected |
| :---------:        | :-------------: |
| Less than 1 to 1+1 | 3d6             |
| 2 to 2+1           | 2d6             |
| 3 to 3+1           | 1d6             |
| 4 to 4+1           | 1               |

### Speak with Animals

**Level**: C2

**Range**: 30 ft.

**Duration**: 1 hour

The caster can speak with animals within range. There is a chance that the animals will assist him, and they will not
attack him or his party (unless he’s got something particularly offensive to say).

Speak with Plants
Level: C4
Range: 30 ft.
Duration: 1 hour
The caster can speak to and under-
stand the replies of plants. Plants will
obey his commands as far as they are
able (e.g. twisting or bending aside to
ease his passage, etc.).

### Sticks to Snakes

**Level**: C4

**Range**: 120 ft.

**Duration**: 1 hour

The caster may turn as many as 2d8 normal sticks into snakes, each one having a 50% chance of being venomous. The snakes
follow his commands, but turn back into sticks at the end of the spell (or when killed).

### Telekinesis

**Level**: M5

**Range**: 120 ft.

**Duration**: 1 hour

The caster can move objects using mental power alone. The amount of weight he can lift and move is 20 pounds per level.

### Teleport

**Level**: M5

**Range**: Touch

**Duration**: Instantaneous

This spell transports the caster or another person to a destination that the caster knows (at least knowing what it
looks like from a painting or a map).  Success depends on how well the caster knows the targeted location:

- If the caster has only indirect experience of the destination (known only through a picture or map) there is a 25%
  chance of success— with failure resulting in death.
- If the caster has seen but not studied the destination there is a 20% chance of failure. Half of failures will place
  the traveler 1d10 × 10 feet below the intended location, possibly resulting in death from arrival within a solid
  substance. The other half of failures will place the traveler 1d10 × 10 feet above the targeted location, possibly
  resulting in a deadly fall.
- If the caster is familiar with the location or has studied it carefully there is a 5% chance of failure, with 1 in 6
  failures arriving below and 5 in 6 arriving above the targeted location. In either case, the arrival is 1d4 × 10 feet
  low or high.

### Transform Rock-Mud

**Level**: M5

**Range**: 120 ft.

**Duration**: 3d6 days, or spell reversal

This spell transforms rock (and any other form of earth) into mud, or mud to rock. An area of roughly 300 × 300 feet
becomes a deep mire, reducing movement to 10% of normal.

### Transform Stone-Flesh

**Level**: M6

**Range**: 120 ft.

**Duration**: Permanent until reversed

This spell transforms flesh into stone or stone into flesh, as desired by the caster. A saving throw is permitted to
avoid being turned into stone, but if the spell succeeds the victim is transformed into a statue.

### Wall of Fire or Ice

**Level**: M4

**Range**: 60 ft.

**Duration**: Concentration

The caster must announce which of the two options are being cast:

- As a Wall of Fire spell, the caster conjures a wall of fire that flares into being and burns for as long as the caster
  concentrates upon it. Creatures with 3 or fewer hit dice cannot pass through it, and no creature can see through it to
  the other side.  Passing through the fire inflicts 1d6 hit points of damage (no saving throw) and undead creatures
  sustain twice the normal damage.The caster may choose to create a straight wall 60 feet long and 20 feet high, or a
  circular wall with a 15 foot radius, also 20 feet high.
- As a Wall of Ice spell, the caster conjures up a 6 foot thick wall of ice. The caster may choose to create a straight
  wall 60 feet long and 20 feet high,or a circular wall with a 15 foot radius, also 20 feet high. Creatures with 3 or
  fewer hit dice cannot affect the wall, but creatures of 4+ hit dice are able to smash through it, taking 1d6 points of
  damage in the process. Creatures with fire- based metabolisms take 2d6 instead of the normal 1d6. Fire spells and
  magical effects are negated in the vicinity of the wall.

### Wall of Stone or Iron

**Level**: M5

**Range**: 60 ft.

**Duration**: Permanent (stone) or 2 hours (iron)

The caster must announce which of the two options are being cast:

- As a Wall of Stone spell, the caster conjures a wall of stone 2 feet thick, with a surface area of 1,000 square feet.
- As a Wall of Iron spell, the caster conjures an iron wall from thin air.  The wall is 3 feet thick, with a surface
  area of 500 square feet.

### Water Breathing

**Level**: M3

**Range**: 30 ft.

**Duration**: 2 hours

This spell grants the ability to breathe underwater until the spell’s duration expires.

### Web

**Level**: M2

**Range**: 30 ft.

**Duration**: 8 hours

Fibrous, sticky webs fill an area up to 10 × 10 × 20 feet. It is extremely difficult to get through the mass of
strands—it takes one turn if a torch and sword (or a flaming sword) are used, and creatures larger than a horse can
break through in 2 turns. Humans take longer to break through - perhaps 3 to 4 turns or longer at the Referee’s
discretion.

### Wizard Eye

**Level**: M4

**Range**: 240 ft.

**Duration**: 1 hour

The caster conjures up an invisible, magical “eye,” that can move a maximum of 240 feet from its creator. It floats
along as directed by the caster, at a rate of 120 feet per turn.

### Wizard Lock

**Level**: M2

**Range**: Close

**Duration**: Permanent until dispelled

As with a Hold Portal spell, but it is permanent until dispelled. Creatures with magic resistance can shatter the spell
without effort. Any Magic-user three levels or higher than the caster can open the portal, and a Knock spell will open
it as well (although the spell is not permanently destroyed in these cases).

# Chapter 7: Running the Game

Running a game of White Box is a lot easier than running most other role-playing games, simply because there are not as
many rules and your own discretion overrides them anyway. Most situations are handled by making common sense decisions
concerning what happens next.

For example: the players are in combat with a group of orcs and the fighter wants to trip one of them. It’s up to the
Referee to decide what the fighter needs to do or roll to be successful. If a Player decides that his character is going
to jump through a wall of fire, with several bottles of flammable oil in his backpack, it’s up to the Referee to
determine whether or not they explode.

This means making up a lot of stuff on the spot. If you’re not a good storyteller or if you’re not up to doing a lot of
creative thinking on the fly, it might be better that you try a different game—one that provides more rules and guidance
for every situation that might arise. But if you’re a good storyteller, creative and fair, White Box’s small, Spartan
rule-set frees up your creativity to create a fantasy role-playing experience completely different from the type of
game that depends on a multitude of rules.

White Box also frees up your creativity in terms of customizing the game.  Unlike a more complex game, you can add house
rules wherever you want to without accidentally messing up something else buried in the rules.If you want to use
critical hits and fumbles, add ‘em in. You won’t break anything—there’s not that much to break!

## Designing an Adventure

The “adventure” is just the setting for the game—usually a map and then notes about certain locations on that map. As
the Players tell you where their charac- ters go and what they do, you’re referring to the map and your notes to
describe what happens as a result. Don’t try to plan for all contingencies—it’s guaranteed that the players will do
something unexpected during the adventure and you’ll just have to roll with it, thinking on your feet and making up new
things as you go. Just as you challenge the Players with adventure, they challenge you to keep up with their collective
creativity.

## Creating a Campaign

A campaign is the world beyond the adventure—the cities, forests, coastlines, and kingdoms of the fantasy world.The
players will almost certainly want their characters to explore the wilderness, visit cities, and do all sorts of things
in the fantasy world. At the beginning of the game, you might want to sketch out a map of a single village (as the
starting point) and some of the surrounding area. (The location of the first adventure—a dark forest, perhaps) As the
players move their characters around from adventure to adventure, you can expand the little map into an entire fantasy
world with the continents, kingdoms, and great empires at your disposal.

If you want to take a shortcut, you can set your entire campaign in a fictional world created by the author of one of
your favorite fantasy stories. Most of these have maps and the author has already created the details and feel of the
world for you. For example: the worlds of Conan’s Hyboria (Robert E.  Howard), of Elric and the Eternal Champions
(Michael Moorcock), and of the Dying Earth ( Jack Vance) are popular fictional settings ready for gaming.  Indeed,
publishers have already created pre-packaged campaigns for all three of these examples.

### Experience Points

Experience Points are awarded to Players for gaining treasure and killing monsters, as described under “Gaining
Experience”in Chapter 5.It may seem counter-intuitive that treasure somehow makes characters more experienced, but
that’s not what awarding experience for gold pieces is all about. Gold pieces are an after-the-fact measurement of how
ingenious the character (Player) was in getting them. The gold pieces are not the source of the experience—they are the
measurable product of it.

That being said, there are many alternative ways for Referees to award XP. For example, active participation in the
campaign might warrant experience for each hour of real-time play, as could solving puzzles and aiding allies. A
particularly clever solution or epic victory might warrant double the normal XP amount awarded, while an abysmal failure
might merit half.

Some Referees make all earned Experience Points a part of a community total to be divided evenly, since not all the
characters have the same opportunities in a given adventure. Others prefer a more competitive game, whereby each player
earns XP according to individual accomplishments.

If you find that whatever system you’re using leads the players toward bad decisions—seeking out unnecessary combat or
looking for traps to spring—you might consider adjusting your system. This is true for the “official” system of awarding
experience as well. Remember, the Referee is the ultimate judge of what works best for a game and any rule can be
changed to fit the group.

### Scale of Advancement

Referees all have their own style of campaign, and it would be silly for one author to impose needless restrictions on
anyone playing the game. However, it is worth noting that this rule set was designed with the notion that 4th level
characters are “heroic” and thus the tables were capped at level 10. A Referee who wishes to extend the tables to higher
levels is encouraged to do so if it fits the style of the campaign.

## Underworld and Wilderness Adventures

Many Referees will create a map of the Underworld or Wilderness in advance of play. The player characters will then
explore the map but are unaware of its contents.

In the Underworld a map is filled with monsters, traps, treasure, and any mysterious creatures or locations the Referee
can dream into existence.  The Wilderness map is created using hexagon paper, with each hex representing 6 miles, and
having a primary terrain and possibly an interesting feature.  A feature could be a Wizards tower, a Fighter’s
stronghold, and an Evil High Priest’s dark temple or maybe it could be a small elf like creature, sitting on a tree
stump, playing a magical flute. Let your imagination run wild when creating features.

### Wandering Monsters

In the Underworld, check every hour (six turns), for wandering monsters and in the Wilderness check every hex. Normally
there is a 1 in 6 chance of encountering something in the Underworld. Check the Getting Lost and Encounters table for
chances of encounters in the Wilderness.

The specific encounter is rolled on a table. Initial encounter distance is dependent on the circumstance.

### Reaction Check

The reaction of monsters to the player character should be determined by the Referee based on the monster’s intelligence
or wisdom and the situation in which the encounter occurs. Unintelligent monsters will simply attack.  If there is any
uncertainty about the monsters reaction, a roll can be made on the following table.

#### Table 33: Reaction Check

| Roll 2d6  | Reaction     |
| :------:  | :------:     |
| 2 or less | Hostile      |
| 3-5       | Negative     |
| 6-8       | Uncertain    |
| 9-11      | Positive     |
| 12        | Enthusiastic |

### Getting Lost and Encounters

In the Wilderness, if the characters are following a road or trail, there isn’t a chance of getting lost. If however,
they are exploring unknown lands, there is a chance of losing their way depending on the hex terrain.  Whether on
well-marked roads or out in the unknown, there is always a chance for an encounter.

Roll d6 twice per hex to check for becoming lost and/or an encounter

#### Table 34: Getting Lost and Encounters

| Terrain   | Open | Forest | River, Coast | Jungle, Swamp | Hills, Mountains | Desert | Settled |
| :-----:   | :--: | :----: | :----------: | :-----------: | :--------------: | :----: | :-----: |
| Lost      | 1    | 1-2    | 1            | 1-3           | 1-2              | 1-3    | --      |
| Encounter | 1    | 1-2    | 1-2          | 1-3           | 1-3              | 1-21   |

### Wilderness Movement Rates by Transport

#### Table 35: Land Movement

| Transport      | Move Rate   | Hexes Per Day |
| :-------:      | :---------: | :-----------: |
| Dwarf/Halfling | 9           | 3/2           |
| Human/Elf      | 12          | 2             |
| Horse, draft   | 12          | 2             |
| Horse, riding  | 24          | 4             |
| Horse, war     | 18          | 3             |
| Mule           | 12          | 2             |
| Wagon          | 6           | 1             |

#### Table 36: Water Movement

| Transport             | Move Rate   | Hexes Per Day |
| :-------:             | :---------: | :-----------: |
| Boat                  | 6           | 1             |
| Galley, small         | 12          | 2             |
| Galley, large         | 18          | 3             |
| Ship, sailing (small) | 18          | 3             |
| Ship, sailing (large) | 12          | 2             |
| Warship               | 6           | 1             |

#### Table 37: Air Movement

| Transport     | Move Rate\* | Hexes Per Day |
| :-------:     | :---------: | :-----------: |
| Dragon, young | 18          | 3             |
| Dragon, adult | 24          | 4             |
| Dragon, luck  | 96          | 12            |
| Flying Carpet | 24          | 4             |
| Griffon       | 30          | 5             |
| Hippogriff    | 36          | 6             |
| Pegasus       | 42          | 7             |
| Roc           | 48          | 8             |

\* Move rate is at low altitude. High altitude doubles the rate.

## Underworld Encounter Tables by Dungeon Level

#### Table 38: Underworld Encounters

| Dungeon Level | EL 1 | EL 2 | EL 3 | EL 4 | EL 5 | EL 6 | EL 7 | EL 8 | EL 9 | EL 10 |
| :-----------: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :---: |
| 1             | 1-3  | 4-5  | 6    | -    | -    | -    | -    | -    | -    | -     |
| 2             | 1    | 2-4  | 5-6  | -    | -    | -    | -    | -    | -    | -     |
| 3             | -    | 1    | 2-4  | 5-6  | -    | -    | -    | -    | -    | -     |
| 4             | -    | -    | 1    | 2-4  | 5-6  | -    | -    | -    | -    | -     |
| 5             | -    | -    | -    | 1    | 2-4  | 5-6  | -    | -    | -    | -     |
| 6             | -    | -    | -    | -    | 1    | 2-4  | 5    | 6    | -    | -     |
| 7             | -    | -    | -    | -    | -    | 1    | 2-4  | 5    | 6    | -     |
| 8             | -    | -    | -    | -    | -    | -    | 1-2  | 3-5  | 6    | -     |
| 9             | -    | -    | -    | -    | -    | -    | -    | 1-2  | 3-5  | 6     |
| 10+           | -    | -    | -    | -    | -    | -    | -    | 1    | 2-3  | 4-6   |


For character class encounters, roll a d6 for their alignment: 1-3 = Chaotic, 4-5 = Neutral, 6 = Lawful. The character
class can be any of the character races or for an additional option; The Referee can choose a humanoid. For example,
instead of a level 5 human Fighter, it could be a level 5, Orc Fighter.

### Encounter Levels

| Roll | Level 1            | Level 2            | Level 3               |
| :--: | :-----:            | :-----:            | :-----:               |
| 1    | Kobold             | Beetle, Giant Fire | Demon, Lemure         |
| 2    | Skeleton           | Centipede, Giant   | Grey Ooze             |
| 3    | Rat, Giant         | Ghoul              | Harpy                 |
| 4    | Goblin             | Gnoll              | Lycanthrope, Wererat  |
| 5    | Gnome              | Lizardman          | Lycanthrope, Werewolf |
| 6    | Orc                | Spider, Giant      | Wight                 |
| 7    | Zombie             | Hobgoblin          | Bugbear               |
| 8    | Dwarf              | Human, Berserker   | Shadow                |
| 9    | Elf                | Human, Bandit      | Fighter, level 3      |
| 10   | Hobgoblin          | Fighter, level 2   | Magic-user, level 2   |
| 11   | Stirge             | Cleric, level 2    | Demon, Imp            |
| 12   | Beetle, Giant Fire | Elf, Dark          | Elf, Dark             |

| Roll | Level 4             | Level 5            | Level 6             |
| :--: | :-----:             | :-----:            | :-----:             |
| 1    | Hell Hound          | Hell Hound         | Hell Hound          |
| 2    | Centipede, Giant    | Hydra              | Dragon, White       |
| 3    | Doppelganger        | Dragon, White      | Dragon, Black       |
| 4    | Gargoyle            | Cockatrice         | Hydra               |
| 5    | Gelatinous Cube     | Ochre Jelly        | Basilisk            |
| 6    | Worg                | Mummy              | Blink Dog           |
| 7    | Wraith              | Ogre               | Medusa              |
| 8    | Ogre                | Ogre Mage          | Spectre             |
| 9    | Fighter, level 4    | Fighter, level 5   | Troll               |
| 10   | Cleric, level 4     | Cleric, level 5    | Magic-user, level 5 |
| 11   | Magic-user, level 3 | Magic-user level 4 | Thief, level 6      |
| 12   | Thief, level 4      | Thief, level 5     | Sidhe               |

| Roll | Level 7         | Level 8               | Level 9             | Level 10         |
| :--: | :-----:         | :-----:               | :-----:             | :------:         |
| 1    | Minotaur        | Lycanthrope, Werebear | Hydra               | Hydra            |
| 2    | Hell Hound      | Hydra                 | Dragon, Blue        | Lich             |
| 3    | Hydra           | Dragon, Blue          | Dragon, Black       | Dragon, Red      |
| 4    | Dragon, White   | Dragon, Black         | Dragon, Green       | Dragon, Gold     |
| 5    | Dragon, Black   | Dragon, Green         | Dragon, Red         | Black Pudding    |
| 6    | Dragon, Green   | Vampire               | Chimera             | Elemental, Air   |
| 7    | Banshee         | Elemental, Air        | Demon, Baalroch     | Elemental, Earth |
| 8    | Vampire         | Elemental, Earth      | Vampire             | Elemental, Fire  |
| 9    | Demon, Succubus | Elemental, Fire       | Fighter, level 9    | Elemental, Water |
| 10   | Salamander      | Elemental, Water      | Cleric, level 9     | Death Knight     |
| 11   | Wyvern          | Gorgon                | Magic-user, level 8 | Slug, Giant      |
| 12   | Djinni          | Invisible Stalker     | Thief, level 9      | Purple Worm      |

## Wilderness Encounter Tables by Terrain Type

#### Table 39: Wilderness Encounter Type

| Roll | Open     | Forest   | River, Coast | Swamp, Jungle | Hills, Mountains | Barren  | Settled |
| :--: | :--:     | :----:   | :----------: | :-----------: | :--------------: | :----:  | :------ |
| 1    | Dragon   | Dragon   | Dragon       | Dragon        | Dragon           | Dragon  | Undead  |
| 2    | Monster  | Monster  | Monster      | Monster       | Monster          | Monster | Humnd.  |
| 3    | Animal   | Animal   | Animal       | Undead        | Animal           | Undead  | Human   |
| 4    | Humnd.   | Animal   | Animal       | Animal        | Humnd.           | Animal  | Human   |
| 5    | Human    | Humnd.   | Humnd.       | Humnd.        | Humnd.           | Humnd.  | Human   |
| 6    | Human    | Human    | Human        | Human         | Human            | Human   | Humnd.  |
| 7    | Humnd.   | Humnd.   | Animal       | Animal        | Humnd.           | Humnd.  | --      |
| 8    | Animal   | Animal   | Animal       | Undead        | Humnd.           | Animal  | --      |
| 9    | Monster  | Monster  | Monster      | Monster       | Animal           | Undead  | --      |
| 10   | Lycanth. | Lycanth. | Lycanth.     | Lycanth.      | Giant            | Monster | --      |

#### Human

| Roll | Open, Forest | River, Coast | Swamp, Jungle | Hills, Mtns, Barren | Settled  |
| :--: | :----------: | :----------: | :-----------: | :-----------------: | :-----:  |
| 1    | NPCs         | NPCs         | NPCs          | NPCs                | NPCs     |
| 2    | Clerics      | Berserkers   | Mages         | Berserkers          | Bandits  |
| 3    | Fighters     | Fighters     | Berserkers    | Fighters            | Clerics  |
| 4    | Bandits      | Pirates      | Fighters      | Bandits             | Common   |
| 5    | Common       | Pirates      | Bandits       | Bandits             | Common   |
| 6    | Bandits      | Bandits      | Bandits       | Bandits             | Common   |
| 7    | Soldiers     | Fighters     | Fighters      | Fighters            | Soldiers |
| 8    | Fighters     | Berserkers   | Berserkers    | Berserkers          | Soldiers |
| 9    | Berserkers   | Mages        | Mages         | Clerics             | Fighters |
| 10   | Mages        | Clerics      | Clerics       | Mages               | Mages    |

#### Humanoid

| Roll | Open, Forest | River, Coast | Swamp, Jungle | Hills, Mtns, Barren | Settled |
| :--: | :----------: | :----------: | :-----------: | :-----------------: | :-----: |
| 1    | Ogres        | Ogres        | Ogres         | Ogre Mage           | Goblins |
| 2    | Gnolls       | Gnolls       | Bugbears      | Gnolls              | Dwarfs  |
| 3    | Hobgoblins   | Orcs         | Orcs          | Hobgoblins          | Elves   |
| 4    | Elves        | Lizardmen    | Lizardmen     | Dwarfs              | Kobolds |
| 5    | Gnomes       | Hobgoblins   | Lizardmen     | Kobolds             | Gnomes  |
| 6    | Orcs         | Trolls       | Kobolds       | Orcs                | --      |
| 7    | Sidhe        | --           | Hobgoblins    | Goblins             | --      |
| 8    | Kobolds      | --           | Trolls        | Trolls              | --      |
| 9    | Bugbears     | --           | --            | Ogres               | --      |
| 10   | Trolls       | --           | --            | Minotaurs           | --      |

#### Animal

| Roll | Open, Forest  | River, Coast  | Swamp, Jungle | Hills, Mtns, Barren |
| :--: | :----------:  | :----------:  | :-----------: | :-----------------: |
| 1    | Worgs         | Rats          | Rats          | Snake, Giant        |
| 2    | Snake, Giant  | Centipedes    | Snake, Giant  | Worgs               |
| 3    | Spiders       | Snake, Giant  | Spiders       | Rats                |
| 4    | Rats          | Beetle, Giant | Centipedes    | Spiders             |
| 5    | Beetle, Giant | Spiders       | Beetle, Giant | Centipedes          |
| 6    | Centipedes    | Stirge        | Stirge        | Stirge              |

#### Monster

| Roll | Open, Forest | Barren      | Hills, Mountains | Swamp, Jungle | River, Coast |
| :--: | :----------: | :----:      | :--------------: | :-----------: | :----------: |
| 1    | Unicorns     | Salamander  | Basilisk         | Medusa        | Sea Serpent  |
| 2    | Dryad        | Purple Worm | Chimerae         | Dryad         | Basilisk     |
| 3    | Centaur      | Basilisk    | Gorgons          | Basilisk      | Chimerae     |
| 4    | Basilisk     | Gorgons     | Griffons         | Cockatrice    | Harpies      |
| 5    | Wererat      | Manticore   | Harpies          | Chimerae      | Medusa       |
| 6    | Cockatrice   | Medusa      | Werewolf         | Harpies       | Pegasi       |
| 7    | Blink Dog    | Roc         | Hippogriffs      | --            | --           |
| 8    | Werewolf     | Roll again  | Manticore        | --            | --           |
| 9    | Chimerae     | --          | Medusa           | --            | --           |
| 10   | Manticore    | --          | Pegasi           | --            | --           |
| 11   | --           | --          | Roc              | --            | --           |
| 12   | --           | --          | Wererat          | --            | --           |

#### Other

| Roll | Dragon        | Undead       | Giant          |
| :--: | :----:        | :----:       | :---:          |
| 1    | Dragon, Gold  | Banshee      | Efreet         |
| 2    | Dragon, Blue  | Wraiths      | Djinn          |
| 3    | Dragon, Black | Shadow       | Giant, Frost   |
| 4    | Dragon, Green | Ghouls       | Giant, Stone   |
| 5    | Dragon, Red   | Skeletons    | Giant, Hill    |
| 6    | Dragon, White | Zombies      | Giant, Storm   |
| 7    | Wyverns       | Wights       | Giant, Fire    |
| 8    | Hydrae        | Mummies      | Giant, Cloud   |
| 9    | --            | Spectre      | Giant, Firbolg |
| 10   | --            | Vampires     | Treants        |
| 11   | --            | Death Knight | --             |
| 12   | --            | Liches       | --             |

# Chapter 8: Monsters

There’s not a lot of detail given about the monsters, because the more detail given, the more your own mental image of
the fantasy world is going to be locked into a single track. We’re not going to say that giant ants are red, nocturnal,
three feet long, and fond of eating Elves. Because, in your mind, they might be blue, diurnal, five feet long, and eat
only plants unless attacked. Details about monsters toss roadblocks in front of the imagination. Yes, details can also
inspire imagination, but we’re making the assumption that if you’re interested in fantasy gaming in the first place,
you’ve got a good imagination as-is and don’t require details about the size of a giant ant.

One important characteristic of all monsters, though, is that they can see in the dark.

The following is a quick reference guide for how to read monster descriptions:

#### Armor Class

“Armor Class” is explained earlier in the rules for combat. If you’re using the Descending AC system (where lower AC is
better), disregard the numbers in brackets. If you’re using the Ascending AC system (where a high AC is better), use the
numbers in brackets.

#### Attacks

“Attacks” lists the number of attacks a monster has and the damage they inflict.  Most monsters have one attack and
inflict 1d6 damage—there are, however, some exceptions.

#### Experience Points

“Experience Points” are presented after a monster’s Hit Dice Equivalent (HDE) rating, and list the number of XP the
adventuring party gains as a result of killing the creature. In some cases, however, the Referee may choose to award
experience points for defeating a creature without killing it (circumventing it by creative means, capturing it to bring
home, etc.). Some monsters have multiple Experience Points listed—this is because certain monsters have a range of hit
dice. Order is always from lowest HD to highest HD.

#### Hit Dice

“Hit Dice” is the number of dice (d6) rolled to determine an individual creature’s Hit Points (HP). If there is a plus
or minus after the number, add or subtract that number once from the total rolled.

#### Hit Dice Equivalent

“Hit Dice Equivalent” is used to separate monsters into “difficulty levels” so that when creating an adventure the
Referee has some guidelines concerning what characters can and cannot handle. Some monsters have multiple HDE’s
listed—this is because certain monsters have a range of hit dice. Order is always from lowest HD to highest HD.

#### Magic Resistance

“Magic Resistance”isn’t one of the entries, but some creatures may have “magic resistance.” The given percentage chance
is the likelihood that any magic used against the creature (other than bonuses from weapons) will fail to take effect.
To determine whether or not magic used will have an impact, roll a d100. The magic fails if the result is less than the
given percentage.

#### Move

“Move” is the monster’s movement rate and is handled like it is for PCs.

#### Saving Throw

“Saving Throw” isn’t one of the entries, but all creatures have a saving throw.  It is the number on the d20 the monster
needs to equal or exceed in order to avoid a threat or lessen its effect. It is found by subtracting the monster’s HD
from 19. For example, a monster with 4 HD has a saving throw of 15. Alternatively, a monster can simply be treated as a
Fighter or whatever character class makes sense for that monster, with the monster’s hit dice being used for its level.

#### Special

“Special” is just a “flag” of sorts for the Referee that serves as a reminder that a monster has a special ability.

#### Table 40: Monster Attack Rolls (DAC/AAC)

| HD   | 9 [10] | 8 [11] | 7 [12] | 6 [13] | 5 [14] | 4 [15] | 3 [16] | 2 [17] | 1 [18] | 0 [19] |
| :--: | :----: | :----: | :----: | :----: | :----: | :----: | :----: | :----: | :----: | :----: |
| <1   | 10     | 11     | 12     | 13     | 14     | 15     | 16     | 17     | 18     | 19     |
| 1    | 9      | 10     | 11     | 12     | 13     | 14     | 15     | 16     | 17     | 18     |
| 2    | 8      | 9      | 10     | 11     | 12     | 13     | 14     | 15     | 16     | 17     |
| 3    | 7      | 8      | 9      | 10     | 11     | 12     | 13     | 14     | 15     | 16     |
| 4    | 6      | 7      | 8      | 9      | 10     | 11     | 12     | 13     | 14     | 15     |
| 5    | 5      | 6      | 7      | 8      | 9      | 10     | 11     | 12     | 13     | 14     |
| 6    | 4      | 5      | 6      | 7      | 8      | 9      | 10     | 11     | 12     | 13     |
| 7    | 3      | 4      | 5      | 6      | 7      | 8      | 9      | 10     | 11     | 12     |
| 8    | 2      | 3      | 4      | 5      | 6      | 7      | 8      | 9      | 10     | 11     |
| 9    | 1      | 2      | 3      | 4      | 5      | 6      | 7      | 8      | 9      | 10     |
| 10   | 1      | 1      | 2      | 3      | 4      | 5      | 6      | 7      | 8      | 9      |
| 11   | 1      | 1      | 1      | 2      | 3      | 4      | 5      | 6      | 7      | 8      |
| 12   | 1      | 1      | 1      | 1      | 2      | 3      | 4      | 5      | 6      | 7      |
| 13   | 1      | 1      | 1      | 1      | 1      | 2      | 3      | 4      | 5      | 6      |
| 14   | 1      | 1      | 1      | 1      | 1      | 1      | 2      | 3      | 4      | 5      |
| 15+  | 1      | 1      | 1      | 1      | 1      | 1      | 1      | 2      | 3      | 4      |

#### Note

If you are using the quick formula for calculating attacks using the ascending armor class system, the monster’s normal
“to-hit” bonus is equal to its hit dice (capped at +15). For example, a monster with 3 HD attacks with a +3 “to-hit”
bonus.


