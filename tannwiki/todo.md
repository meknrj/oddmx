1. How acpid works?
2. rofi -show combi
5. Networking Basics (some book recommended by openbsd guys about tcp/ip)
6. Create runC container
7. Create container without docker, using namespaces and cgroups (cgroup-tools, stress)
8. docker (or switch to podman and forget about docker)
9. find bookmarks storage
10. rclone + webdav
11. add tmux (with settings)
12. properly setup https://github.com/SidOfc/mkdx
13. Find cloud storage provider (also sync on harddisk at home)
14. Add https://simnalamburt.github.io/vim-mundo/ into vimrc
15. Configuration for firefox (???) - https://www.reddit.com/r/openbsd/comments/bzq5b9/how_i_configured_firefox_after_the_base_install/
16. Wisdom tower - https://habr.com/ru/post/458672/
17. Compile into full guide https://www.reddit.com/r/SWN/comments/c7z5fc/keeping_track_of_combat_without_a_map_or/
18. Compile own list from this - http://taxidermicowlbear.weebly.com/dd-retroclones.html
19. Compile - https://www.reddit.com/r/rpg/comments/9xf3ky/a_hard_scifi_sandbox_for_mothership_sandbox/
20. Review list of books - https://begriffs.com/posts/2017-04-13-longterm-computing-reading.html
21. Start tmux project -  http://danielallendeutsch.com/blog/16-using-tmux-properly.html
22. Wisdom tower - https://thereader.mitpress.mit.edu/habits-of-expert-software-designers/
23. Add docker snippets
24. Turn off firefox spying (https://www.unixsheikh.com/articles/mozilla-is-becoming-evil-be-careful-with-firefox.html)
25. [Guide] Port https://www.dedoimedo.com/computers/docker-supervisord.html
26. Add useful things to guide about supervisord
27. [Guide] Port - https://opensource.com/article/18/7/sysadmin-guide-selinux - SELinux notes
28. [Guide] Port https://stackoverflow.com/questions/20762094/how-are-zlib-gzip-and-zip-related-what-do-they-have-in-common-and-how-are-they/20765054#20765054
29. [Guide] Port http://bruxy.regnet.cz/web/linux/EN/socks-via-ssh/
30. [Guide] Port http://bruxy.regnet.cz/web/linux/EN/ssh-bastion/
31. [Guide] Port http://bruxy.regnet.cz/web/linux/EN/ssh-agent/
32. [Guide] Pack Windows games into Wine (https://rutracker.org/forum/viewtopic.php?t=5152471)
33. [Guide] Searching files on IRC
34. [Guide] IPTables
35. https://phoenixnap.com/kb/iptables-tutorial-linux-firewall
36. https://ru.wikibooks.org/wiki/Iptables
37. https://www.booleanworld.com/depth-guide-iptables-linux-firewall/
38. https://www.frozentux.net/iptables-tutorial/iptables-tutorial.html
39. What the difference between ifconfig and ip on linux?
40. More info about LVM - http://wiki.linuxformat.ru/wiki/LXF112:LVM
41. https://www.reddit.com/r/linux_gaming/comments/cydgxu/guide_configuring_pcsx2_150_with_brief_explanation/
42. Migrate from to docker to podman
43. How to get logs from running container?
44. Build container with Buildah
45. Difference between Dockerfile and Buildah scripts
46. Check Redhat courses about file systems

