# List of various links
- https://www.manualslib.com/ -- huge list of manuals for electronics (notebooks and laptops).
- https://16colo.rs/ -- ASCII and ANSI art archive.
- https://www.marinetraffic.com/en/ais/home/centerx:81.1/centery:7.2/zoom:2 -- marine traffic map
- http://mywiki.wooledge.org/BashFAQ/ -- best BASH FAQ
- https://www.artstation.com/olegdanilenko -- awesome space pics
- https://www.fosskers.ca/en/blog/wayland -- Interesting project for long winter nights
- https://www.agwa.name/projects/git-crypt/ -- enables transparent encryption and decryption of files in a git repository
- https://imgur.com/gallery/L76LU -- Call of Cthulhu Japanes collection
- https://www.reddit.com/r/worldbuilding/comments/56glme/scifi_worldbuilders_how_do_your_interstellar/  -- thread about interstellar communication

## Cool software
1. DNStop
2. iftop - is a tool to report network interfaces bandwith just like vmstat/iostat
3. lsscsi - list SCSI devices (or hosts) and their attributes (What is FreeBSD analogue?)
4. mpck — консольная утилита для проверки корректности потока MPEG в MP3 файлах.
5. duc — набор инструментов для проверки и визуализации использования дисков.
6. VintageRadio is a GUI front-end for the fm command-line application included with fmtools <http://benpfaff.org/fmtools>.
7. duf - Disk Usage/Free Utility (Linux, BSD, macOS & Windows) - https://github.com/muesli/duf
8. mdp — консольная Ncurses утилита для просмотра презентаций в формате Markdown.
9. Gifsicle — набор консольных утилит для создания и манипулирования анимированными изображениями в формате GIF
10. hwprobe — консольная Perl утилита для определения и отображения детальной информации о имеющимся аппаратном обеспечении.
11. Skippy-XD — автономное приложение предоставляющее средство выбора открытых окон одновременно отображаемых в виде миниатюр.
12. netwatch предназначается для отображения соединений между локальной и удалёнными системами, а также скорость
    передачи данных. Поддерживается мониторинг статистики сотен соединений одновременно, доступен мониторинг качества
    соединений на порту (port) и адрес назначения.
13. asciiplanets — маленькая Python (pyephem) / GNU Bash / Ncurses (tput) консольная утилита для просмотра расположения планет в Солнечной системе.
14. bsod — консольная Ncurses (Curses) утилита отображающая в эмуляторе терминала знаменитый Синий экран смерти" из Windows XP.
15. journalwatch — простая Python (python-systemd) консольная утилита в автоматическом режиме проверяющая журнал systemd
    (подсистема инициализации) на наличие ошибок и уведомляющая о них системного администратора по электронной почте.
16. logcheck — консольная C (си) утилита уведомляющая системного администратора об аномалиях в файлах журналов
    (лог-файлах) по электронной почте.
17. maim (Make Image) — маленькая утилита для создания снимков экрана (скриншотов / screenshoot), разработана как улучшенная версия scrot.
18. Teiler — простой инструмент для создания снимков экрана (скриншотов / screenshoot) и записи скринкастов (динамических скриншотов).
19. ttyrec — консольная C (си) утилита и формат файлов для записи вывода в консоль приложений с текстовым интерфейсом
    вместе с наложением меток и воспроизводить их (консольный скринкастинг).
20. cli-visualizer — консольная C++ / Ncurses утилита для визуализации аудио сигнала в режиме реального времени,
    используя преобразование Фурье с помощью библиотеки FFTW.
21. xawtv was originally just an analog TV viewing application for Bttv devices (bt848, bt878), written by Gerd Knorr,
    but evolved into a small suite of X11 applications for V4L devices (the 3.9x branch). The version 4.x development
    branch, amongst other changes, added support for DVB functionality.
22. noteshrink — консольная Python утилита для преобразования сканированных рукописных конспектов/заметок в красивые и
    компактные PDF-файлы.
23. Verso — гибко настраиваемый Perl / GTK графический редактор EXIF, IPTC и XMP метаданных цифровых изображений
    (фотографий) на основе функционала ExifTool.
24. muzikQ — консольный C (си) / Curses (Ncurses) / SDL музыкальный проигрыватель (mp3 & ogg).
25. cutmp3 — небольшой и быстрый консольный редактор файлов формата MP3 (MPEG-1 Audio Layer 3).
26. mpgedit — консольная утилита для обработки аудио файлов формата MP3 (MPEG 1/2/2.5).
27. FFQueue — удобный C++ / GTK / wxWidgets (wxGTK) графический интерфейс (GUI) для набора консольных утилит FFmpeg (frontend for ffmpeg).
28. Syncplay — специализированное Python / QT (PySide) графическое приложение (GUI) для синхронизации воспроизведения видеоплееров.
29. PulseEffects — удобное Python / GTK графическое приложение для набора PulseAudio эффектов.
30. ComplexShutdown — очень простая в использовании Python / GTK графическая утилита для завершения работы компьютера,
    таймер обратного отчёта.
31. Gpoweroff — маленькая и простая Python / GTK графическая утилита предлагающая различные способы планирования
    выключения компьютера, выхода из системы, перезагрузки, запуска спящего или ждущего режима.
32. EasyShutdown — маленькая Python / GTK графическая утилита для запланированного выключения компьютера. Таймер
    обратного отчёта выключения можно выставить от одной секунды да 99 часов, 59 минут и 59 секунд.
33. ANoise (Ambient Noise) — специализированный аудиоплеер для воспроизведения разнообразных фоновых шумов и/или звуков
    окружающей среды.
34. HSTR (HiSToRy) — консольная C (си) / Curses (Ncurses / GNU Readline) утилита для интерактивного поиска и выполнения
    команд bash / zsh из истории.
35. gifcast — небольшая утилита для записи происходящего в отдельном окне, на рабочем столе и выделенной области
    рабочего стола, сохраняя действия в анимированное GIF изображение.
36. ttygif — консольная C (си) утилита для преобразования файлов ttyrec (tty-файлов) в GIF-изображение с анимацией.
    Утилита является урезанной версией консольной утилиты ttyplay (воспроизведение tty-файла, часть комплекта утилит
    ttyrec).
37. Bucklespring — консольная утилита для эмуляции "щелчков" механической клавиатуры IBM Model-M при печати на современной клавиатуре.
38. cmdpack-wordadd — набор консольных C (си) утилит различного назначения, форк (копия) набора консольных утилит
    cmdpack (Command-Line Pack).
39. PySolFC (PySol Fan Club edition) — хорошо систематизированная Python / GTK / Tk коллекция из более тысячи пасьянсов,
    модификация клуба поклонников PySol.
40. Freecell — консольная Ncurses реализация популярного пасьянса "Свободная ячейка" (FreeCell).
41. Conty - This is an easy to use compressed unprivileged Linux container packed into a single executable that works on
    most Linux distros. It is designed to be as simple and user-friendly as possible. You can use it to run any
    applications, including games (Vulkan and OpenGL).
42. https://github.com/davecheney/httpstat
43. Firezone is a self-hosted VPN server and Linux firewall - https://github.com/firezone/firezone
44. Netdata is a distributed, real-time, performance and health monitoring platform for systems, hardware, containers
    and applications, collecting thousands of useful metrics with zero configuration needed.
45. FiloSottile/age: A simple, modern and secure encryption tool (and Go library) with small explicit keys, no config
    options, and UNIX-style composability.
46. Weave Ignite is an open source Virtual Machine (VM) manager with a container UX and built-in GitOps management.
47. Belarusian language module for Babel, v1.5 - https://github.com/andrewshadura/babel-belarusian
48. iwdrofimenu - Minimalistic WiFi network chooser for iwd with a rofi frontend.
49. Distrobox - https://github.com/89luca89/distrobox
50. ContainerSSH - An SSH Server that Launches Containers in Kubernetes and Docker


# What is a "Senior Developer"?
1. A senior developer can balance business and technical requirements.
2. A senior developer remembers that other people have to maintain and use their code.
3. A senior developer can grasp a problem, understand the goals its trying to satisfy, and appropriately evaluate
   technical solutions to said problem.

# About Linux
Но изучать дистры - трата времени. Изучай Линукс. Изучи баш, процессы, сигналы, основные утилиты, что лежит в /proc,
напиши пару своих утилит. Будешь в 10 раз больше знать чем какой сиделец на арче, который шрифты додрачивает непонятно
для чего. Это все будут переносимые знания и в любом дистре будешь как дома после короткого знакомства.

# Principles for Simple Systems
1. *Features don’t justify complexity.* What good is a complicated flight control system if it grounds an entire fleet
   of aircraft, or an enterprise marketing platform like Marketo if nobody can run a marketing campaign? Choose tools
   that are simple to operate over those that promise the most features. A frequent recommendation I give to startups is
   to choose HubSpot for their marketing platform instead of an enterprise platform like Marketo, Eloqua, or Pardot.
2. *Complex ideas lead to complex implementations.* If it takes too long to explain or grasp an idea, then its
   implementation will be complex, and it will take too long to fix when something inevitably breaks. For example, a
   proposed sales process that requires an hour-long presentation will be a nightmare to maintain, regardless of how
   clever it seems.
3. *Modifications before additions.* When new requirements come up, the tendency is to add layers on top of the existing
   system—by way of additional steps or integrations. Instead, see if the system’s core can be modified to meet the new
   requirements. The change may cause (planned) downtime upfront, as with my Marketo-to-HubSpot migration example, but
   less (unplanned) downtime over the long term.

# Choosing a good book on programming

* Provide explanations that match your way of thinking.
* You can understand the examples being used.
* The author gets straight to the point without filling empty space with useless babbling in order to make his or hers book thicker and more expensive.
* The author uses different types of examples rather than just expanding the same example with more and more complexity.
* Don't waste tons of paper on telling you the history of the programming language. History - even if it is interesting - is for history books, not programming books. You can locate such information online.
* Don't waste tons of paper on general information that is easily located online, such as "Unix man pages" or similar stuff.
* Begins at a level that fits your current skills.

# The Hobo Ethical Code
1. Decide your own life; don’t let another person run or rule you.
2. When in town, always respect the local law and officials, and try to be a gentleman at all times.
3. Don’t take advantage of someone who is in a vulnerable situation, locals or other hobos.
4. Always try to find work, even if temporary, and always seek out jobs nobody wants. By doing so you not only help a
   business along, but ensure employment should you return to that town again.
5. When no employment is available, make your own work by using your added talents at crafts.
6. Do not allow yourself to become a stupid drunk and set a bad example for locals’ treatment of other hobos.
7. When jungling in town, respect handouts, do not wear them out, another hobo will be coming along who will need them as badly, if not worse than you.
8. Always respect nature, do not leave garbage where you are jungling.
9. If in a community jungle, always pitch in and help.
10. Try to stay clean, and boil up wherever possible.
11. When traveling, ride your train respectfully, take no personal chances, cause no problems with the operating crew or
    host railroad, act like an extra crew member.
12. Do not cause problems in a train yard, another hobo will be coming along who will need passage through that yard.
13. Do not allow other hobos to molest children; expose all molesters to authorities…they are the worst garbage to infest any society.
14. Help all runaway children, and try to induce them to return home.
15. Help your fellow hobos whenever and wherever needed, you may need their help someday.

# Про тестирование

* Процесс создания программного продукта — это отношения между людьми, как эмоциональные, так и рациональные.
* Каждый проект находится в условиях неопределенности и давления фактора времени.
* Несмотря на наши лучшие ожидания и намерения по отношению к проекту, некоторая степень неопытности, небрежности и
  некомпетентности является нормальной.
* Тест — это деятельность; смысл его в процессе, а не в артефактах.
* Цель тестирования — выявить статус производимого продукта и оценить, что может угрожать его полезности, так, чтобы
  наши клиенты могли принимать обоснованные решения по поводу него.
* Мы обязуемся проводить надежное и экономически эффективное тестирование, и мы будем информировать наших клиентов о
  чем-либо, что угрожает этому обязательству.
* Мы не станем сознательно или по небрежности вводить в заблуждение наших клиентов и коллег.
* Тестеры несут ответственность за качество своей работы, хотя они не могут контролировать качество продукта.

# Things I Believe About Software Engineering

- Writing non-trivial software that is correct (for any meaningful definition of correct) is beyond the current
  capabilities of the human species.
- Being aligned with teammates on what you're building is more important than building the right thing.
- There are many fundamental discoveries in computer science that are yet to be found.
- Peak productivity for most software engineers happens closer to 2 hours a day of work than 8 hours.
- Most measures of success are almost entirely uncorrelated with merit.
- Thinking about things is a massively valuable and underutilized skill. Most people are trained to not apply this
  skill.
- The fact that current testing practices are considered "effective" is an indictment of the incredibly low standards of
  the software industry.
- How kind your teammates are has a larger impact on your effectiveness than the programming language you use.
- The amount of sleep that you get has a larger impact on your effectiveness than the programming language you use.

## Commandments of IT Slackerism
1. **Stupidity is not the same as the lack of intelligence...** It's an independent dimension, quality of its own. It's
   unwitting self-destruction, the ability to act against one's best interests, social blindness...  It's a a typical
   quality of gifted programmers/system administrators and you need to cultivate skepticism and your sense of humor in
   order to fight this disease before it destroys you...
2. **There is a very fine line between software development as job, as hobby, and as mental disease.** Thou shalt cultivate
   other interests to ensure that evil software development spirits do not fully possess thy soul.  There's much more to
   life than developing software day and night including open source software. Remember about warning signs of a
   software developer addiction: "My personal appearance went downhill. I didn't care. My girlfriend left. I lost my
   job. I didn't care. I had become, yes, a open source programmer!". Remember that sacrificing your life for developing
   some semi-useless and duplicative open source program might be not the best way to realize yourself as a person.
   Developers pay for OSS, and they often pay a heavy price. Just ask Larry Wall.
3. **Value your time and use the highest level of language possible.** Program in scripting language unless it is absolutely
   necessary to use compiled language or Java.  If your program does not work or is useless it is not important how
   efficient it is. If it is useful,  people will use it even it is slightly slower then compiled language version.
   Also, typically 20% of code consumes 80% of time, therefore concentrating on those you can speed the program much
   more that writing everything in lower level language. Ignore the proliferation of OO programming languages (all of
   which seem to have stolen countless features from one another). It makes it difficult to understand why all those
   features are needed, and, especially, why the hell you should study them.  That's not a warning sign that you cannot
   cope with the University program. That actually may means two things:
        - You are still normal despite studying software engineering for some time.
        - In software fashion rulez no matter what.
4. **Thou shalt know by your heart that all software sucks, but Unix sucks less the other OSes.** Beware of those who say
   that their software does not suck, for they are either zealots or liars or charlatans. There is no silver bullet in
   software engineering.  That includes Microsoft products, GCC, Linux, Solaris, Java, etc.  Most of the books/articles
   that worship some fashionable trends that promise some kind of breakthrough are either intentionally (written by
   software engineering charlatans)  or unintentionally ( written by religious zealots) misleading and will be forgotten
   in a decade or so. The only true revelation of the art of programming is contained in  The Art of Computer
   Programming written by Donald Knuth. In operating systems domain Unix is more elegant and sucks less that other OSes,
   but it still sucks.  Especially as a desktop. The necessity to tinker with OS to make some device work is a good
   training exercise during college days, but it became annoying and distracting masochism  later. Both Microsoft
   Windows and Linux are to operating systems what McDonalds is to gourmet cooking: too much fat.  Thou shalt try other
   OSes including minimized Linux distributions, OpenBSD/FreeBSD, etc, it is has features that make it more suitable to
   the task in hand. Never assume that any particular OS is good for all tasks.
5. **When people are free to do as they please, they usually imitate each other.** It's better to destroy your health while
   you are being handsomely paid, that do it for free. Paradoxically a lot of great software was written by trying to
   meet tough deadlines in the commercial project.
6. **Beware of "this needs to be rewritten" trap.** More often that not this is just a manifestation of  "Not invented here"
   syndrome, which is a powerful motivator for doing stupid things. I've never seen an good programmer who examined the
   code and did not say or think "Well, this crap needs to be rewritten!" If code works, it usually doesn't need to be
   rewritten despite the fact that it doesn't fit your prejudices. Value your time and don't rewrite things that does
   not make sense in any language... unless absolutely necessary
7. **When you encounter ideas pushed by higher management that politely could only be described as “ridiculous” think
   twice before trying to enlighten those poor smacks.** The chances are reasonably high that the "the one, the only" whom
   you try to enlighten is a sociopath and you will inflict severe punishment on yourself for your own stupidity.
   Instead of boiling about stupidity of the idea, think about (possibly covert) ways to convert completely stupid
   suggestion into something at least workable without irritating stupid jerks. Or at least benefiting personally from
   this stupidity.  Moreover in ten years differences much be negligible as everything will be swiped in the sea of
   obsolesce  by a new wave of technologies. IT management jerks control much less that they think and circumventing
   them helps to polish your architectural skills ;-).  Think strategically and try to understand simple fact that in
   3-5-10 years nobody will care about the fact that those jerks moved electrons in wrong direction. It's all like
   creating a beautiful painting on a sand beach -- the next big wave will wipe everything anyway. Chances are that
   during the project you might have an opportunity to change the direction in some kind of covert action; also think
   about what you can learn while doing the project independently of the results and what training you can get  as a
   bonus for not questioning stupid higher up judgment.
8. **Initiative in any large corporation IT department is a punishable offence.** You will be much better off writing open
   source software as a hobby under pseudonym, or taking a couple of courses at company expense, then trying to break
   the bureaucracy walls in your current company. Actually self-education including but not limited to writing open
   source software might get you faster to better position, salary, etc in a different company that might value your
   skills higher then current.
9. **Remember that in any project the most suitable programming language is the language that project leader knows the
   best.  Don't fight such  idiosyncrasies even if you hate the language.** You can always generate one language from
   another and create a prototype in the language you like (without advertizing this transgression ;-) and manually
   translate it into a target language.  Think strategically: the language is just one tool in the tool chain and if it
   has a good debugger  it's an OK language.  Otherwise try to find other people who share your resentment and present
   facts about debugger quality in an objective non-threatening to the ego of the project leader way.
10. **Thou shall never believe that by clapping hands and chanting "La! La! La! Free/Open Software is the best!" long and
    loudly enough, it'll come true.** That's Raymondism. Choose free over non-free only when it is better suits your needs
    or you have no money to buy commercial software and thou art willing to fix what is broken. Choose a license of
    thine liking for software thou writest and do not blame those who choose differently for software they write.
    Remember that Unix is more than 30 years old, GNU is more then 25 years old, and Linux is more then 15 years old.
    Never refer to anything that is more then ten years old as revolutionary. You should just laugh at those poor jerks
    who call  Linux a "the revolutionary operating system".  Linux is  "the last century operating system" and no better
    or worse then other flavors of Unix; it just more bloated :-).  Ask yourself if it really make sense killing
    yourself trying make it better or promoting it in your crazy corporate IT environment. Whatever flavor of Unix is
    present in your environment might suit you just fine :-). Open Standards are not equivalent to open source and are
    more important than open source. Like people benefit from knowing more than one language, programmers can benefit
    from knowing and using at least two OSes: one for the desktop and the other for the server. Monoculture of software
    is bad, diversity within reasonable limits is good.  Never put all eggs into one basket, be it Windows or Linux,
    Java or Python.

## Id Software Programming principle
1. No prototypes. Just make the game. Polish as you go. Don’t depend on polish happening later. Always maintain constantly shippable code.
2. It’s incredibly important that your game can always be run by your team. Bulletproof your engine by providing
   defaults upon load failure.
3. Keep your code absolutely simple. Keep looking at your functions and figure out how you simplify further.
4. Great tools help make great games. Spend as much time on tools as possible.
5. We are our own best testing team and should never allow anyone else to experience bugs or see the game crash. Don’t
   waste others’ time. Test thoroughly before checking in your code.
6. As soon as you see a bug, you fix it. Do not continue on. If you don’t fix your bugs your new code will be built on a
   buggy codebase and ensure an unstable foundation.
7. This was very true in the 90s when consumer computers could be so much slower than high-end ones.
8. Write your code for this game only - not for a future game. You’re going to be writing new code later because you’ll be smarter.
9. Encapsulate functionality to ensure design consistency. This minimizes mistakes and saves design time.
10. Try to code transparently. Tell your lead and peers exactly how you are going to solve your current task and get
    feedback and advice. Do not treat game programming like each coder is a black box. The project could go off the
    rails cause delays.
11. Programming is a creative art form based in logic. Every programmer is different and will code differently.  It’s
    the output that matters.

# Managing finance
## Managing your financial life requires following three rules:
* Spend less than you earn;
* Prioritize investing for your future;
* Figure out what matters and spend accordingly.

# Programmers Falsehood
## Falsehoods programmers believe about build systems
All of the following are **wrong**:
* Build graphs are trees.
* Build graphs are acyclic.
* Every build step updates at most one file.
* Every build step updates at least one file.
* Compilers will always modify the timestamps on every file they are expected to output.
* It's possible to tell the compiler which file to write its output to.
* It's possible to tell the compiler which directory to write its output to.
* It's possible to predict in advance which files the compiler will update.
* It's possible to narrow down the set of possibly-updated files to a small hand-enumerated set.
* It's possible to determine the dependencies of a target without building it.
* Targets do not depend on the rules used to build them.
* Targets depend on every rule in the whole build system.
* Detecting changes via file hashes is always the right thing.
* Detecting changes via file hashes is never the right thing.
* Nobody will ever want to rebuild a subset of the available dirty targets.
* People will only want to build software on Linux.
* People will only want to build software on a Unix derivative.
* Nobody will want to build software on Windows.
* People will only want to build software on Windows.
* Nobody will want to build on a system without strace or some equivalent.
* stat is slow on modern filesystems.
* Non-experts can reliably write portable shell script.
* Your build tool is a great opportunity to invent a whole new language.
* Said language does not need to be a full-featured programming language.
* In particular, said language does not need a module system more sophisticated than #include.
* Said language should be based on textual expansion.
* Adding an Nth layer of textual expansion will fix the problems of the preceding N-1 layers.
* Single-character magic variables are a good idea in a language that most programmers will rarely use.
* System libraries and globally-installed tools never change.
* Version numbers of system libraries and globally-installed tools only ever increase.
* It's totally OK to spend over four hours calculating how much of a 25-minute build you should do.
* All the code you will ever need to compile is written in precisely one language.
* Everything lives in a single repository.
* Files only ever get updated with timestamps by a single machine.
* Version control systems will always update the timestamp on a file.
* Version control systems will never update the timestamp on a file.
* Version control systems will never change the time to one earlier than the previous timestamp.
* Programmers don't want a system for writing build scripts; they want a system for writing systems that write build scripts.

## Falsehoods Programmers Believe About Sea
* Search engines work like databases
* Search can be considered an additional feature just like any other
* Search can be added as a well performing feature to your existing product quickly
* Search can be added as a well performing feature to your existing product with reasonable effort
* Choosing the correct search engine is easy and you will always be happy with your decision
* Once setup, search will work the same way forever
* Once setup, search will work the same way for a while
* Once setup, search will work the same way for the next week
* The default search engine settings will deliver a good search experience
* Customers know what they are looking for
* Customers who know what they are looking for will search for it in the way you expect
* Customers who don’t know what they are looking for will search accordingly
* A customer using the same query twice expects the same results for both searches
* Customers only search for a few terms
* Customers only search for less than some set number of terms
* Customers never copy and paste a whole document into a search bar
* Customers balance quotes and parenthesis
* Customers that don’t balance quotes or parenthesis don’t expect phrasing or grouping
* You can pass the customer query directly into your search engine
* You can write a query parser that will always parse the query successfully
* You will never have to return a query parse error to the customer
* When you find the boolean operator ‘OR’, you always know it doesn’t mean Oregon
* Customers notice their own misspellings
* Customers don’t expect your search to correct misspellings
* It is possible to create a list of all misspellings
* It is possible to create an algorithm to handle all misspellings
* A misspelled word is never the same as another correctly spelled word
* All customers expect spelling correction to work the same
* All customers want their misspellings corrected
* A search should always return results, no matter how absurd
* If you don’t have any results to show, customers won’t mind
* When the perfect results are shown to the customer, they will notice it
* You don’t need to monitor search queries, results, and clicks
* Customers won’t get nervous that you are logging their search activity
* Search queries are not affected by GDPR
* Looking at the data, it is always possible to tell whether a customer found what they were looking for
* Customers will click on what they are looking for when they’ve found it
* You can build a search that works like Google
* You can build a search that works like Google sometimes
* You should use Google as a target for your search
* Customers don’t mind if your search doesn’t work like Google
* Customers don’t expect your search to work like Google
* Customers won’t compare you to Google
* A bad search, no matter how minor nor how rare, will never reflect poorly on your product
* Since Google doesn’t use facets, customers don’t need them
* Facet hit counts are always correct
* Facets have no impact on performance
* You can just cache queries to get performant facets
* Personalized search is easy
* Learning to rank is easy and just requires a plugin
* You have enough data for learning-to-rank
* Over time, you can curate enough data for learning-to-rank
* You don’t need to spend lots of time formatting content for it to work well in your search engine
* Text extraction engines will always produce text that doesn’t need to be post-processed
* All your markup will be stripped as you expect it to be
* Content is well formed
* Content is mostly well formed
* Content is predictably well formed
* Content, sourced from a database and templates, are formed the same
* Content teams treat search as their top priority
* Manually changing content to improve search is easy
* Improving content can be automated with reasonable effort
* Queries for ‘C programming’ and ‘C++ programming’ will produce different results
* Queries for ‘401k’ and ‘401(k)’ will produce the same results
* Tokenization as it works out of the box is right for your content and queries
* Tokenization can be changed to meet the needs of your entire corpus and all queries
* Tokenization can be changed to meet the needs of most of your corpus and most queries
* Tokenization can be conditional
* You should roll your own tokenizer
* You will never have a debate about tokenization
* Regular Expressions for tokenization is a good idea
* Regular Expressions have minimal performance impact
* You will never have a debate about regular expressions
* You should remove stop words
* You should not remove stop words
* You know what the list of stop words should be
* Stop words will never change
* When you find the stopword ‘in’, you know it doesn’t mean Indiana
* It’s easy to make certain things case sensitive
* Case sensitivity is a good idea
* Synonyms are easy
* Synonyms will improve recall in the way you want
* Synonyms have the same relevance in all documents
* Synonyms for Abbreviations and Acronyms always work as you expect
* Synonyms can be extracted from your corpus with natural language processing
* Using Word2Vec for synonyms is a good idea
* Stemming will solve your recall problems
* Lemmatization will solve your recall problems
* Lemmatization dictionaries are static
* Languages don’t change
* Natural language processing (NLP) tools work perfectly
* Incorporating NLP into your analysis pipeline is straightforward
* Search queries are complete sentences and can be accurately tagged with parts of speech
* Showing a list of search suggestions is easy
* Suggestions should just use the out of the box search engine suggestions
* Suggestions should incorporate customer query logs
* Customers would never type anything offensive into your search bar
* Customers would never try to hack you through your search bar
* Customers don’t need highlighting to find what they’ve searched for
* Default highlighters are good enough for all your content and queries
* Making a custom highlighter isn’t too difficult. It’s just matching strings right?
* Making a custom highlighter that is better than the default version will take less than a year
* Turning on caching will solve your performance issues
* Customers don’t expect near real time updates
* 30 second commit time is short enough for everyone

# To Be Alone Amongst Friend
George Mallory, explaining the attraction of mountaineering, said: "Why do we travel to remote locations? To prove our
adventurous spirit or to tell stories about incredible things? We do it to be alone amongst friends and to find
ourselves in a land without man." Most fantasy RPGs are about what Mallory puts under the umbrella of "proving [one's]
adventurous spirit or telling stories about incredible things". That is what I think you would describe as the main
motivation for D&D PCs (along with gaining wealth and power), for example, and the game models that well enough. Fantasy
RPGs model the pleasure of "being alone among friends in a land without man" much less successfully. The struggle of man
in the wilderness and the pleasures and hardships of travel and exploration have largely escaped our focus. This may
replicate an imbalance at the heart of fantasy literature, which I think has tended to draw from the more bombastic,
saving-the-world and derring-do elements of The Lord of the Rings while neglecting the (huge) parts of the story which
concern travel and exploration as interesting activities in their own right. You don't get many fantasy epics which are
mainly about the experience of a journey, although Vance's books can have this quality. Eventually I will get around to
buying Ryuutama, which promises to be able to produce that type of play experience. Some elements of the game (the
"players and GM create the world together" and the idea of nudging PCs to help each other through the mechanics) are a
turn off. But it seems worth putting up with that - by which I mean cutting out those bits - for "Hayao Miyazaki’s
Oregon Trail". As long as I can transpose the contents into Lower Druk Yul or Lamarakh or whatever, we're golden.

# Drunk Post: Things I've learned as a Sr Enginee
I'm drunk and I'll probably regret this, but here's a drunken rank of things I've learned as an engineer for the past 10 years.

1. The best way I've advanced my career is by changing companies.
2. Technology stacks don't really matter because there are like 15 basic patterns of software engineering in my field
   that apply. I work in data so it's not going to be the same as webdev or embedded. But all fields have about 10-20
   core principles and the tech stack is just trying to make those things easier, so don't fret overit.
3. There's a reason why people recommend job hunting. If I'm unsatisfied at a job, it's probably time to move on.
4. I've made some good, lifelong friends at companies I've worked with. I don't need to make that a requirement of every
   place I work. I've been perfectly happy working at places where I didn't form friendships with my coworkers and I've
   been unhappy at places where I made some great friends.
5. I've learned to be honest with my manager. Not too honest, but honest enough where I can be authentic at work. What's
   the worse that can happen? He fire me? I'll just pick up a new job in 2 weeks.
6. If I'm awaken at 2am from being on-call for more than once per quarter, then something is seriously wrong and I will
   either fix it or quit.
7. Qualities of a good manager share a lot of qualities of a good engineer.
8. When I first started, I was enamored with technology and programming and computer science. I'm over it.
9. Good code is code that can be understood by a junior engineer. Great code can be understood by a first year CS
   freshman. The best code is no code at all.
10. The most underrated skill to learn as an engineer is how to document. Fuck, someone please teach me how to write
    good documentation. Seriously, if there's any recommendations, I'd seriously pay for a course (like probably a lot
    of money, maybe 1k for a course if it guaranteed that I could write good docs.)
11. Related to above, writing good proposals for changes is a great skill.
12. Almost every holy war out there (vim vs emacs, mac vs linux, whatever) doesn't matter... except one. See below.
13. The older I get, the more I appreciate dynamic languages. Fuck, I said it. Fight me.
14. If I ever find myself thinking I'm the smartest person in the room, it's time to leave.
15. I don't know why full stack webdevs are paid so poorly. No really, they should be paid like half a mil a year just
    base salary. Fuck they have to understand both front end AND back end AND how different browsers work AND networking
    AND databases AND caching AND differences between web and mobile AND omg what the fuck there's another framework out
    there that companies want to use? Seriously, why are webdevs paid so little.
16. We should hire more interns, they're awesome. Those energetic little fucks with their ideas. Even better when they
    can question or criticize something. I love interns.
17. Don't meet your heroes. I paid 5k to take a course by one of my heroes. He's a brilliant man, but at the end of it I
    realized that he's making it up as he goes along like the rest of us.
18. Tech stack matters. OK I just said tech stack doesn't matter, but hear me out. If you hear Python dev vs C++ dev,
    you think very different things, right? That's because certain tools are really good at certain jobs. If you're not
    sure what you want to do, just do Java. It's a shitty programming language that's good at almost everything.
19. The greatest programming language ever is lisp. I should learn lisp.
20. For beginners, the most lucrative programming language to learn is SQL. Fuck all other languages. If you know SQL
    and nothing else, you can make bank. Payroll specialtist? Maybe 50k. Payroll specialist who knows SQL? 90k. Average
    joe with organizational skills at big corp? $40k. Average joe with organization skills AND sql? Call yourself a PM
    and earn $150k.
21. Tests are important but TDD is a damn cult.
22. Cushy government jobs are not what they are cracked up to be, at least for early to mid-career engineers. Sure,
    $120k + bennies + pension sound great, but you'll be selling your soul to work on esoteric proprietary technology.
    Much respect to government workers but seriously there's a reason why the median age for engineers at those places
    is 50+. Advice does not apply to government contractors.
23. Third party recruiters are leeches. However, if you find a good one, seriously develop a good relationship with
    them. They can help bootstrap your career. How do you know if you have a good one? If they've been a third party
    recruiter for more than 3 years, they're probably bad. The good ones typically become recruiters are large
    companies.
24. Options are worthless or can make you a millionaire. They're probably worthless unless the headcount of engineering
    is more than 100. Then maybe they are worth something within this decade.
25. Work from home is the tits. But lack of whiteboarding sucks.
26. I've never worked at FAANG so I don't know what I'm missing. But I've hired (and not hired) engineers from FAANGs
    and they don't know what they're doing either.
27. My self worth is not a function of or correlated with my total compensation. Capitalism is a poor way to determine
    self-worth.
28. Managers have less power than you think. Way less power. If you ever thing, why doesn't Manager XYZ fire somebody,
    it's because they can't.
29. Titles mostly don't matter. Principal Distinguished Staff Lead Engineer from Whatever Company, whatever. What did
    you do and what did you accomplish. That's all people care about.
30. Speaking of titles: early in your career, title changes up are nice. Junior to Mid. Mid to Senior. Senior to Lead.
    Later in your career, title changes down are nice. That way, you can get the same compensation but then get an
    increase when you're promoted. In other words, early in your career (<10 years), title changes UP are good because
    it lets you grow your skills and responsibilities. Later, title changes down are nice because it lets you grow your
    salary.
31. Max out our 401ks.
32. Be kind to everyone. Not because it'll help your career (it will), but because being kind is rewarding by itself.
33. If I didn't learn something from the junior engineer or intern this past month, I wasn't paying attention.
34. Paying for classes, books, conferences is worth it. I've done a few conferences, a few 1.5k courses, many books, and
    a subscription. Worth it. This way, I can better pretend what I'm doing.
35. Seriously, why aren't webdevs paid more? They know everything!!!
36. Carpal tunnel and back problems are no joke. Spend the 1k now on good equipment.
37. The smartest man I've every worked for was a Math PhD. I've learned so much from that guy. I hope he's doing well.
38. Once, in high school, there was thing girl who was a great friend of mine. I mean we talked and hung out and shared
    a lot of personal stuff over a few years. Then there was a rumor that I liked her or that we were going out or
    whatever. She didn't take that too well so she started to ignore me. That didn't feel too good. I guess this would
    be the modern equivalent to "ghosting". I don't wish her any ill will though, and I hope she's doing great. I'm
    sorry I didn't handle that better.
39. I had a girlfriend in 8th grade that I didn't want to break up with even though I didn't like her anymore so I just
    started to ignore her. That was so fucked up. I'm sorry, Lena.
40. You know what the best part of being a software engineer is? You can meet and talk to people who think like you. Not
    necessarily the same interests like sports and TV shows and stuff. But they think about problems the same way you
    think of them. That's pretty cool.
41. There's not enough women in technology. What a fucked up industry. That needs to change. I've been trying to be more
    encouraging and helpful to the women engineers in our org, but I don't know what else to do.
42. Same with black engineers. What the hell?
43. I've never really started hating a language or technology until I started becoming intimately familiar with it.
    Also, I think a piece of tech is good if I hate it but I simultaneously would recommend it to a client. Fuck Jenkins
    but man I don't think I would be commuting software malpractice by recommending it to a new client.
44. That being said, git is awful and I have choice but to use it. Also, GUI git tools can go to hell, give me the
    command line any day. There's like 7 command lines to memorize, everything else can be googled.
45. Since I work in data, I'm going to give a data-specific lessons learned. Fuck pandas.
46. My job is easier because I have semi-technical analysts on my team. Semi-technical because they know programming but
    not software engineering. This is a blessing because if something doesn't make sense to them, it means that it was
    probably badly designed. I love the analysts on the team; they've helped me grow so much more than the most
    brilliant engineers.
47. Dark mode is great until you're forced to use light mode (webpage or an unsupported app). That's why I use light
    mode.
48. I know enough about security to know that I don't know shit about security.
49. Being a good engineer means knowing best practices. Being a senior engineer means knowing when to break best
    practices.
50. If people are trying to assign blame to a bug or outage, it's time to move on.
51. A lot of progressive companies, especially startups, talk about bringing your "authentic self". Well what if your
    authentic self is all about watching porn? Yeah, it's healthy to keep a barrier between your work and personal life.
52. I love drinking with my co-workers during happy hour. I'd rather spend time with kids, family, or friends.
53. The best demonstration of great leadership is when my leader took the fall for a mistake that was 100% my fault. You
    better believe I would've walked over fire for her.
54. On the same token, the best leaders I've been privileged to work under did their best to both advocate for my
    opinions and also explain to me other opinions 'that conflict with mine. I'm working hard to be like them.
55. Fuck side projects. If you love doing them, great! Even if I had the time to do side-projects, I'm too damn busy
    writing drunken posts on reddit
56. Algorithms and data strictures are important--to a point. I don't see pharmacist interviews test trivia about
    organic chemistry. There's something fucked with our industry's interview process.
57. Damn, those devops guys and gals are f'ing smart. At least those mofos get paid though.
58. It's not important to do what I like. It's more important to do what I don't hate.
59. The closer I am to the product, the closer I am to driving revnue, the more I feel valued regardless of how
    technical my work is. This has been true for even the most progressive companies.
60. Linux is important even when I was working in all Windows. Why? Because I eventually worked in Linux. So happy for
    those weekend where I screwed around installing Arch.
61. I've learned to be wary for ambiguous buzz words like big data. WTF is "big" data? I've dealt with 10k rows
    streaming every 10 minutes in Spark and Kafka and dealt with 1B rows batched up hourly in Python and MySQL. Those
    labels can go fuck themselves.
62. Not all great jobs are in Silicon Valley. But a lot are.

Finally, if you really want to hurt me, don't downvote I don't care about that. Just ignore this post. Nothing makes me
sadder than when I wrote a long post and then nobody responds. So if you hate this post, just ignore.

## Не пишите внутренние инструменты cli на Python

Коротенький и простой совет.

Никто не знает, как правильно устанавливать и упаковывать Python-приложения. Если вы пишете внутренний инструмент, он
должен быть либо полностью переносимым, либо написанным на Go или Rust. Избавьте себя и пользователя от страданий с
приложениями, которые не хотят корректно устанавливаться.


# All About Old School Dungeons and Dragons

## Twenty quick questions for your campaign setting

You can run D&D with just some PCs and a dungeon. I think that's totally legit. But if you have a campaign setting, here
are some things for you to think about.  Better to muse on these before your players ask you, rather than finding
yourself on the spot.

1. What is the deal with my cleric's religion?
2. Where can we go to buy standard equipment?
3. Where can we go to get platemail custom fitted for this monster I just befriended?
4. Who is the mightiest wizard in the land?
5. Who is the greatest warrior in the land?
6. Who is the richest person in the land?
7. Where can we go to get some magical healing?
8. Where can we go to get cures for the following conditions: poison, disease, curse, level drain, lycanthropy,
   polymorph, alignment change, death, undeath?
9. Is there a magic guild my MU belongs to or that I can join in order to get more spells?
10. Where can I find an alchemist, sage or other expert NPC?
11. Where can I hire mercenaries?
12. Is there any place on the map where swords are illegal, magic is outlawed or any other notable hassles from Johnny Law?
13. Which way to the nearest tavern?
14. What monsters are terrorizing the countryside sufficiently that if I kill them I will become famous?
15. Are there any wars brewing I could go fight?
16. How about gladiatorial arenas complete with hard-won glory and fabulous cash prizes?
17. Are there any secret societies with sinister agendas I could join and/or fight?
18. What is there to eat around here?
19. Any legendary lost treasures I could be looking for?
20. Where is the nearest dragon or other monster with Type H treasure?

Campaign worlds, whether published or just notes scrawled in some DM's binder, contain a lot of material that most
players honestly just don't give a crap about.  That's entirely okay.  Answer some of these questions or others like
them and you'll have yourself a campaign regular players can relate to.

## An alternative to adventure hook

When it comes to designing trad RPG adventures, one of the first things you need is a hook to get the players involved
in that adventure. If the hook isn't good enough then no adventure.

Really, this isn't good design. As a group you already know you're under the social contact of the game when you enter
that magic circle. When the GM asks you to roll a die for a check, you roll the die, or perhaps suggest an alternative.
You don't just say 'nah, don't feel like it'. But that's kind of how plot/story hooks are used in a game.

The alternative is the GM asking the players "Here's the situation. How would your character get involved in this
adventure?" Revolve the adventure around the characters rather than the other way around. How do their motivations or
fears play into this adventure? This is a great opportunity to create emergent backgrounds for characters. "Well, my
father went out to the Borderlands but never returned. I would want to see if I can find his remains."



## How to free yourself from the mind prison of 5th Edition Dungeons & Dragons (and engage in the rest of the hobby that has been there all along.

- Decide you would like to try something new, because new things are fun and this is a big hobby, full of wonderful
  things.
- Start a new campaign using a free game that you find on itch.io or a cheap used game that you find at a crusty
  bookstore with a roleplaying game section. Print the rules out on your printer or at a library if the game you found
  only has digital copies. No recommendations for games will be provided in this post because finding the game you want
  to play is a game unto itself and any help would be a spoiler, learn to spend long hours steeped in the hobby of
  looking for games as a source of nourishment.
- Run your game once a week, regardless of who shows up. Do this online or in real life. Have a large pool of players,
  and never play with anyone you wouldn’t want to take a 3 hour car trip with. Never play for more than 2 hours over
  discord or more than three hours sitting around a real table in the real world. Try to keep people off their phones
  and keep track of things on large quantities of scrap paper that you are constantly losing track of. Understand that
  conflict may arise between the people at your table and try to treat those situations as if it were a dinner party and
  don’t expect a games rulebook to help with these conflicts.
- Talk to your group of gaming friends about the kind of game they want to play. LISTEN TO THEM. What parts of RPGs do
  they like, and which ones do they not care about? Do they like to be partially in control of story beats, or do they
  like the thrill of exploring an established world? Find out the things that you all find fun, and build your homebrew
  rules around those things as you go, cutting away the parts you collectively don’t need. 
- Be a hype machine for free game stuff. There are a million and a half things that folks have made to make your life as
  a Game Enjoyer easier, use as much of them as you need, and leave nice reviews of things in your wake.
- Ignore the majority of the rules of that game you printed out, it's only really there as a mascot for the activity and
  something for people to thumb through while they talk about a different game that they have been meaning to play.
  Consult it around once per session to see how much a bottle of holy water costs.
- Only play with people you trust to not lie about their dice rolls, and if they do lie about their dice rolls,
  understand that maybe they really need this, and just let it slide. 
- Start a blog to keep track of everything going on in your campaign in between sessions and for players to submit
  reports of their adventures from their characters perspectives.
- Be excellent to each other. This is a hobby, not life or death.

## A Grognard's Principles for Prepping and Running OSR Game

I've played a lot of OSR games, and with it, I have crafted some guiding principles that have served my games well. Hope
this can be of help, and invite discussion of what principles others follow.

### Preparing For a Session

**Your first session should start just outside the dungeon entrance**. Reason: Having the first session begin directly
outside of the dungeon immediately gives the players a clear objective—explore the dungeon. This lets the session hit
the ground running as players will jump immediately into action. Not to mention, this gives your preparation a clear end
point. Really, any clear objective and confined location should work since it simplifies prep to one location so the
players can immediately jump into the meat of the game.

Ask what your players will do next session. Reason: Sessions are best when you have good preparation, so leaving things
ambiguous as to what the characters will do session can lead to a lot of wasted prep. Be upfront with your players and
say what they decide is what you will prepare, so no changing minds last second. They will understand and it will make
your game world all the better.

Only prepare for the next session. Reason: Over-prepping is more likely to burn you out than help you. Broad stroke
ideas are enough for hooks to leave your players. They do not need to be fleshed out until they will be encountered in
the upcoming session.

Sprinkle hooks to adventures liberally around your world. Each location prepared should have at least one hook to
another location. Reason: Doing this will create a web of relationships that make your game world come to life. This
will make it easier for players to create their own objectives within the world.

Factions should have a goal, an obstacle, and a plan to overcome that obstacle. Reason: This hits the sweet spot for
prepping just enough to have great utility while not over prepping a faction in case your group never interacts with
them. Idea from this GFC DND video. All the videos on that channel are well worth watching by the way.

### Dungeon Design

Follow a Dungeon Checklist for stocking your dungeon:

- Something to talk to
- Multiple routes and branches to explore
- Environments that can be altered
- Something to kill you
- Something to slay
- Treasure to take
- Something to experiment with
- Something hidden

**Leave hints about other locations within the dungeon**. Reason: This lets the players make intelligent decisions about how
they are going to approach exploring the dungeon. You want players to think things like, "should we follow these
scratches or head the other way?".

**Make sure there are no bland swinging-bags-of-HP style encounters**. Reason: Fights where the players and enemies
alternate rolling without making a decision more meaningful than "I roll to hit the monster" are boring and undesirable.
Don't do this, I'll explain more further down.

### Combat Encounters

**Combats are best kept short and sweet**. Reason: The interesting decisions in a fight tend to be made early in the fight.
Once the interesting choices have been made (e.g. PCs/enemies utilizing the environments for an upper hand) then the
combat should be nearing a close. Roll for morale! Your enemies shouldn't want to die.

**Dynamic environments, enemy tactics, or secondary objectives add a good layer of complexity to what could be an
otherwise bland fight**. Environment Example: Environments that have high potential energy are ideal. Locations that
contain a cliff on a battle field, or the threat of a falling rocks trap provide the players/enemies an environmental
means of gaining an upper hand. Enemy Tactics: A goblin gang might jump the PCs then try to get the players to chase
them into an ambush; a team of orcs might target and try to kidnap the smallest member of the party. The point is the
enemies have a goal beyond take turns rolling dice until they are dead. Secondary Objectives: Objectives like "take that
wand from the Kobold Shaman", or "protect the prisoner from getting recaptured" are worth adding on occasion. Don't go
overboard with this, but do keep it in mind.

### Running the Dungeon

**State room descriptions in this order**: Dimension & Exits, Monsters, then Details. Reason: Dimensions and exits of the
room first so the players have a sense of space and the mapper can write it down. Describe sensory details of the
monsters seen, preferring to avoid stating its name outright. Then describe key details of the room. Use cardinal
directions frequently where appropriate. Having a standard order for narration helps hit all the important details of a
room consistently.

**When moving, count off feet (or squares) moved in a cardinal direction** Example: "You move west down the hallway 5ft,15
ft—you are standing in the middle of a T intersection with the halls continuing north or south. What do you do?" Reason:
This makes life easier for you and the mapper. They will be able to more accurately map, and tracking turns used in
movement becomes much simpler.

**Frequently restate the current situation and where the players are located**. Reason: Restating frequently helps keep
players from losing their sense of space and surroundings so everyone can stay on the same page. This is especially
important to do when entering new rooms/chambers. E.g. are the players standing at the entrance to the room? Or have
they entered and are standing in the middle?

### Miscellaneous Things I Recommend

**Use a caller**. A caller is the player who is designated to state what all the player characters do in a dungeon turn.
Other players can of course correct the caller should they misspeak. Reason: Structuring conversation through a caller
keeps turns organized much better than resolving things as players state what they want to do directly at you, the GM.
This might sound unappealing at first, but it works remarkably well for keeping the game moving. Players talk among
themselves, and ask questions to you about what they plan on doing. The turn only gets resolved when the caller is done
stating what actions are being taken this turn.

**Roll as little as possible**. Reason: Never roll for things that you expect the PCs to be able to do. Unless there is
some significant risk (e.g. hp loss) or reason they might fail, then the players should succeed with most things by
default no dice being rolled.

**Roll almost everything in the open**. Reason: Doing this has numerous positive effects on the table. For starters, it
prevents you from cheating your players as a crutch by showing them the consequences of their actions. Enemies won't
pull their punches. As a knock on effect (in my experience at least), players see the dice as an antagonist instead of
the GM. Even more, rolling in the open has fostered better cooperation between players and the GM at all the tables I
have been at. Secret rolls can be kept secret.

**Utilize Procedures**. Reason: Organization of play is key to keeping a steady and enjoyable pace to the game. It's a lot
harder to get derailed with it than without it.


## 12 Things I Wish I Would Had Known Before Running My First Game

* Your PC is only cool if he is cool during play, not because he has an amazing backstory. Likewise, your NPCs are only
  cool if they do cool stuff during play. Your adventure is only good if it provides good times to people playing it.
* The story does not have to be cool or make sense to anyone other than the people playing the game. Like in real life,
  you go on adventures to have experiences, not to tell stories after the fact.
* Different people like different things, including mechanics. Some people will never spend inspiration or that last
  potion of healing, no matter what you do. Some people want to pick fights, some people want interaction, some people
  want to play ninjas. If it suits them, that is okay.
* Find the best system for you and your players. Eventually, this will be the system you have created yourself.
* That puzzle (or conspiracy) you built for your players is not as obvious as you think. The players are not in your
  head.
* Everybody will forget most of the details after a couple of days. If you want long arches and complicated plots with
  various adventures, that is fine, but don't expect you player to remember every NPC they meet, unless they are
  recurring. Also, if something happens to the PCs - specially if they are wronged - they are more likely to remember
* Every important person, thing or location should have ONE obvious distinction. Not grey hair, but a mohawk. Not a
  scar, but a distinct lack of nose. Not grey houses, but impossibly tall spires. Think "caricatures".
* Do not plan the story in advance and do not keep safeguards against derailing. No fudging dice, no saving the players
  from bad luck or bad choices. You're robbing them of some amazing experiences. Failing is part of the game.
* Few fights should be to the death. People are more likely to surrender than to fight to the bitter end, and few
  animals will take a beating if they can escape.
* Common sense trumps the rules. But if the rules defy common sense all the time, you should be looking for a different
  set of rules. This is about rules as physics, not story - people defy common sense all the time!
* Let the dice push you out of your comfort zone. Your PCs all failed their saving throws - now what? Your important NPC
  was killed before he could start his plan - what happens now? If you rely on common sense to decide probabilities
  without using the dice, everything will become predictable.
* Everyone said that already, but expect the unexpected from your players. Do not assume they will be nice to a baby in
  the crib when they are invading a castle.

## Principles of adventure design from Bryce Lynch

### General Tips: The 5 C’s

**Color**: The referee should give brief but evocative descriptions of locations, monsters, NPCs, and treasures. Avoid
the vague or generic.

**Context**: In order for their actions to be significant and purposeful, players must generally have some information
about the likely consequences of their actions, such as likely reactions of monsters or NPCs.

**Choices**: There should be more than one course of action available to players in order for the adventure to continue.
Avoid choke points—both literal choke points in the physical layouts of dungeons and other locations, and figurative
choke points which require a unique decision or solution in order for the adventure to proceed.

**Consequences**: Player actions should be allowed to make a real difference in the adventure and in the campaign. Avoid
a set storyline or sequence of events immune to player interference.

**Creativity**: Related to (3) and (4), reward player creativity by allowing them to pursue unanticipated courses of
action or to produce unanticipated consequences, rather than restricting player action and player creativity by
setting up arbitrary constraints in the location layout or course of events.

### Hooks

Don’t rely on a single hook; use multiple kinds (treasure; reward; magic; glory; political power).

Create a rumor table with hooks and color.

Hooks should appeal to the players (not just their characters).

Hooks can and should be made complex / nuanced; e.g., working for an evil NPC, or working for rival factions.

To support sandbox play, particular dungeon, town, and wilderness locations, monsters, and NPCs should all have hooks.

### Locations (Dungeons, Towns, Wilderness, etc.)

Location descriptions should be terse (not verbose) but evocative (not boring, obvious, generic).

Only include background info that affects gameplay. Avoid long descriptions of irrelevant info.

Rooms should have features that players can interact with to produce meaningful consequences. Give concrete descriptions
of secret doors, traps, etc.

Floor plan tips:

- Multiple routes (vs. choke points or linear, one-way paths).
- Multiple entrances / exits.
- Multiple stairs per floor.
- Open spaces with balconies, galleries, and ledges at various elevations.
- Pools and rivers that connect different rooms or levels.
- Bridges and ladders.

### Monsters and NPCs

Create interesting, believable motivations for monsters and NPCs.

Create factions of monsters and NPCs (which lead to a dynamic, interconnected strategic situation).

Give players the choice of allying, attacking, or having other relationships with monsters and NPCs.

Create schedules, routines, tactics, and orders of battle for monsters and NPCs.

Wandering monsters too should be given motives, goals, hooks, and tactics.

Avoid standard monsters. Failing that, describe standard monsters in a non-standard way (e.g., don’t just name their
species).

Give evocative descriptions of monsters. Give concrete descriptions of their appearance and activities. Go for the
telltale sensory detail, rather than the generic abstract trait. Show, don’t tell. Example: Instead of stating that “One
of the guards in the camp is a cruel bully,” say that “The burly Manfred takes a leak on poor Tobias’s bedroll, and then
he snatches Tobias’s roasted chicken dinner from his hand and quickly gobbles it down.”

Use truly evil monsters to evoke a Sense of Terror.

### Treasure

Treasure should be valuable enough to motivate players and to make the challenges worthwhile.

Non-magical treasure should relate to the setting and give clues or information about monsters, NPCs, locations, etc.

Avoid standard magic items.

Give evocative descriptions of magic items. Give concrete descriptions of their appearance and how they must be
manipulated to produce their magical effects.

Use magic items to evoke a Sense of Wonder.

### Format and Functionality

Include the following kinds of reference tables:

- Rumor / hook table.
- Monster / NPC table which lists their main traits, motivations, location, etc.
- Room / building table which lists the rooms in a dungeon (or other keyed locations).

In published modules, put maps and monster stats on separate sheets (so they are easy to refer to in play).

On maps, use keyed symbols to indicate standard features (e.g., lit/unlit, locked/unlocked, secret, trapped, etc.),
rather than a verbal description in the location key.

## Uses of wine

1. Wine taste better than water.
2. Adding some wine to a waterskin or using it on leather can prevent algae, shrooms or other "slimy stuff" to grow on
   its surface.
3. You can use wine to clean wounds if you don't have anything better
4. Mixing wine into underboiled or "spoiled water" in a ratio of 1:3 could kill a lot of common stuff in it acting as a
   rudimental "purifier".
5. Pour wine on wood, let it dry and it will be easier to lit on fire.
6. Wine/Vinegar can be used as a pest control thing, it will kill a lot of insects, prevents flies, mosquitos and
   similar to stick on your skin and can also be used to keep them away from meat.
7. Wine can repel some birds.
8. Wine was supposed to repel wolves (nope, it doesn't ... it is a myth due to some drunkards being ignored by wolves in woods).
9. Wine can be used to treat ropes after rain.
10. Wine can be used to polish weapons and armors removing the stink of the grease/oil used to mantain them.
11. Wine can be used to bribe monsters. in the '70s - '80s drinking was supposed to be "mainly" so orcs, goblins,
    adventurers and other lowlifes were attracted to it.
12. Wine can be used as a pain killer, to remove an arrowhead stuck in your arm. If you drop alcohol on open wounds it
    will get to your blood stream and be a lot more effective. Recently it was rediscovered with vodka-based tampons or
    eyeballing.
13. Wine can be used to fix or nurture candles giving them a bit more lifetime.
14. Wine can be used as a catalyst making it easier to lit something on fire.
15. Wine was usually red so it can be used to detect holes, fissures or "minor slants" in the floor.
16. Using wine or winegar on flesh make it easier to "skin" an animal [nope, that's a myth, but it woud kill worms and
    other small parasites so the effect was the same].
17. Wine can be used to remove sticky stuff, like glue, blood or other bodily fluids from yourself.
18. Wine can be used to counter hypotermia due to SHORT exposure to low temperatures (i.e. swimming in a pool, white dragon breath...)
19. You can use alcohol as alcohol. It has a lot of uses, like neutralize some types of poisons/caustic reactions. i.e.
    you learn about it in the chemistry lessons. :)
20. Wine can be used to clean weapons coated in poison.
21. Conan used to drink wine. So it MUST be a trope.

# How to learn programming
I actually do have some advice for people on this topic. If you want to learn programming here is a very important concept:
Every piece of software you've ever used can be broken down into 4 simple ideas. What I call The Four Friends of Programming.
1. Save data and use it later. (example: variables, memory allocation)
2. Conditionally execute things. (example: if statement, switch, match, jump)
3. Repeat yourself. (example: loops, recursion)
4. Organize the first three things. (example: structs, functions, classes)

Thats it. Thats every program you've ever used. The next tip, is that every programming language is someone's opinion of
HOW you use those four friends. programming languages are made by people who have opinions about how to program. What
makes different languages interesting is that you get see/discover new ways of using those 4 simple rules to solve
problems. The people that wrote Java believed that all data should be contained in a type. The people that made Lisp
believed you only need lists and recursion to compute anything. But in the end its the same four rules wrapped in an
opinion of computation.

So. If you want to learn programming, pick a language, any language. One that you think will make you money or one that
you think will enable to build what you love easily. Then MASTER the four friends in that language. When you do that and
move on to the next language you can peel away syntax and jargon (because those are a someones opinions of how code
should look) and really peer into the big ideas of the person that designed it.

# About Windows containers
Windows actually has support for container. Its only Docker that doesn't support it, but Windows actually supports the
OCI API for open containers, one could literally try to make LXC work on windows if you wanted..

Containers are basically a namespace on the Kernel, the Windows NT kernel already had a namespace system even before
Linux ever though of having this idea. Its only that you can directly create namespaces on Windows using the
Kernel32/Win32 API, only the internal ones, well, that was before they exported their LXCSS system ( I mean, WSL ), they
also exported namespaces, there were only two namespaces, one that was used for everything usermode and the rest was
some sort of "/" root system.

Another fun thing is that if you enable container support, when you run Win32 applications on Windows on containers, it
is not a VM, only when you run Linux Container on the Windows container system that it uses its VM to para-virtualize
the Linux Kernel, its more like KVM what they did to the poor linux kernel inside windows Its KVM with WindowsNT as the
Host.
