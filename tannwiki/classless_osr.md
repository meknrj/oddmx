# Classless OSR

Written for LotFP and other Retroclones

- All saving throws start at 15.
- Experience and leveling work as a Fighter.
- Spell casting works as a Magic-User.
- Everyone starts with a +1 base attack modifier.
- Everyone has a 1-in-6 attempt at any skill (or 15% for non-LotFP systems).
- At level 1 the minimum HP regardless of roll is 3.

Each level, including 1st, you must opt for either fighting, casting, or proficiency.

**Fighting**: +1 attack modifier, D8 HP and 2 points lost from saves of your choice.

**Casting**: D6 HP, and 2 points lost from saves of your choice. The first time this is selected, you receive a grimoir
with 4 random spells. New spells are learned through adventuring. Each selection of casting represents one Magic-User
level, ie) selecting casting three times follows a Level-3 Magic-User’s spell table.

**Proficiency**: 2 skill points (or 30%) to expend, D6 HP, and 4 points deducted from saves of your choice. If using a
percentile system, use increments of 5%.

## Races

- Humans get 2 free skillpoints (30%) to distribute.
- Elves get a +1 to saves vs magic and a 1-in-6 chance to be surprised.
- Dwarves get 60’ darkvision and an extra +5 (10 lbs) to their encumbrance adjustment.
- Halflings get a +1 DEX modifier and 5-6 stealth (85%) when hiding outdoors.

**Skillchecks** are for attempts that have a risk of consequential failure, not mundane tasks. DMs should add or remove any
skills as their game sees fit. Below are common skills and examples of their uses that should cover most adventuring
situations:
- **Acrobatics**: tumbling, jumping - **dex**
- **Athletics**: climbing, force doors - **str**
- **Bushcraft**: nature, survival, animal handling - **wis**
- **Knowledge**: select a field of study - **int**
    - Arcana
    - Architecture & Engineering
    - History & Lore
    - Languages
    - Religion
- **Medicine**: healing, identifying maladies - **int**
- **Search**: finding secret doors & traps - **wis**
- **Sleight of Hand**: pickpocket, delicate tasks - **dex**
- **Speechcraft**: persuasion, deception - **cha**
- **Stealth**: hiding, moving silently - **dex**
- **Tinkering**: lock picking, removing traps - **dex**

DMs may choose to modify some or all of these skills by relevant modifiers, +1 (15%) per adjustment.

Negative modifiers may not reduce these skills below 15%.

For LotFP, a 6-in-6 skill rolls 2d6 and fails on double-6’s. For percentile based systems, no skill may go past 95%.

**Magic** is cast as per usual for your system, but there is no longer a division of Cleric and Magic-User spells. If
not using LotFP, Turn Undead is now relegated to being a Level-1 spell.
