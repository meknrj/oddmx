package main

import "fmt"

type OptFunc func(*Opts)

type Opts struct {
	maxConn int
	id      string
	tls     bool
}

func defaultOpts() Opts {
	return Opts{
		maxConn: 10,
		id:      "default",
		tls:     false,
	}
}

func withTLS(opts *Opts) {
	opts.tls = true
}

func withMaxConn(n int) OptFunc {
	return func(opts *Opts) {
		opts.maxConn = n
	}
}

func withId(id string) OptFunc {
	return func(opts *Opts) {
		opts.id = id
	}
}

type Server struct {
	Opts
}

func newServer(opts ...OptFunc) *Server {
	o := defaultOpts()
	for _, fn := range opts {
		fn(&o)
	}

	return &Server{
		Opts: o,
	}
}

func main() {
	defaultServer := newServer()
	fmt.Printf("%+v\n", defaultServer)

	tlsServer := newServer(withTLS)
	fmt.Printf("%+v\n", tlsServer)

	maxConnServer := newServer(withMaxConn(99))
	fmt.Printf("%+v\n", maxConnServer)

	customServer := newServer(withTLS, withMaxConn(1988), withId("wada wada wada"))
	fmt.Printf("%+v\n", customServer)

}
