== TODO ==

=== Fast todo ===
1. Find cloud storage provider (also sync on harddisk at home)
2. Add more art subbreddits feeds
3. Add https://simnalamburt.github.io/vim-mundo/ into vimrc
4. Configuration for firefox (???) - https://www.reddit.com/r/openbsd/comments/bzq5b9/how_i_configured_firefox_after_the_base_install/
5. Add pure sh scripts into *library* https://github.com/dylanaraps/pure-sh-bible
6. Add to library - https://benoithamelin.tumblr.com/ruby1line/
7. Add to library - https://github.com/dylanaraps/pure-bash-bible#obsolete-syntax
8. Wisdom tower - https://habr.com/ru/post/458672/
9. Compile into full guide https://www.reddit.com/r/SWN/comments/c7z5fc/keeping_track_of_combat_without_a_map_or/
10. Compile own list from this - http://taxidermicowlbear.weebly.com/dd-retroclones.html
11. Compile - https://www.reddit.com/r/rpg/comments/9xf3ky/a_hard_scifi_sandbox_for_mothership_sandbox/
12. Add to mklorum - https://throneofsalt.blogspot.com/2019/10/mothership-planet-generation.html
13. Add to mklorum - https://www.traaa.sh/mothership-house-rules
14. Add to mklorum - https://www.traaa.sh/mothership-warden-screen
15. Add to mklorum - https://www.reddit.com/r/mothershiprpg/comments/fl1uqf/corporate_dystopia_equipment_systems/
16. Add to mklorum - https://acheronsector.blogspot.com/
17. Review list of books - https://begriffs.com/posts/2017-04-13-longterm-computing-reading.html
18. Start tmux project -  http://danielallendeutsch.com/blog/16-using-tmux-properly.html
19. Wisdom tower - https://thereader.mitpress.mit.edu/habits-of-expert-software-designers/
20. Add docker snippets
21. Where to store passwords?
22. Auto upload of all changes into git repo
23. How to remove telemetry from vscode?

### Wiki - Guides
* Turn off firefox spying (https://www.unixsheikh.com/articles/mozilla-is-becoming-evil-be-careful-with-firefox.html)
* [Guide] Port https://www.dedoimedo.com/computers/docker-supervisord.html
* [Snippets] How to setup synaptic (Can XFCE help?)
* Add useful things to guide about supervisord
* [Guide] Port - https://opensource.com/article/18/7/sysadmin-guide-selinux - SELinux notes
* [Guide] Port https://stackoverflow.com/questions/20762094/how-are-zlib-gzip-and-zip-related-what-do-they-have-in-common-and-how-are-they/20765054#20765054
* [Guide] Port http://bruxy.regnet.cz/web/linux/EN/socks-via-ssh/
* [Guide] Port http://bruxy.regnet.cz/web/linux/EN/ssh-bastion/
* [Guide] Port http://bruxy.regnet.cz/web/linux/EN/ssh-agent/
* [Guide] Loadbalancing websockets
* [Guide] Pack Windows games into Wine (https://rutracker.org/forum/viewtopic.php?t=5152471)
* [Guide] How to use deb and rpm packages on Slackware
* [Guide] Searching files on IRC
* [Snippets] Snippet for slackware init - https://www.computerhope.com/unix/telinit.htm
* [Snippets] Vivaldi browser hacks
* [Guide] IPTables
	- https://phoenixnap.com/kb/iptables-tutorial-linux-firewall
	- https://ru.wikibooks.org/wiki/Iptables
	- https://www.booleanworld.com/depth-guide-iptables-linux-firewall/
	- https://www.frozentux.net/iptables-tutorial/iptables-tutorial.html
* What the difference between ifconfig and ip on linux?
* https://www.linuxquestions.org/questions/slackware-14/configure-a-printer-in-14-2-a-4175585896/ - Configure printer on Slackware 14.2
* More info about LVM - http://wiki.linuxformat.ru/wiki/LXF112:LVM
* Links hot keys - http://links.twibright.com/user_en.html
* https://www.reddit.com/r/linux_gaming/comments/cydgxu/guide_configuring_pcsx2_150_with_brief_explanation/
