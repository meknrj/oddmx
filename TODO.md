# General
- How to make backups, backup info from smartphone, do not lose phone number
- Use fzf for file search - https://thevaluable.dev/practical-guide-fzf-example/
- Add tmux configuration
- Add rg configuration
- Setup https://clang.llvm.org/extra/clang-tidy/checks/list.html
- Setup clang format
- Command to run j4-desktop-menu - j4-dmenu-desktop --dmenu "dmenu -fn 'Liberation Mono:pixelsize=19:style=bold' -l 20 -nb '#1a1a1a' -nf '#d6d6d6' -sb '#7f9f7f' -sf '#3a3a3a'" --term alacritty --term-mode alacritty --display-binary --usage-log .j4dd-usage.log

# RPG
- Take Cepheus Engine as a base for RPG hack
