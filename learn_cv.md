# C Programming

## Videos and courses
1. https://www.youtube.com/watch?v=2ybLD6_2gKM
2. https://www.youtube.com/watch?v=87SH2Cn0s9A
3. https://www.youtube.com/watch?v=9UIIMBqq1D4
4. https://www.youtube.com/watch?v=GV10eIuPs9k
5. https://www.youtube.com/watch?v=8JEEYwdrexc
6. https://www.youtube.com/watch?v=AYSISa95oJE
7. https://www.youtube.com/watch?v=xCxI2GIFdZo

## Questions
1. Maybe it make more sense to switch to golang (with c)?
2. Try to have examples (c files) for each part
3. \_t variables - why to use them instead of normal ones?
4. What is pointers?
5. Hash maps
6. Dynamic arrays
7. UTF-8 symbols - how to use them and how it works?
8. How to impelement array bound checking via struct (in runtime)? This approach can be applied to the strings too.
9. Cmake
10. How to exactly measure function execution time? (In C++ it could be done with rtdsc assembly instruction)
11. How sanitazers works? (clang -fsanitize=adress)
12. Autotools
13. Unit tests:
    - What libraries exists?
15. Makefiles
16. Clangd integration with vim:
    - Bear (compilation database generator)
    - ALE fixers:
        - clangtidy
        - clang-format
18. How to debug segfaults?
19. How to use gdb? Vim integration?
20. Sanitazers
21. Valgrind
22. Macros debugging
23. Most useful macroses
24. Objdump
25. elfread
26. How #embed macro works?
27. How main works? Why void main()?

## Books
1. Effective C (2ed)
2. Fluent C
3. Tiny C Projects
4. Extreme C
5. The CERT C Coding Standard
6. Some books about linux kernel development
7. https://clang.llvm.org/docs/PointerAuthentication.html

# Vim setup

1. Gdb
2. clang-formatter

# SQL and Databases

1. Only MySQL (check course from CodeBro)
2. Read about specifics of MySQL
3. Difference from MariaDB
4. Setup mysqlrc
5. How to handle passwords?
6. How to migrate between databases?
7. How to properly describe table with primary and foreign keys?

# Scripting

1. Use golang for scripting
2. How to run simple server to share files?
3. How to write/read files?
4. Concurrency in go
5. httptest package

# Tools to work

1. Run qemu vms with shell scripts
2. How to prepare vms
3. Qemu snapshots
4. Connect to vm for debugging

# Container

1. Put vim inside
2. Compilers
3. https://github.com/mviereck/x11docker

# Tools to look cool

1. MPV instead of vlc
2. Patched st terminal
3. tmux (to use it for different projects - workspaces)
4. https://www.youtube.com/watch?v=wLVHXn79l8M - DOS BBS on Kubernetes (or jail?)

# Linux

1. How to build kernel?
2. How to debug kernel?
3. Using qemu to debug kernel
4. How to create ssh tunnels in Go?
